defmodule LogisticService.MixProject do
  @moduledoc false
  use Mix.Project

  def project do
    [
      app: :logistic_service,
      version: "0.1.0",
      elixir: "~> 1.11",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      docs: [main: "LogisticService"],
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {LogisticService.Application, []},
      extra_applications: [
        :logger,
        :scrivener_ecto,
        :runtime_tools,
        :timex,
        :corsica,
        :os_mon
      ]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  defp deps do
    [
      {:ex_doc, "~> 0.21.3", only: :dev, runtime: false},
      {:faker, "~> 0.13"},
      # Феникс и его html шаблонизатор
      {:phoenix, "~> 1.5.4"},
      {:phoenix_html, "~> 2.11"},
      {:phoenix_pubsub, "~> 2.0"},
      # ОРМ
      {:phoenix_ecto, "~> 4.1"},
      # Адаптеры для ОРМ
      {:ecto_sql, "~> 3.4"},
      {:postgrex, ">= 0.0.0"},
      # Graphql
      {:absinthe, "~> 1.5"},
      {:absinthe_plug, "~> 1.5"},
      {:absinthe_phoenix, "~> 2.0"},
      {:dataloader, "~> 1.0"},
      # Замер времени ответа при gql
      {:apollo_tracing, "~> 0.4.0"},
      # Live мониторинг системы
      {:phoenix_live_view, "~> 0.14.0"},
      {:phoenix_live_dashboard, "~> 0.2"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:telemetry_metrics, "~> 0.4"},
      {:telemetry_poller, "~> 0.4"},
      # Переводы и работа с текстом
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.0"},
      # сервер эликсира
      {:plug_cowboy, "~> 2.0"},
      # Работа с корсами
      {:corsica, "~> 1.0"},
      # Работа с хэшированием\паролями
      {:comeonin, "~> 5.3"},
      {:bcrypt_elixir, "~> 2.2"},
      {:guardian, "~> 1.0"},
      # Кэширование
      {:cachex, "3.2.0"},
      # Пагинатор для ОРМ
      {:scrivener_ecto, "~> 2.0.0"},
      # Более удобная работа с временем
      {:timex, "~> 3.7"},
      # Система планируемых задач типа крона
      {:quantum, "~> 3.0"},
      # Смена регистров
      {:recase, "~> 0.5"},
      # Линтинг для
      {:credo, "~> 1.4", only: [:dev, :test], runtime: false},
      # облегчение создания объектов
      {:shorter_maps, "~> 2.0"},
      # csv ридер
      {:csv, "~> 2.3"},
      # парсер в мапу
      {:morphix, "~> 0.8.0"},
      # Делаем логи приложения по файлам и форматируем логи
      {:logger_file_backend, "~> 0.0.10"},
      {:distillery, "~> 2.1"},
      {:tesla, "~> 1.4.0"},
      # optional, but recommended adapter
      {:hackney, "~> 1.16.0"},
      {:poison, "~> 3.1"},
      {:ecto_function, "~> 1.0.1"},
      {:elixlsx, "~> 0.4.2"},
      {:excoveralls, "~> 0.10", only: :test},
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false}
    ]
  end

  defp aliases do
    [
      setup: ["deps.get", "ecto.setup", "cmd npm install --prefix assets"],
      "ecto.setup": ["ecto.create", "ecto.migrate", "ecto.seed"],
      "ecto.seed": ["run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate --quiet", "test"]
    ]
  end
end
