defmodule LogisticService.Accounts.Utils.Loader do
  @moduledoc """
  dataaloader for accounts module
  """

  import Ecto.Query, warn: false
  alias LogisticService.Repo

  def datasource, do: Dataloader.Ecto.new(Repo, query: &query/2)
  def query(q, _), do: q
end
