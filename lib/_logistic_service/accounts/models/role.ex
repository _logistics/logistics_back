defmodule LogisticService.Accounts.Models.Role do
  @moduledoc """
  Role model
  """
  alias LogisticService.Accounts.Models.Role
  use Ecto.Schema
  import Ecto.Changeset

  @required_fields ~w(slug)a
  @optional_fields ~w(extensions notes)a

  @type t :: %Role{}
  schema "roles" do
    field :extensions, :map
    field :notes, :string
    field :slug, :string

    many_to_many :users, LogisticService.Accounts.Models.User,
      join_through: "user_roles",
      on_replace: :delete
  end

  @doc false
  def changeset(role, attrs) do
    role
    |> update_changeset(attrs)
    |> validate_required(@required_fields)
  end

  @doc false
  def update_changeset(role, attrs) do
    role
    |> cast(attrs, @optional_fields ++ @required_fields)
    |> validate_length(:slug, min: 3, max: 30)
  end
end
