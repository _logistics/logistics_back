defmodule LogisticService.Accounts.Models.UserCode do
  @moduledoc false
  alias LogisticService.Accounts.Models.UserCode
  use Ecto.Schema
  import Ecto.Changeset

  @required_fields ~w(phone code)a
  @optional_fields ~w()a

  @type t :: %UserCode{}
  schema "user_code" do
    field :phone, :string
    field :code, :integer
  end

  @doc false
  def changeset(user_photo, attrs) do
    user_photo
    |> update_changeset(attrs)
    |> validate_required(@required_fields)
  end

  @doc false
  def update_changeset(user_photo, attrs) do
    user_photo
    |> cast(attrs, @optional_fields ++ @required_fields)
  end
end
