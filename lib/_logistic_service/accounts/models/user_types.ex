defmodule LogisticService.Accounts.Models.UserTypes do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset

  schema "user_types" do
    field :name, :string
  end

  @doc false
  def changeset(user_types, attrs) do
    user_types
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
