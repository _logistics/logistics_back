defmodule LogisticService.Accounts.Models.Profile do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset

  alias LogisticService.Accounts.Models.User

  @required_fields ~w(user_id)a
  @optional_fields ~w(state town area street comments house_number floor apartment first_name last_name mail middle_name phone prefered_chanel telegram vk whatsapp)a
  @protected_fields ~w(locked_at user_id)a

  schema "personal_profiles" do
    field :state, :string
    field :town, :string
    field :area, :string
    field :street, :string
    field :house_number, :string
    field :floor, :string
    field :apartment, :string
    field :comments, :string
    field :first_name, :string
    field :last_name, :string
    field :locked_at, :naive_datetime
    field :mail, :string
    field :middle_name, :string
    field :phone, :string
    field :prefered_chanel, :string
    field :telegram, :string
    field :vk, :string
    field :whatsapp, :string

    timestamps()
    belongs_to :user, User, foreign_key: :user_id, on_replace: :update
  end

  @doc false
  def changeset(profile, attrs) do
    profile
    |> update_changeset(attrs)
    |> fetch_user(attrs)
    |> validate_required(@required_fields)
  end

  def update_changeset(profile, attrs) do
    profile
    |> cast(attrs, @optional_fields ++ @required_fields)
  end

  def fetch_user(entity, attrs) do
    case Map.get(attrs, :user) do
      nil -> entity
      val -> cast(entity, %{user_id: g(val)}, @protected_fields)
    end
  end

  def admin_update_changeset(profile, attrs) do
    profile
    |> cast(attrs, @optional_fields ++ @required_fields ++ @protected_fields)
  end

  def g(val), do: Map.get(val, :id)
end
