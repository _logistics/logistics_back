defmodule LogisticService.Accounts.Models.UserSmses do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset

  schema "user_smses" do
    field :counter, :integer
    field :phone, :string
    timestamps()
  end

  @doc false
  def changeset(user_smses, attrs) do
    user_smses
    |> cast(attrs, [:counter, :phone, :updated_at])
    |> validate_required([:phone])
  end
end
