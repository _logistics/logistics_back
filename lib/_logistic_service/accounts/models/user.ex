defmodule LogisticService.Accounts.Models.User do
  @moduledoc false
  alias LogisticService.Accounts.Models.User
  use Ecto.Schema
  import Ecto.Changeset
  alias LogisticService.Accounts.Models.{Role, CommonSetting, PageSetting, OrgProfile, Profile}
  alias LogisticService.Transfering.Models.Order

  @required_fields ~w(phone)a
  @optional_fields ~w(logged_at deleted_at one_c_data is_organization is_confirmed contract_link opportunity_to_create confirmation_code comments updated_at)a
  @private_fields ~w(password password_confirmation)a

  @type t :: %User{}
  schema "users" do
    field :phone, :string

    field :one_c_data, :string
    field :is_confirmed, :boolean
    field :confirmation_code, :string
    field :comments, :string

    field :contract_link, :string
    field :opportunity_to_create, :boolean

    timestamps()
    field :deleted_at, :utc_datetime
    field :logged_at, :utc_datetime

    field :is_organization, :boolean

    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true
    field :password_hash, :string

    many_to_many :roles, Role, join_through: "user_roles", on_replace: :delete
    has_many :common_settings, CommonSetting
    has_many :page_settings, PageSetting

    has_many :sended, Order, foreign_key: :sender_id
    has_many :couriering, Order, foreign_key: :courier_id

    has_one :user_profile, Profile
    has_one :org_profile, OrgProfile
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> update_changeset(attrs)
    |> update_password_changeset(attrs)
    |> validate_required(@required_fields)
    |> unique_constraint(:phone)
  end

  def update_password_changeset(user, attrs) do
    user
    |> cast(attrs, @private_fields)
    |> validate_length(:password, min: 5)
    |> validate_confirmation(:password)
    |> put_password_hash
  end

  def update_changeset(user, attrs) do
    user
    |> cast(attrs, @optional_fields ++ @required_fields)
    |> validate_length(:phone, is: 10)
  end

  def assoc_roles_changeset(user, roles) do
    user
    |> cast(%{}, @required_fields)
    |> put_assoc(:roles, roles)
  end

  defp put_password_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pwd}} ->
        put_change(changeset, :password_hash, Bcrypt.hash_pwd_salt(pwd))

      _ ->
        changeset
    end
  end
end
