defmodule LogisticService.Accounts.Models.PageSetting do
  @moduledoc false
  alias LogisticService.Accounts.Models.PageSetting
  use Ecto.Schema
  import Ecto.Changeset
  alias LogisticService.Accounts.Models.User
  alias LogisticService.Services.Models.Page

  @required_fields ~w(name)a
  @optional_fields ~w(settings page_id user_id)a

  @type t :: %PageSetting{}
  schema "page_settings" do
    field :name, :string
    field :settings, :map
    field :page_id, :integer
    field :user_id, :integer

    belongs_to :users, User
    belongs_to :pages, Page
  end

  @doc false
  def changeset(page_setting, attrs) do
    page_setting
    |> update_changeset(attrs)
    |> validate_required(@required_fields)
  end

  @doc false
  def update_changeset(page_setting, attrs) do
    page_setting
    |> cast(attrs, @required_fields ++ @optional_fields)
  end
end
