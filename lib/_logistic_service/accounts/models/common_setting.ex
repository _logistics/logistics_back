defmodule LogisticService.Accounts.Models.CommonSetting do
  @moduledoc false
  alias LogisticService.Accounts.Models.CommonSetting
  use Ecto.Schema
  import Ecto.Changeset
  alias LogisticService.Accounts.Models.User

  @required_fields ~w(name)a
  @optional_fields ~w(settings_json settings_string user_id)a

  @type t :: %CommonSetting{}
  schema "common_settings" do
    field :name, :string
    field :settings_json, :map
    field :settings_string, :string
    field :user_id, :id

    belongs_to :users, User
  end

  @doc false
  def changeset(common_setting, attrs) do
    common_setting
    |> update_changeset(attrs)
    |> validate_required(@required_fields)
  end

  @doc false
  def update_changeset(common_setting, attrs) do
    common_setting
    |> cast(attrs, @optional_fields ++ @required_fields)
  end
end
