defmodule LogisticService.Accounts.Models.UserStatuses do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset

  schema "user_statuses" do
    field :name, :string
  end

  @doc false
  def changeset(user_statuses, attrs) do
    user_statuses
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
