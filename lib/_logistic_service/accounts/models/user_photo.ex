defmodule LogisticService.Accounts.Models.UserPhoto do
  @moduledoc false
  alias LogisticService.Accounts.Models.UserPhoto
  use Ecto.Schema
  import Ecto.Changeset
  alias LogisticService.Accounts.Models.User

  @required_fields ~w(photo_link user_id)a
  @optional_fields ~w()a

  @type t :: %UserPhoto{}
  schema "user_photos" do
    field :photo_link, :string

    belongs_to :user, User
  end

  @doc false
  def changeset(user_photo, attrs) do
    user_photo
    |> update_changeset(attrs)
    |> validate_required(@required_fields)
  end

  @doc false
  def update_changeset(user_photo, attrs) do
    user_photo
    |> cast(attrs, @optional_fields ++ @required_fields)
  end
end
