defmodule LogisticService.Accounts.Delegates.ProfilesManagement do
  @moduledoc false
  alias LogisticService.Accounts.Delegates.{
    UserFuncs,
    UserProfilesFuncs,
    OrganizationProfilesFuncs
  }

  alias LogisticService.Accounts.Models.{Profile, OrgProfile}

  def show_user_profile(user_id) do
    UserProfilesFuncs.get_profile(user_id: user_id)
  end

  def create_user_profile(user_id, attrs) do
    user = UserFuncs.get_user_with_profile(user_id)

    case Map.get(user, :user_profile) do
      nil ->
        attrs
        |> Map.put(:user, user)
        |> UserProfilesFuncs.create_profile()

      _ ->
        {:error, "Профиль пользователя уже зарегистрирован."}
    end
  end

  def update_user_profile(user_id, attrs) do
    user = UserFuncs.get_user_with_profile(user_id)

    case Map.get(user, :user_profile) do
      nil ->
        attrs
        |> Map.put(:user, user)
        |> UserProfilesFuncs.create_profile()

      %Profile{} = user_profile ->
        UserProfilesFuncs.update_profile(user_profile, attrs)

      _ ->
        {:error, "Неизвестная ошибка, невозможно обновить профиль."}
    end
  end

  def delete_user_profile(user_id) do
    user = UserFuncs.get_user_with_profile(user_id)

    case Map.get(user, :user_profile) do
      nil -> {:error, "Профиль пользователя не найден."}
      %Profile{} = user_profile -> UserProfilesFuncs.delete_profile(user_profile)
      _ -> {:error, "Неизвестная ошибка, невозможно обновить профиль."}
    end
  end

  def show_org_profile(user_id) do
    OrganizationProfilesFuncs.get_org_profile(user_id: user_id)
  end

  def create_org_profile(user_id, attrs) do
    user = UserFuncs.get_user_with_org_profile(user_id)

    case Map.get(user, :org_profile) do
      nil ->
        attrs
        |> Map.put(:user, user)
        |> OrganizationProfilesFuncs.create_org_profile()

      _ ->
        {:error, "Профиль организации уже зарегистрирован."}
    end
  end

  def update_org_profile(user_id, attrs) do
    user = UserFuncs.get_user_with_org_profile(user_id)

    case Map.get(user, :org_profile) do
      nil ->
        attrs
        |> Map.put(:user, user)
        |> OrganizationProfilesFuncs.create_org_profile()

      %OrgProfile{} = org_profile ->
        OrganizationProfilesFuncs.update_org_profile(org_profile, attrs)

      _ ->
        {:error, "Неизвестная ошибка, невозможно обновить профиль."}
    end
  end

  def delete_org_profile(user_id) do
    user = UserFuncs.get_user_with_org_profile(user_id)

    case Map.get(user, :org_profile) do
      nil -> {:error, "Профиль организации не найден."}
      %OrgProfile{} = org_profile -> OrganizationProfilesFuncs.delete_org_profile(org_profile)
      _ -> {:error, "Неизвестная ошибка, невозможно обновить профиль."}
    end
  end
end
