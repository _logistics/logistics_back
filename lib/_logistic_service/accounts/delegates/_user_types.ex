defmodule LogisticService.Accounts.Delegates.UserTypesFuncs do
  @moduledoc """
  Base Functions for User
  """
  import Ecto.Query, warn: false
  alias LogisticService.Repo

  alias LogisticService.Accounts.Models.UserTypes

  @doc """
  Returns the list of user_types.

  ## Examples

      iex> list_user_types()
      [%UserTypes{}, ...]

  """
  def list_user_types do
    Repo.all(UserTypes)
  end

  @doc """
  Gets a single user_types.

  Raises `Ecto.NoResultsError` if the User types does not exist.

  ## Examples

      iex> get_user_types!(123)
      %UserTypes{}

      iex> get_user_types!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user_types!(id), do: Repo.get!(UserTypes, id)

  @doc """
  Creates a user_types.

  ## Examples

      iex> create_user_types(%{field: value})
      {:ok, %UserTypes{}}

      iex> create_user_types(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user_types(attrs \\ %{}) do
    %UserTypes{}
    |> UserTypes.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user_types.

  ## Examples

      iex> update_user_types(user_types, %{field: new_value})
      {:ok, %UserTypes{}}

      iex> update_user_types(user_types, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user_types(%UserTypes{} = user_types, attrs) do
    user_types
    |> UserTypes.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a user_types.

  ## Examples

      iex> delete_user_types(user_types)
      {:ok, %UserTypes{}}

      iex> delete_user_types(user_types)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user_types(%UserTypes{} = user_types) do
    Repo.delete(user_types)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user_types changes.

  ## Examples

      iex> change_user_types(user_types)
      %Ecto.Changeset{data: %UserTypes{}}

  """
  def change_user_types(%UserTypes{} = user_types, attrs \\ %{}) do
    UserTypes.changeset(user_types, attrs)
  end
end
