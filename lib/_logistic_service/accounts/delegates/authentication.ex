defmodule LogisticService.Accounts.Delegates.Authentication do
  @moduledoc """
  Это модуль Авторизации пользователя
  Подробнее в документации проекта
  phone = "9692003453"
  password = "x1WjNUNYwVz/yd2n/s"
  LogisticService.Accounts.Delegates.Authentication.signin(%{phone: phone, password: password})
  """
  @dialyzer {:nowarn_function, is_admin?: 1}
  import Bcrypt, only: [verify_pass: 2]
  alias LogisticService.Accounts.Delegates.{UserFuncs, RoleModel}
  alias LogisticService.Accounts.Models.User
  alias LogisticService.Accounts.Delegates.UserCodeFuncs
  alias Ecto.Multi
  alias Helper.{DBR, GuardianUser}
  import ShorterMaps
  require Faker

  def reset_password(phone), do: send_message(phone)

  def update_password(phone, code, password, password_confirmation) do
    if UserCodeFuncs.correct_submission("+7#{phone}", code) do
      signup(~M{phone, password, password_confirmation})
    else
      {:error, "неверный код"}
    end
  end

  def submit_signup(phone, code, attrs) do
    if UserCodeFuncs.correct_submission("+7#{phone}", code) do
      signup(attrs)
    else
      {:error, "неверный код"}
    end
  end

  def signup(phone) when is_binary(phone), do: send_message(phone)

  def signup(attrs) when is_map(attrs) do
    IO.inspect(attrs)

    try do
      result =
        Multi.new()
        |> Multi.run(:create_user, fn _, _ ->
          UserFuncs.create_user(attrs)
        end)
        |> Multi.run(:upsert_user_roles, fn _, %{create_user: user} ->
          RoleModel.upsert_user_roles(user, attrs[:roles])
        end)
        |> DBR.transaction()
        |> signup_afterinsert()

      case result do
        {:ok, _data} = res ->
          res

        {:error, _data} = _res ->
          user = attrs |> Map.get(:phone) |> UserFuncs.get_user_by_phone!()
          UserFuncs.update_user!(user, attrs)

          signin(%{phone: attrs[:phone], password: attrs[:password]})
      end
    rescue
      _e -> {:error, "Не может быть авторизован"}
    end
  end

  defp signup_afterinsert({:ok, %{create_user: user, upsert_user_roles: _user_with_roles}}) do
    {:ok, user} = UserFuncs.find_user_with_roles(user.id)

    with {:ok, result} <- get_token(user) do
      {:ok, result}
    end
  end

  defp signup_afterinsert({:error, :create_user, %Ecto.Changeset{} = result, _steps}),
    do: {:error, result}

  def signin(%{phone: phone, password: password}) do
    case DBR.find_by(User, phone: phone) do
      {:ok, user} ->
        with %{password_hash: password_hash} <- user,
             true <- verify_pass(password, password_hash) do
          user
          |> UserFuncs.update_user_phone_date!()
          |> get_token()
        else
          _err -> {:error, :unauthorized}
        end

      {:error, error} ->
        {:error, error}
    end
  end

  def signed_in?(assigns: assigns), do: assigns[:current_user_id] || assigns["current_user_id"]
  def signed_in?(%{assigns: assigns} = _conn), do: assigns[:current_user_id]

  defp get_token(%User{} = user) do
    with {:ok, token, _info} <- GuardianUser.jwt_encode(user) do
      {:ok, %{token: token, user: user}}
    end
  end

  def authorize(token) do
    with {:ok, claims, _info} <- GuardianUser.jwt_decode(token) do
      case DBR.find(User, claims.phone) do
        {:ok, user} ->
          user

        {:error, _} ->
          {:error, "Пользователь не существует или токен устарел"}
      end
    else
      error -> error
    end
  end

  def verify_password_change(%{
        id: id,
        old_password: old_pwd,
        password: password,
        password_confirmation: password_confirmation
      }) do
    with {:ok, user} <- UserFuncs.get_user(id),
         %{password_hash: password_hash} <- user,
         true <- verify_pass(old_pwd, password_hash),
         true <- is_password_confirmed(password, password_confirmation) do
      true
    else
      {:error, msg} -> msg
      _e -> false
    end
  end

  def verify_password_change(_), do: "Введите все необходимые поля."

  def is_admin?(%User{} = user), do: is_admin?(user.id)

  def is_admin?(user_id) when is_integer(user_id) do
    try do
      if UserFuncs.find_admin_user(user_id),
        do: true,
        else: false
    rescue
      e -> {:error, e}
    end
  end

  def is_current?(c, ~M{user_id}) do
    try do
      if c.id !== nil and c.id === user_id, do: true, else: false
    rescue
      e -> {:error, e}
    end
  end

  def is_current?(c, u) do
    try do
      if c.id !== nil and c.id === u.id, do: true, else: false
    rescue
      e -> {:error, e}
    end
  end

  def send_message(phone) do
    case LogisticService.Accounts.validate_new("#{phone}") do
      {:error, _} = e ->
        e

      {:ok, _} ->
        code = Faker.random_between(10_000, 99_999)
        phone = "+7#{phone}"

        try do
          {:ok, _any} = UserCodeFuncs.create_user_code(phone, code)
          :ok = SMS.send_sms(phone, code)

          {:ok, "сообщение успешно отправлено"}
        rescue
          _e ->
            {:error, "ошибка при отправке сообщения"}
        end
    end
  end

  defp is_password_confirmed(pwd, pwd_c) do
    case String.equivalent?(pwd, pwd_c) do
      true -> true
      false -> {:error, "пароли не совпадают"}
    end
  end
end
