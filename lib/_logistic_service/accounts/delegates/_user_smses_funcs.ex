defmodule LogisticService.Accounts.Delegates.UserSmsesFuncs do
  import Ecto.Query, warn: false
  alias LogisticService.Repo
  alias LogisticService.Accounts.Models.UserSmses

  def remove_old_smses() do
    dt = Timex.now() |> Timex.shift(days: -1)
    from(us in UserSmses, where: us.updated_at <= ^dt) |> Repo.delete_all()
  end

  def validate_new(phone) do
    user_smses = get_user_smses_or_create(phone)
    count = user_smses |> Map.get(:counter) |> Kernel.||(0)

    if count >= 3 do
      {:error, "too much requests #{count + 1}"}
    else
      update_counter(user_smses, %{counter: count + 1})
      {:ok, count + 1}
    end
  end

  def get_user_smses_or_create(phone) do
    case UserSmses |> Repo.get_by(phone: phone) do
      nil -> create_user_smses(%{phone: phone})
      {:ok, user_smses} -> user_smses
      user_smses -> user_smses
    end
  end

  def create_user_smses(attrs \\ %{}) do
    %UserSmses{}
    |> UserSmses.changeset(attrs)
    |> Repo.insert()
    |> case do
      {:ok, us} -> us
      e -> e
    end
  end

  def update_counter(user_smses, attrs) do
    user_smses
    |> UserSmses.changeset(attrs)
    |> Repo.update()
  end
end
