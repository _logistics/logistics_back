defmodule LogisticService.Accounts.Delegates.CommonSettingsFuncs do
  @moduledoc """
  В этом модуле описаны базовые функции для ролей
  """
  import Ecto.Query, warn: false
  alias LogisticService.Repo

  alias LogisticService.Accounts.Models.CommonSetting

  @doc """
  Returns the list of common_settings.

  ## Examples

      iex> list_common_settings()
      [%CommonSetting{}, ...]

  """
  def list_common_settings do
    Repo.all(CommonSetting)
  end

  @doc """
  Gets a single common_setting.

  Raises `Ecto.NoResultsError` if the Common setting does not exist.

  ## Examples

      iex> get_common_setting!(123)
      %CommonSetting{}

      iex> get_common_setting!(456)
      ** (Ecto.NoResultsError)

  """
  def get_common_setting!(id), do: Repo.get!(CommonSetting, id)

  @doc """
  Creates a common_setting.

  ## Examples

      iex> create_common_setting(%{field: value})
      {:ok, %CommonSetting{}}

      iex> create_common_setting(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_common_setting(attrs \\ %{}) do
    %CommonSetting{}
    |> CommonSetting.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a common_setting.

  ## Examples

      iex> update_common_setting(common_setting, %{field: new_value})
      {:ok, %CommonSetting{}}

      iex> update_common_setting(common_setting, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_common_setting(%CommonSetting{} = common_setting, attrs) do
    common_setting
    |> CommonSetting.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a common_setting.

  ## Examples

      iex> delete_common_setting(common_setting)
      {:ok, %CommonSetting{}}

      iex> delete_common_setting(common_setting)
      {:error, %Ecto.Changeset{}}

  """
  def delete_common_setting(%CommonSetting{} = common_setting) do
    Repo.delete(common_setting)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking common_setting changes.

  ## Examples

      iex> change_common_setting(common_setting)
      %Ecto.Changeset{data: %CommonSetting{}}

  """
  def change_common_setting(%CommonSetting{} = common_setting, attrs \\ %{}) do
    CommonSetting.changeset(common_setting, attrs)
  end
end
