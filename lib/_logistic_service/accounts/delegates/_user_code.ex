defmodule LogisticService.Accounts.Delegates.UserCodeFuncs do
  @moduledoc """
  Base Functions for User
  """
  alias LogisticService.Accounts.Models.UserCode
  alias LogisticService.Repo
  require Faker

  def create_user_code(phone, code) do
    try do
      %UserCode{}
      |> UserCode.changeset(%{phone: phone, code: code})
      |> Repo.insert()
    rescue
      _e ->
        usercode = UserCode |> Repo.get_by!(phone: phone)

        usercode
        |> UserCode.changeset(%{code: code})
        |> Repo.update()
    end
  end

  def get_user_code(phone), do: UserCode |> Repo.get_by!(phone: phone) |> Map.get(:code)

  def correct_submission(phone, code) do
    get_user_code(phone) == code
  end
end
