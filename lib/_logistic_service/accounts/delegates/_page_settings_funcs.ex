defmodule LogisticService.Accounts.Delegates.PageSettingsFuncs do
  @moduledoc """
  В этом модуле описаны базовые функции для ролей
  """
  import Ecto.Query, warn: false
  alias LogisticService.Repo

  alias LogisticService.Accounts.Models.PageSetting

  @doc """
  Returns the list of page_settings.

  ## Examples

      iex> list_page_settings()
      [%PageSetting{}, ...]

  """
  def list_page_settings do
    Repo.all(PageSetting)
  end

  @doc """
  Gets a single page_setting.

  Raises `Ecto.NoResultsError` if the Page setting does not exist.

  ## Examples

      iex> get_page_setting!(123)
      %PageSetting{}

      iex> get_page_setting!(456)
      ** (Ecto.NoResultsError)

  """
  def get_page_setting!(id), do: Repo.get!(PageSetting, id)

  @doc """
  Creates a page_setting.

  ## Examples

      iex> create_page_setting(%{field: value})
      {:ok, %PageSetting{}}

      iex> create_page_setting(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_page_setting(attrs \\ %{}) do
    %PageSetting{}
    |> PageSetting.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a page_setting.

  ## Examples

      iex> update_page_setting(page_setting, %{field: new_value})
      {:ok, %PageSetting{}}

      iex> update_page_setting(page_setting, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_page_setting(%PageSetting{} = page_setting, attrs) do
    page_setting
    |> PageSetting.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a page_setting.

  ## Examples

      iex> delete_page_setting(page_setting)
      {:ok, %PageSetting{}}

      iex> delete_page_setting(page_setting)
      {:error, %Ecto.Changeset{}}

  """
  def delete_page_setting(%PageSetting{} = page_setting) do
    Repo.delete(page_setting)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking page_setting changes.

  ## Examples

      iex> change_page_setting(page_setting)
      %Ecto.Changeset{data: %PageSetting{}}

  """
  def change_page_setting(%PageSetting{} = page_setting, attrs \\ %{}) do
    PageSetting.changeset(page_setting, attrs)
  end
end
