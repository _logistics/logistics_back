defmodule LogisticService.Accounts.Delegates.UserStatusesFuncs do
  @moduledoc """
  Base Functions for User
  """
  import Ecto.Query, warn: false
  alias LogisticService.Repo

  alias LogisticService.Accounts.Models.UserStatuses

  @doc """
  Returns the list of user_statuses.

  ## Examples

      iex> list_user_statuses()
      [%UserStatuses{}, ...]

  """
  def list_user_statuses do
    Repo.all(UserStatuses)
  end

  @doc """
  Gets a single user_statuses.

  Raises `Ecto.NoResultsError` if the User statuses does not exist.

  ## Examples

      iex> get_user_statuses!(123)
      %UserStatuses{}

      iex> get_user_statuses!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user_statuses!(id), do: Repo.get!(UserStatuses, id)

  @doc """
  Creates a user_statuses.

  ## Examples

      iex> create_user_statuses(%{field: value})
      {:ok, %UserStatuses{}}

      iex> create_user_statuses(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user_statuses(attrs \\ %{}) do
    %UserStatuses{}
    |> UserStatuses.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user_statuses.

  ## Examples

      iex> update_user_statuses(user_statuses, %{field: new_value})
      {:ok, %UserStatuses{}}

      iex> update_user_statuses(user_statuses, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user_statuses(%UserStatuses{} = user_statuses, attrs) do
    user_statuses
    |> UserStatuses.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a user_statuses.

  ## Examples

      iex> delete_user_statuses(user_statuses)
      {:ok, %UserStatuses{}}

      iex> delete_user_statuses(user_statuses)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user_statuses(%UserStatuses{} = user_statuses) do
    Repo.delete(user_statuses)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user_statuses changes.

  ## Examples

      iex> change_user_statuses(user_statuses)
      %Ecto.Changeset{data: %UserStatuses{}}

  """
  def change_user_statuses(%UserStatuses{} = user_statuses, attrs \\ %{}) do
    UserStatuses.changeset(user_statuses, attrs)
  end
end
