defmodule LogisticService.Accounts.Delegates.RoleFuncs do
  @moduledoc """
  В этом модуле описаны базовые функции для ролей
  """
  import Ecto.Query, warn: false
  alias LogisticService.Repo

  alias LogisticService.Accounts.Models.Role

  @doc """
  Returns the list of roles.

  ## Examples

      iex> list_roles()
      [%Role{}, ...]

  """
  def list_roles do
    Repo.all(Role)
  end

  @doc """
  Get role list.

  ## Examples

      iex> list_roles([1,2,3])
      [%Role{}, ...]

  """
  def list_roles(ids: role_ids) do
    Role
    |> where([r], r.id in ^role_ids)
    |> Repo.all()
  end

  def list_roles(slugs: role_slugs) do
    Role
    |> where([r], r.slug in ^role_slugs)
    |> Repo.all()
  end

  @doc """
  Gets a single role.

  Raises `Ecto.NoResultsError` if the Role does not exist.

  ## Examples

      iex> get_role!(123)
      %Role{}

      iex> get_role!(456)
      ** (Ecto.NoResultsError)

  """
  def get_role!(id), do: Repo.get!(Role, id)

  @doc """
  Gets a single role.

  Raises `Ecto.NoResultsError` if the Role does not exist.

  ## Examples

      iex> get_role!(123)
      %Role{}

      iex> get_role!(456)
      ** (Ecto.NoResultsError)

  """
  def get_role_by_slug(slug), do: Repo.get_by(Role, slug: slug)

  @doc """
  Creates a role.

  ## Examples

      iex> create_role(%{field: value})
      {:ok, %Role{}}

      iex> create_role(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_role(attrs \\ %{}) do
    %Role{}
    |> Role.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a role.

  ## Examples

      iex> update_role(role, %{field: new_value})
      {:ok, %Role{}}

      iex> update_role(role, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_role(%Role{} = role, attrs) do
    role
    |> Role.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a role.

  ## Examples

      iex> delete_role(role)
      {:ok, %Role{}}

      iex> delete_role(role)
      {:error, %Ecto.Changeset{}}

  """
  def delete_role(%Role{} = role) do
    Repo.delete(role)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking role changes.

  ## Examples

      iex> change_role(role)
      %Ecto.Changeset{data: %Role{}}

  """
  def change_role(%Role{} = role, attrs \\ %{}) do
    Role.changeset(role, attrs)
  end

  @doc """
  Создание множества пользователей

  ## Examples

      iex> multisert_users(users)
      %{"1" => %User{}, ..}

  """
  def multisert_roles(roles) do
    case roles |> multisert_roles! do
      {:error, changeset} -> {:error, changeset}
      entity -> {:ok, entity}
    end
  end

  def multisert_roles!(roles) do
    result =
      roles
      |> Enum.map(fn role ->
        %Role{}
        |> Role.changeset(role)
      end)
      |> Enum.with_index()
      |> Enum.reduce(Ecto.Multi.new(), fn {changeset, index}, acc ->
        Ecto.Multi.insert(acc, Integer.to_string(index), changeset)
      end)
      |> Repo.transaction()

    case result do
      {:ok, entity} -> entity
      {:error, _, changeset, _} -> {:error, changeset}
    end
  end
end
