defmodule LogisticService.Accounts.Delegates.RoleModel do
  @moduledoc """
  role model funcs
  """

  import Ecto.Query, warn: false

  import Helper.Validators,
    only: [is_list_of_string?: 1, is_list_of_int?: 1, is_list_of_structs?: 1]

  # alias Helper.{DBR}
  alias LogisticService.{Accounts, Repo}
  alias LogisticService.Accounts.Models.User
  alias LogisticService.Accounts.Delegates.{RoleFuncs, UserFuncs}

  @doc """
  обновляет роли пользователя - удаляет предыдущие
  """
  def upsert_user_roles(user, roles) when is_list(roles) and is_struct(user) do
    with {:ok, _struct} <- update_roles(user, roles) do
      Accounts.find_user_with_roles(user.id)
    else
      err -> err
    end
  end

  def upsert_user_roles(user_id, roles) when is_list(roles) and is_integer(user_id),
    do: UserFuncs.get_user!(user_id) |> upsert_user_roles(roles)

  def upsert_user_roles(user, _role_ids), do: {:error, user}

  defp update_roles(user, roles) do
    user
    |> Repo.preload(:roles)
    |> User.assoc_roles_changeset(fetch_roles(roles))
    |> Repo.update()
  end

  defp fetch_roles(roles) when is_list(roles) do
    cond do
      is_list_of_int?(roles) -> RoleFuncs.list_roles(ids: roles)
      is_list_of_string?(roles) -> RoleFuncs.list_roles(slugs: roles)
      is_list_of_structs?(roles) -> roles
      true -> []
    end
  end
end
