defmodule LogisticService.Accounts.Delegates.OrganizationProfilesFuncs do
  @moduledoc """
  Base Functions for User
  """
  import Ecto.Query, warn: false
  alias LogisticService.Repo

  alias LogisticService.Accounts.Models.OrgProfile

  @doc """
  Returns the list of organization_profile.

  ## Examples

      iex> list_organization_profile()
      [%OrgProfile{}, ...]

  """
  def list_organization_profile do
    Repo.all(OrgProfile)
  end

  @doc """
  Gets a single org_profile.

  Raises `Ecto.NoResultsError` if the Org profile does not exist.

  ## Examples

      iex> get_org_profile!(123)
      %OrgProfile{}

      iex> get_org_profile!(456)
      ** (Ecto.NoResultsError)

  """
  def get_org_profile!(id), do: Repo.get!(OrgProfile, id)

  def get_org_profile(user_id: user_id), do: Repo.get_by(OrgProfile, user_id: user_id)

  @doc """
  Creates a org_profile.

  ## Examples

      iex> create_org_profile(%{field: value})
      {:ok, %OrgProfile{}}

      iex> create_org_profile(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_org_profile(attrs \\ %{}) do
    %OrgProfile{}
    |> OrgProfile.changeset(attrs)
    |> Repo.insert_or_update()
  end

  @doc """
  Updates a org_profile.

  ## Examples

      iex> update_org_profile(org_profile, %{field: new_value})
      {:ok, %OrgProfile{}}

      iex> update_org_profile(org_profile, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_org_profile(%OrgProfile{} = org_profile, attrs) do
    org_profile
    |> OrgProfile.changeset(attrs)
    |> Repo.insert_or_update()
  end

  @doc """
  Deletes a org_profile.

  ## Examples

      iex> delete_org_profile(org_profile)
      {:ok, %OrgProfile{}}

      iex> delete_org_profile(org_profile)
      {:error, %Ecto.Changeset{}}

  """
  def delete_org_profile(%OrgProfile{} = org_profile) do
    Repo.delete(org_profile)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking org_profile changes.

  ## Examples

      iex> change_org_profile(org_profile)
      %Ecto.Changeset{data: %OrgProfile{}}

  """
  def change_org_profile(%OrgProfile{} = org_profile, attrs \\ %{}) do
    OrgProfile.changeset(org_profile, attrs)
  end
end
