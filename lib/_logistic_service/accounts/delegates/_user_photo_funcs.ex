defmodule LogisticService.Accounts.Delegates.UserPhotoFuncs do
  @moduledoc false
  import Ecto.Query, warn: false
  alias LogisticService.Repo

  alias LogisticService.Accounts.Models.UserPhoto

  @doc """
  Returns the list of user_photos.

  ## Examples

      iex> list_user_photos()
      [%UserPhoto{}, ...]

  """
  def list_user_photos do
    Repo.all(UserPhoto)
  end

  @doc """
  Gets a single user_photo.

  Raises `Ecto.NoResultsError` if the User photo does not exist.

  ## Examples

      iex> get_user_photo!(123)
      %UserPhoto{}

      iex> get_user_photo!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user_photo!(id), do: Repo.get!(UserPhoto, id)

  @doc """
  Creates a user_photo.

  ## Examples

      iex> create_user_photo(%{field: value})
      {:ok, %UserPhoto{}}

      iex> create_user_photo(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user_photo(attrs \\ %{}) do
    %UserPhoto{}
    |> UserPhoto.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user_photo.

  ## Examples

      iex> update_user_photo(user_photo, %{field: new_value})
      {:ok, %UserPhoto{}}

      iex> update_user_photo(user_photo, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user_photo(%UserPhoto{} = user_photo, attrs) do
    user_photo
    |> UserPhoto.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a user_photo.

  ## Examples

      iex> delete_user_photo(user_photo)
      {:ok, %UserPhoto{}}

      iex> delete_user_photo(user_photo)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user_photo(%UserPhoto{} = user_photo) do
    Repo.delete(user_photo)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user_photo changes.

  ## Examples

      iex> change_user_photo(user_photo)
      %Ecto.Changeset{data: %UserPhoto{}}

  """
  def change_user_photo(%UserPhoto{} = user_photo, attrs \\ %{}) do
    UserPhoto.changeset(user_photo, attrs)
  end
end
