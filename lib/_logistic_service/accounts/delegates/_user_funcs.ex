defmodule LogisticService.Accounts.Delegates.UserFuncs do
  @moduledoc """
  Base Functions for User
  """
  import Ecto.Query, warn: false
  alias LogisticService.Repo

  alias LogisticService.Accounts.Models.User

  alias LogisticService.Accounts.Delegates.{
    RoleFuncs,
    RoleModel,
    Authentication,
    OrganizationProfilesFuncs,
    UserProfilesFuncs
  }

  alias Helper.DBR

  @deleted_users from(u in User, where: not is_nil(u.deleted_at))
  @active_users from(u in User, where: is_nil(u.deleted_at))
  @admin_query from(u in User,
                 join: r in assoc(u, :roles),
                 where: r.slug in ["суперпользователь", "администратор", "логист"],
                 limit: 1
               )
  @courier_query from(u in User, join: r in assoc(u, :roles), where: r.slug in ["курьер"])

  @message_user_only "Изменения может сделать исключительно пользователь с паролем"

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users, do: list_users(true) |> fetch_profile

  def users(filters), do: @active_users |> DBR.find_all(filters) |> fetch_profile
  def all_users(filters), do: User |> DBR.find_all(filters) |> fetch_profile
  def deleted_users(filters), do: @deleted_users |> DBR.find_all(filters) |> fetch_profile
  def list_users(:courier, filters), do: @courier_query |> DBR.find_all(filters) |> fetch_profile

  def count_users,
    do:
      {:ok,
       %{
         count: from(u in User, select: count(u)) |> Repo.one() |> Kernel.||(0),
         dt: nil,
         type: nil
       }}

  def new_users(dt_start, dt_finish, divided) do
    start = format_dt(dt_start)
    finish = format_dt(dt_finish) |> Timex.shift(minutes: 1)

    calc_req(start, finish, divided)
  end

  def calc_req(nil, _finish, _), do: {:error, "invalid date-time"}
  def calc_req(_start, nil, _), do: {:error, "invalid date-time"}

  def calc_req(start, finish, true) do
    res =
      cond do
        Timex.diff(finish, start, :year) > 0 ->
          from(u in User,
            where:
              u.inserted_at >= ^start and
                u.inserted_at < ^finish,
            group_by: fragment("1"),
            select: %{
              dt: fragment("extract(year from ?)::integer", u.inserted_at),
              count: count(u)
            }
          )
          |> put_type("year")

        Timex.diff(finish, start, :month) > 0 ->
          from(u in User,
            where:
              u.inserted_at >= ^start and
                u.inserted_at < ^finish,
            group_by: fragment("1"),
            select: %{
              dt: fragment("extract(month from ?)::integer", u.inserted_at),
              count: count(u)
            }
          )
          |> put_type("month")

        true ->
          from(u in User,
            where:
              u.inserted_at >= ^start and
                u.inserted_at < ^finish,
            group_by: fragment("1"),
            select: %{
              dt: fragment("extract(day from ?)::integer", u.inserted_at),
              count: count(u)
            }
          )
          |> put_type("day")
      end

    {:ok, res}
  end

  def calc_req(start, finish, false) do
    res =
      from(u in User,
        where:
          u.inserted_at >= ^start and
            u.inserted_at < ^finish,
        select: count(u)
      )
      |> Repo.all()

    {:ok, %{count: res, type: "*"}}
  end

  def calc_req(_, _, _), do: {:error, "invalid date-time"}

  def put_type(query, type) do
    case query |> Repo.all() do
      nil -> [%{count: 0, dt: nil, type: nil}]
      map when is_map(map) -> [Map.put(map, :type, type)]
      list when is_list(list) -> Enum.map(list, &Map.put(&1, :type, type))
      _any -> [%{count: 0, dt: nil, type: nil}]
    end
  end

  def format_dt(dt, format \\ "%Y-%m-%d %H:%M:%S") do
    dt =
      case String.length(dt) do
        8 -> "20#{dt} 00:00:00"
        10 -> "#{dt} 00:00:00"
        _dt -> dt
      end

    try do
      Timex.parse!(dt, format, :strftime)
    rescue
      _e -> nil
    end
  end

  @doc """
  Returns the list of users (deleted or not).

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users(true), do: @active_users |> Repo.all() |> fetch_profile
  def list_users(false), do: @deleted_users |> Repo.all() |> fetch_profile
  def list_users(:all), do: User |> Repo.all() |> fetch_profile

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id) |> fetch_profile

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user(123)
      {:ok, %User{}}

      iex> get_user(456)
      {:error, %Ecto.Changeset{}}

  """
  def get_user(id), do: {:ok, Repo.get(User, id)} |> fetch_profile

  def get_user_with_profile(id) do
    User
    |> Repo.get(id)
    |> Repo.preload(:user_profile)
    |> fetch_profile
  end

  def get_user_with_org_profile(id) do
    User
    |> Repo.get(id)
    |> Repo.preload(:org_profile)
    |> fetch_profile
  end

  @doc """
  Gets a single user by phone.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user_by_phone!(phone), do: User |> where(phone: ^phone) |> Repo.one() |> fetch_profile
  def get_user_by_phone(phone), do: User |> DBR.find_by(phone: phone) |> fetch_profile

  def get_unique_users(user_ids) do
    users = user_ids |> MapSet.new() |> Enum.to_list()

    from(u in User, where: u.id in ^users)
    |> Repo.all()
    |> fetch_profile()
  end

  def make_user_org_status(user),
    do: user |> User.changeset(%{is_organization: true}) |> Repo.update!()

  def make_user_user_status(user),
    do: user |> User.changeset(%{is_organization: false}) |> Repo.update!()

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}),
    do: %User{} |> User.changeset(attrs) |> Repo.insert() |> fetch_profile

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

      iex> update_user(user)
      {:ok, %User{}}

      iex> update_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs),
    do: user |> User.update_changeset(attrs) |> Repo.update() |> fetch_profile

  def update_user!(%User{} = user, %{password: _pwd} = attrs),
    do: user |> User.changeset(attrs) |> Repo.update!() |> fetch_profile

  def update_user!(%User{} = user, attrs),
    do: user |> User.update_changeset(attrs) |> Repo.update!() |> fetch_profile

  def update_user(%{id: id, password: _password} = user) do
    case Authentication.verify_password_change(user) do
      true ->
        user =
          with %{roles: roles} <- user do
            id
            |> get_user!()
            |> update_user!(user)
            |> RoleModel.upsert_user_roles(roles)
            |> fetch_profile
          else
            _no_role ->
              id
              |> get_user!()
              |> update_user!(user)
              |> fetch_profile
          end

        {:ok, user}

      false ->
        {:error, @message_user_only}

      e ->
        {:error, e}
    end
  end

  def update_user(%{roles: roles, id: id} = user) do
    id
    |> get_user!()
    |> update_user!(user)
    |> RoleModel.upsert_user_roles(roles)
    |> fetch_profile
  end

  def update_user(%{id: id} = user) do
    id
    |> get_user!()
    |> update_user(user)
    |> fetch_profile
  end

  @doc """
  Updates a user phone date.

  ## Examples

      iex> update_user_phone_date(user)
      {:ok, %User{}}

      iex> update_user_phone_date!(user)
      %User{}

  """
  def update_user_phone_date(%User{} = user),
    do: user |> update_user(%{logged_at: DateTime.utc_now()}) |> fetch_profile

  def update_user_phone_date!(%User{} = user),
    do: user |> update_user!(%{logged_at: DateTime.utc_now()}) |> fetch_profile

  @doc """
  Мягкое удаление Пользователя.

  ## Examples

      iex> soft_delete_user(user)
      {:ok, %User{}}

      iex> soft_delete_user(user)
      {:error, %Ecto.Changeset{}}

      iex> soft_delete_user(user_id)
      {:ok, %User{}}

      iex> soft_delete_user(user_id)
      {:error, _}

  """
  def soft_delete_user(%User{} = user),
    do: user |> update_user(%{deleted_at: DateTime.utc_now()}) |> fetch_profile

  def soft_delete_user(user_id), do: user_id |> get_user!() |> soft_delete_user() |> fetch_profile

  @doc """
  Deletes a user.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def restore_user(%User{} = user), do: user |> update_user(%{deleted_at: nil}) |> fetch_profile
  def restore_user(user_id), do: user_id |> get_user!() |> restore_user() |> fetch_profile

  @doc """
  Deletes a user.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def hard_delete_user(%User{} = user), do: Repo.delete(user) |> fetch_profile

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{data: %User{}}

  """
  def change_user(%User{} = user, attrs \\ %{}), do: User.changeset(user, attrs)

  @doc """
  Создание множества пользователей

  ## Examples

      iex> multisert_users(users)
      %{"1" => %User{}, ..}

  """
  def multisert_users(users) do
    case users |> multisert_users! do
      {:error, changeset} -> {:error, changeset}
      entity -> {:ok, entity}
    end
  end

  def multisert_users!(users) do
    role = RoleFuncs.get_role_by_slug("пользователь")

    result =
      users
      |> Enum.map(fn user ->
        roles =
          case Map.get(user, :roles) do
            nil -> [role]
            roles -> roles
          end

        %User{} |> User.changeset(user) |> User.assoc_roles_changeset(roles)
      end)
      |> Enum.with_index()
      |> Enum.reduce(Ecto.Multi.new(), fn {changeset, index}, acc ->
        Ecto.Multi.insert(acc, Integer.to_string(index), changeset)
      end)
      |> Repo.transaction()

    case result do
      {:ok, entity} -> entity
      {:error, _, changeset, _} -> {:error, changeset}
    end
  end

  def fetch_profile(nil), do: nil

  def fetch_profile(%User{} = user) do
    case Map.get(user, :is_organization) do
      nil ->
        Map.put(user, :profile, nil)

      true ->
        op = OrganizationProfilesFuncs.get_org_profile(user_id: user.id)
        Map.put(user, :profile, op)

      false ->
        up = UserProfilesFuncs.get_profile(user_id: user.id)
        Map.put(user, :profile, up)
    end
  end

  def fetch_profile({:ok, %User{} = user}), do: {:ok, fetch_profile(user)}

  def fetch_profile({:ok, %{entries: ent} = res}),
    do: {:ok, Map.put(res, :entries, fetch_profile(ent))}

  def fetch_profile(list_of_users) when is_list(list_of_users),
    do: list_of_users |> Enum.map(&fetch_profile/1)

  def fetch_profile(other), do: other

  def find_user_with_roles(id), do: User |> DBR.find(id, preload: :roles) |> fetch_profile
  def find_admin_user(id), do: @admin_query |> DBR.find_by(%{id: id}) |> fetch_profile
end
