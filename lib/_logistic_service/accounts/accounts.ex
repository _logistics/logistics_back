defmodule LogisticService.Accounts do
  @moduledoc """
  The Accounts context.
  """
  alias LogisticService.Accounts.Delegates.{
    UserFuncs,
    RoleFuncs,
    RoleModel,
    Authentication,
    UserPhotoFuncs,
    PageSettingsFuncs,
    CommonSettingsFuncs,
    ProfilesManagement,
    UserSmsesFuncs
  }

  defdelegate validate_new(user_id), to: UserSmsesFuncs
  defdelegate remove_old_smses(), to: UserSmsesFuncs

  # user_funcs
  defdelegate list_users(), to: UserFuncs
  defdelegate list_users(bool), to: UserFuncs
  defdelegate list_users(p, filters), to: UserFuncs
  defdelegate get_unique_users(user_ids), to: UserFuncs
  defdelegate users(filters), to: UserFuncs
  defdelegate get_user!(id), to: UserFuncs
  defdelegate get_user(id), to: UserFuncs
  defdelegate get_user_with_profile(id), to: UserFuncs
  defdelegate find_user_with_roles(id), to: UserFuncs
  defdelegate get_user_by_phone!(phone), to: UserFuncs
  defdelegate get_user_by_phone(phone), to: UserFuncs
  defdelegate create_user(attrs \\ %{}), to: UserFuncs
  defdelegate soft_delete_user(user), to: UserFuncs
  defdelegate restore_user(user), to: UserFuncs
  defdelegate hard_delete_user(user), to: UserFuncs
  defdelegate change_user(user, attrs \\ %{}), to: UserFuncs
  defdelegate update_user(user, attrs), to: UserFuncs
  defdelegate update_user(user), to: UserFuncs
  defdelegate multisert_users(users), to: UserFuncs
  defdelegate multisert_users!(users), to: UserFuncs
  defdelegate all_users(filters), to: UserFuncs
  defdelegate deleted_users(filters), to: UserFuncs
  defdelegate make_user_org_status(user), to: UserFuncs
  defdelegate make_user_user_status(user), to: UserFuncs
  defdelegate new_users(dt_start, dt_finish, divided), to: UserFuncs
  defdelegate count_users, to: UserFuncs

  # role_funcs
  defdelegate list_roles(), to: RoleFuncs
  defdelegate get_role!(id), to: RoleFuncs
  defdelegate get_role_by_slug(slug), to: RoleFuncs
  defdelegate create_role(attrs \\ %{}), to: RoleFuncs
  defdelegate delete_role(role), to: RoleFuncs
  defdelegate change_role(role, attrs \\ %{}), to: RoleFuncs
  defdelegate update_role(role, attrs \\ %{}), to: RoleFuncs
  defdelegate multisert_roles(roles), to: RoleFuncs
  defdelegate multisert_roles!(roles), to: RoleFuncs

  # user photo functions
  defdelegate list_user_photos(), to: UserPhotoFuncs
  defdelegate get_user_photo!(id), to: UserPhotoFuncs
  defdelegate create_user_photo(attrs \\ %{}), to: UserPhotoFuncs
  defdelegate delete_user_photo(user_photo), to: UserPhotoFuncs
  defdelegate change_user_photo(user_photo, attrs \\ %{}), to: UserPhotoFuncs
  defdelegate update_user_photo(user_photo, attrs \\ %{}), to: UserPhotoFuncs

  # page settings functions
  defdelegate list_page_settings(), to: PageSettingsFuncs
  defdelegate get_page_setting!(id), to: PageSettingsFuncs
  defdelegate create_page_setting(attrs \\ %{}), to: PageSettingsFuncs
  defdelegate delete_page_setting(page_setting), to: PageSettingsFuncs
  defdelegate change_page_setting(page_setting, attrs \\ %{}), to: PageSettingsFuncs
  defdelegate update_page_setting(page_setting, attrs \\ %{}), to: PageSettingsFuncs

  # common settings functions
  defdelegate list_common_settings(), to: CommonSettingsFuncs
  defdelegate get_common_setting!(id), to: CommonSettingsFuncs
  defdelegate create_common_setting(attrs \\ %{}), to: CommonSettingsFuncs
  defdelegate delete_common_setting(common_setting), to: CommonSettingsFuncs
  defdelegate change_common_setting(common_setting, attrs \\ %{}), to: CommonSettingsFuncs
  defdelegate update_common_setting(common_setting, attrs \\ %{}), to: CommonSettingsFuncs

  # Roles model
  defdelegate upsert_user_roles(user, roles), to: RoleModel

  # Authentication
  defdelegate signin(auth_params), to: Authentication
  defdelegate signed_in?(conn), to: Authentication
  defdelegate is_admin?(user_id), to: Authentication
  defdelegate is_current?(pret_usr, user), to: Authentication
  defdelegate authorize(token), to: Authentication

  defdelegate signup(params), to: Authentication
  defdelegate reset_password(phone), to: Authentication
  defdelegate update_password(phone, code, password, password_confirmation), to: Authentication
  defdelegate submit_signup(phone, code, attrs), to: Authentication

  # User profiles
  defdelegate create_user_profile(user_id, attrs), to: ProfilesManagement
  defdelegate update_user_profile(user_id, attrs), to: ProfilesManagement
  defdelegate delete_user_profile(user_id), to: ProfilesManagement
  defdelegate show_user_profile(user_id), to: ProfilesManagement

  # Org profiles
  defdelegate create_org_profile(user_id, attrs), to: ProfilesManagement
  defdelegate update_org_profile(user_id, attrs), to: ProfilesManagement
  defdelegate delete_org_profile(user_id), to: ProfilesManagement
  defdelegate show_org_profile(user_id), to: ProfilesManagement
end
