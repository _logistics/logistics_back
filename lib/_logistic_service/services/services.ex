defmodule LogisticService.Services do
  @moduledoc """
  The Services context.
  """

  alias LogisticService.Services.Delegates.{
    MenuFuncs,
    PageFuncs
  }

  defdelegate list_menus(), to: MenuFuncs
  defdelegate get_menu!(id), to: MenuFuncs
  defdelegate create_menu(attrs \\ %{}), to: MenuFuncs
  defdelegate delete_menu(site), to: MenuFuncs
  defdelegate change_menu(site, attrs \\ %{}), to: MenuFuncs
  defdelegate update_menu(site, attrs \\ %{}), to: MenuFuncs

  defdelegate list_pages(), to: PageFuncs
  defdelegate get_page!(id), to: PageFuncs
  defdelegate create_page(attrs \\ %{}), to: PageFuncs
  defdelegate delete_page(site), to: PageFuncs
  defdelegate change_page(site, attrs \\ %{}), to: PageFuncs
  defdelegate update_page(site, attrs \\ %{}), to: PageFuncs
end
