defmodule LogisticService.Services.Models.Page do
  @moduledoc false
  alias LogisticService.Services.Models.Page
  use Ecto.Schema
  import Ecto.Changeset

  alias LogisticService.Services.Models.Menu

  @required_fields ~w(name link)a
  @optional_fields ~w(menu_id component avaliable)a

  @type t :: %Page{}
  schema "pages" do
    field :name, :string
    field :component, :string
    field :link, :string
    field :avaliable, :boolean, default: false
    field :menu_id, :id

    belongs_to :menus, Menu
  end

  @doc false
  def changeset(page, attrs) do
    page
    |> update_changeset(attrs)
    |> validate_required(@required_fields)
  end

  @doc false
  def update_changeset(page, attrs) do
    page
    |> cast(attrs, @optional_fields ++ @required_fields)
  end
end
