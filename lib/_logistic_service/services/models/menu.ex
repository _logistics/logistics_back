defmodule LogisticService.Services.Models.Menu do
  @moduledoc false
  alias LogisticService.Services.Models.Menu
  use Ecto.Schema
  import Ecto.Changeset

  alias LogisticService.Accounts.Models.Role
  alias LogisticService.Services.Models.{Page}

  @required_fields ~w(name)a
  @optional_fields ~w(icon menu_id role_id)a

  @type t :: %Menu{}
  schema "menus" do
    field :icon, :string
    field :name, :string
    field :site_id, :id

    belongs_to :roles, Role
    has_many :pages, Page
  end

  @doc false
  def changeset(menu, attrs) do
    menu
    |> update_changeset(attrs)
    |> validate_required(@required_fields)
  end

  @doc false
  def update_changeset(menu, attrs) do
    menu
    |> cast(attrs, @optional_fields ++ @required_fields)
  end
end
