defmodule LogisticService.Dayparcers do
  def get_mth_days(year, mth) do
    today = Date.utc_today()

    case today.month == mth do
      true -> today.day
      false -> Calendar.ISO.days_in_month(year, mth)
    end
  end

  def get_year_mth(year) do
    today = Date.utc_today()

    case today.year == year do
      true -> today.month
      false -> 12
    end
  end

  def year_by_idx(idx) do
    Enum.at(
      [
        "Январь",
        "Февраль",
        "Март",
        "Апрель",
        "Май",
        "Июнь",
        "Июль",
        "Август",
        "Сентябрь",
        "Октябрь",
        "Ноябрь",
        "Декабрь"
      ],
      idx - 1
    )
  end
end
