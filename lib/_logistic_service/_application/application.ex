defmodule LogisticService.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false
    import Cachex.Spec

    children = [
      LogisticService.Repo,
      LogisticServiceWeb.Telemetry,
      LogisticServiceTasker,
      %{
        id: :my_cache_id,
        start:
          {Cachex, :start_link,
           [
             :site_cache,
             [
               limit: limit(size: 5000, policy: Cachex.Policy.LRW, reclaim: 0.1, options: [])
             ]
           ]}
      },
      {Helper.Scheduler, []},
      {Phoenix.PubSub, name: LogisticService.PubSub},
      LogisticServiceWeb.Endpoint
    ]

    opts = [strategy: :one_for_one, name: LogisticService.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def config_change(changed, _new, removed) do
    LogisticServiceWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
