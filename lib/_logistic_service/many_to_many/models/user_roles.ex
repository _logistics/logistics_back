defmodule LogisticService.ManyToMany.Models.UserRoles do
  @moduledoc false
  alias LogisticService.ManyToMany.Models.UserRoles
  use Ecto.Schema
  import Ecto.Changeset

  @required_fields ~w(role_id user_id)a

  @type t :: %UserRoles{}
  schema "user_roles" do
    field :role_id, :id
    field :user_id, :id
  end

  def changeset(model, attrs) do
    model
    |> update_changeset(attrs)
    |> validate_required(@required_fields)
  end

  def update_changeset(model, attrs) do
    model
    |> cast(attrs, @required_fields)
  end
end
