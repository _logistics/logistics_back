defmodule LogisticService.Transfering.Delegates.OrderProducts do
  @moduledoc """
  Menu functions
  """

  import Ecto.Query, warn: false
  alias LogisticService.Repo
  alias LogisticService.Transfering.Models.OrderProducts

  def can_product_be_deleted?(product_id) do
    case OrderProducts |> Repo.get(product_id) do
      nil ->
        {:error, "не существует"}

      order ->
        order |> Map.get(:order_id) |> LogisticService.Transfering.can_order_be_deleted?()
    end
  end

  @doc """
  Returns the list of order_products.

  ## Examples

      iex> list_order_products()
      [%OrderProducts{}, ...]

  """
  def list_order_products do
    Repo.all(OrderProducts)
  end

  @doc """
  Gets a single order_products.

  Raises `Ecto.NoResultsError` if the Order products does not exist.

  ## Examples

      iex> get_order_products!(123)
      %OrderProducts{}

      iex> get_order_products!(456)
      ** (Ecto.NoResultsError)

  """
  def get_order_products!(id), do: Repo.get!(OrderProducts, id)

  @doc """
  Creates a order_products.

  ## Examples

      iex> create_order_products(%{field: value})
      {:ok, %OrderProducts{}}

      iex> create_order_products(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_order_products(products, order) when is_list(products) do
    Enum.map(products, &create_order_products(&1, order))
  end

  def create_order_products(attrs, order) do
    %OrderProducts{}
    |> OrderProducts.changeset(attrs)
    |> Repo.insert!()
    |> Repo.preload(:order)
    |> OrderProducts.put_order(order)
    |> Repo.update!()
  end

  @doc """
  Updates a order_products.

  ## Examples

      iex> update_order_products(order_products, %{field: new_value})
      {:ok, %OrderProducts{}}

      iex> update_order_products(order_products, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_product(product_id, attrs) do
    OrderProducts
    |> Repo.get!(product_id)
    |> OrderProducts.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a order_products.

  ## Examples

      iex> delete_product(order_products)
      {:ok, %OrderProducts{}}

      iex> delete_product(order_products)
      {:error, %Ecto.Changeset{}}

  """
  def delete_product(product_id) do
    OrderProducts
    |> Repo.get(product_id)
    |> case do
      nil -> nil
      %OrderProducts{} = struct -> Repo.delete!(struct)
      _ -> nil
    end
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking order_products changes.

  ## Examples

      iex> change_order_products(order_products)
      %Ecto.Changeset{data: %OrderProducts{}}

  """
  def change_order_products(%OrderProducts{} = order_products, attrs \\ %{}) do
    OrderProducts.changeset(order_products, attrs)
  end
end
