defmodule LogisticService.Transfering.Delegates.OrderTarifs do
  @moduledoc """
  Menu functions
  """

  import Ecto.Query, warn: false
  alias LogisticService.Repo

  alias LogisticService.Transfering.Models.OrderTarif

  @doc """
  Returns the list of order_tarifs.

  ## Examples

      iex> list_order_tarifs()
      [%OrderTarif{}, ...]

  """
  def list_order_tarifs do
    Repo.all(OrderTarif)
  end

  @doc """
  Gets a single order_tarif.

  Raises `Ecto.NoResultsError` if the Order tarif does not exist.

  ## Examples

      iex> get_order_tarif!(123)
      %OrderTarif{}

      iex> get_order_tarif!(456)
      ** (Ecto.NoResultsError)

  """
  def get_order_tarif!(id), do: Repo.get!(OrderTarif, id)
  def get_order_tarif(id), do: Repo.get!(OrderTarif, id)

  @doc """
  Creates a order_tarif.

  ## Examples

      iex> create_order_tarif(%{field: value})
      {:ok, %OrderTarif{}}

      iex> create_order_tarif(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_order_tarif(attrs \\ %{}) do
    %OrderTarif{}
    |> OrderTarif.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a order_tarif.

  ## Examples

      iex> update_order_tarif(order_tarif, %{field: new_value})
      {:ok, %OrderTarif{}}

      iex> update_order_tarif(order_tarif, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_order_tarif(%OrderTarif{} = order_tarif, attrs) do
    order_tarif
    |> OrderTarif.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a order_tarif.

  ## Examples

      iex> delete_order_tarif(order_tarif)
      {:ok, %OrderTarif{}}

      iex> delete_order_tarif(order_tarif)
      {:error, %Ecto.Changeset{}}

  """
  def delete_order_tarif(%OrderTarif{} = order_tarif) do
    Repo.delete(order_tarif)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking order_tarif changes.

  ## Examples

      iex> change_order_tarif(order_tarif)
      %Ecto.Changeset{data: %OrderTarif{}}

  """
  def change_order_tarif(%OrderTarif{} = order_tarif, attrs \\ %{}) do
    OrderTarif.changeset(order_tarif, attrs)
  end

  def multisert_order_tarifs(order_tarifs) do
    case order_tarifs |> multisert_order_tarifs! do
      {:error, changeset} -> {:error, changeset}
      entity -> {:ok, entity}
    end
  end

  def multisert_order_tarifs!(order_tarifs) do
    order_tarifs
    |> Enum.map(&OrderTarif.changeset(%OrderTarif{}, &1))
    |> Enum.with_index()
    |> Enum.reduce(Ecto.Multi.new(), fn {changeset, index}, acc ->
      Ecto.Multi.insert(acc, Integer.to_string(index), changeset)
    end)
    |> Repo.transaction()
    |> case do
      {:ok, entity} -> entity
      {:error, _, changeset, _} -> {:error, changeset}
    end
  end
end
