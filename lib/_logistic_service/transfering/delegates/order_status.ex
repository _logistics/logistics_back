defmodule LogisticService.Transfering.Delegates.OrderStatus do
  @moduledoc """
  Menu functions
  """

  import Ecto.Query, warn: false
  alias LogisticService.Repo

  alias LogisticService.Transfering.Models.OrderStatus

  @doc """
  Returns the list of order_statuses.

  ## Examples

      iex> list_order_statuses()
      [%OrderStatus{}, ...]

  """
  def list_order_statuses do
    Repo.all(OrderStatus)
  end

  @doc """
  Gets a single order_status.

  Raises `Ecto.NoResultsError` if the Order status does not exist.

  ## Examples

      iex> get_order_status!(123)
      %OrderStatus{}

      iex> get_order_status!(456)
      ** (Ecto.NoResultsError)

  """
  def get_order_status!(name: name),
    do: from(os in OrderStatus, where: ilike(os.name, ^"%#{name}%"), limit: 1) |> Repo.one()

  def get_order_status!(id), do: Repo.get!(OrderStatus, id)
  def get_order_status(id), do: Repo.get(OrderStatus, id)

  @doc """
  Creates a order_status.

  ## Examples

      iex> create_order_status(%{field: value})
      {:ok, %OrderStatus{}}

      iex> create_order_status(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_order_status(attrs \\ %{}) do
    %OrderStatus{}
    |> OrderStatus.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a order_status.

  ## Examples

      iex> update_order_status(order_status, %{field: new_value})
      {:ok, %OrderStatus{}}

      iex> update_order_status(order_status, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_order_status(%OrderStatus{} = order_status, attrs) do
    order_status
    |> OrderStatus.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a order_status.

  ## Examples

      iex> delete_order_status(order_status)
      {:ok, %OrderStatus{}}

      iex> delete_order_status(order_status)
      {:error, %Ecto.Changeset{}}

  """
  def delete_order_status(%OrderStatus{} = order_status) do
    Repo.delete(order_status)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking order_status changes.

  ## Examples

      iex> change_order_status(order_status)
      %Ecto.Changeset{data: %OrderStatus{}}

  """
  def change_order_status(%OrderStatus{} = order_status, attrs \\ %{}) do
    OrderStatus.changeset(order_status, attrs)
  end

  def multisert_order_statuses(order_statuses) do
    case order_statuses |> multisert_order_statuses! do
      {:error, changeset} -> {:error, changeset}
      entity -> {:ok, entity}
    end
  end

  def multisert_order_statuses!(order_statuses) do
    order_statuses
    |> Enum.map(&OrderStatus.changeset(%OrderStatus{}, &1))
    |> Enum.with_index()
    |> Enum.reduce(Ecto.Multi.new(), fn {changeset, index}, acc ->
      Ecto.Multi.insert(acc, Integer.to_string(index), changeset)
    end)
    |> Repo.transaction()
    |> case do
      {:ok, entity} -> entity
      {:error, _, changeset, _} -> {:error, changeset}
    end
  end
end
