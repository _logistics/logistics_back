defmodule LogisticService.Transfering.Delegates.Order do
  @moduledoc """
  Menu functions
  """

  import Ecto.Query, warn: false
  import ShorterMaps
  alias Helper.DBR
  alias LogisticService.{Repo, Accounts}
  alias LogisticService.Transfering.Models.{Order, OrderProducts, OrderStatus}
  alias LogisticService.Transfering.Delegates.OrderProducts, as: OP
  import LogisticService.Accounts.Delegates.UserFuncs, only: [fetch_profile: 1]

  @valid_pms ~w(receiver_address receiver_fio receiver_phone receiver_town
                sender_address sender_fio sender_phone sender_town sender_id
                delivery_awaited_datetime)a

  def get_all_time_report(nil) do
    now = NaiveDateTime.local_now() |> Timex.shift(days: 1)

    first_order =
      try do
        Order |> first(:inserted_at) |> select([o], o.inserted_at) |> Repo.one()
      rescue
        _e -> now
      end
      |> Timex.shift(days: -1)

    first_day = Date.new!(first_order.year, first_order.month, first_order.day)
    last_day = Date.new!(now.year, now.month, now.day)

    res =
      from(o in Order,
        where:
          fragment("?::date", o.inserted_at) >= ^first_day and
            fragment("?::date", o.inserted_at) <= ^last_day,
        group_by: fragment("date_part('year', ?)::integer", o.inserted_at),
        order_by: fragment("date_part('year', ?)::integer", o.inserted_at),
        select: %{
          fragment("date_part('year', ?)::integer", o.inserted_at) => %{
            count: count(o),
            inserted: fragment("date_part('year', ?)::integer", o.inserted_at),
            price: sum(o.price),
            list: fragment("to_jsonb(array_agg(?))", o)
          }
        }
      )
      |> Repo.all()
      |> Enum.reduce(%{}, fn x, acc -> Map.merge(acc, x) end)

    order_statuses = LogisticService.Transfering.list_order_statuses()

    first_day.year..last_day.year
    |> Enum.map(fn dt ->
      case Map.get(res, dt) do
        nil ->
          %{
            count: 0,
            inserted: LogisticService.Dayparcers.year_by_idx(dt),
            price: Decimal.new(0),
            order_list: []
          }

        %{count: count, inserted: inserted, price: price, list: list} ->
          order_list =
            morphyfy(list)
            |> Enum.map(fn
              %{order_status_id: nil} = order ->
                order |> Map.put(:order_status, nil)

              %{order_status_id: osid} = order ->
                os = Enum.find(order_statuses, &(&1.id === osid)) |> Map.get(:name)
                order |> Map.put(:order_status, os)
            end)

          %{count: count, inserted: inserted, price: price, order_list: order_list}
      end
    end)
    |> get_statistic_info()
  end

  def get_all_time_report(user_id) do
    now = NaiveDateTime.local_now() |> Timex.shift(days: 1)

    first_order =
      try do
        Order |> first(:inserted_at) |> select([o], o.inserted_at) |> Repo.one()
      rescue
        _e -> now
      end
      |> Timex.shift(days: -1)

    first_day = Date.new!(first_order.year, first_order.month, first_order.day)
    last_day = Date.new!(now.year, now.month, now.day)

    res =
      from(o in Order,
        where:
          o.sender_id == ^user_id and fragment("?::date", o.inserted_at) >= ^first_day and
            fragment("?::date", o.inserted_at) <= ^last_day,
        group_by: fragment("date_part('year', ?)::integer", o.inserted_at),
        order_by: fragment("date_part('year', ?)::integer", o.inserted_at),
        select: %{
          fragment("date_part('year', ?)::integer", o.inserted_at) => %{
            count: count(o),
            inserted: fragment("date_part('year', ?)::integer", o.inserted_at),
            price: sum(o.price),
            list: fragment("to_jsonb(array_agg(?))", o)
          }
        }
      )
      |> Repo.all()
      |> Enum.reduce(%{}, fn x, acc -> Map.merge(acc, x) end)

    order_statuses = LogisticService.Transfering.list_order_statuses()

    first_day.year..last_day.year
    |> Enum.map(fn dt ->
      case Map.get(res, dt) do
        nil ->
          %{
            count: 0,
            inserted: LogisticService.Dayparcers.year_by_idx(dt),
            price: Decimal.new(0),
            order_list: []
          }

        %{count: count, inserted: inserted, price: price, list: list} ->
          order_list =
            morphyfy(list)
            |> Enum.map(fn
              %{order_status_id: nil} = order ->
                order |> Map.put(:order_status, nil)

              %{order_status_id: osid} = order ->
                os = Enum.find(order_statuses, &(&1.id === osid)) |> Map.get(:name)
                order |> Map.put(:order_status, os)
            end)

          %{count: count, inserted: inserted, price: price, order_list: order_list}
      end
    end)
    |> get_statistic_info()
  end

  def get_year_report(nil, year) do
    first_day = Date.new!(year, 1, 1)
    last_day = Date.new!(year, 12, 31)

    res =
      from(o in Order,
        where:
          fragment("?::date", o.inserted_at) >= ^first_day and
            fragment("?::date", o.inserted_at) <= ^last_day,
        group_by: fragment("date_part('month', ?)::integer", o.inserted_at),
        order_by: fragment("date_part('month', ?)::integer", o.inserted_at),
        select: %{
          fragment("date_part('month', ?)::integer", o.inserted_at) => %{
            count: count(o),
            inserted: fragment("date_part('month', ?)::integer", o.inserted_at),
            price: sum(o.price),
            list: fragment("to_jsonb(array_agg(?))", o)
          }
        }
      )
      |> Repo.all()
      |> Enum.reduce(%{}, fn x, acc -> Map.merge(acc, x) end)

    order_statuses = LogisticService.Transfering.list_order_statuses()

    1..LogisticService.Dayparcers.get_year_mth(year)
    |> Enum.map(fn dt ->
      case Map.get(res, dt) do
        nil ->
          %{
            count: 0,
            inserted: LogisticService.Dayparcers.year_by_idx(dt),
            price: Decimal.new(0),
            order_list: []
          }

        %{count: count, inserted: inserted, price: price, list: list} ->
          order_list =
            morphyfy(list)
            |> Enum.map(fn
              %{order_status_id: nil} = order ->
                order |> Map.put(:order_status, nil)

              %{order_status_id: osid} = order ->
                os = Enum.find(order_statuses, &(&1.id === osid)) |> Map.get(:name)
                order |> Map.put(:order_status, os)
            end)

          %{
            count: count,
            inserted: LogisticService.Dayparcers.year_by_idx(inserted),
            price: price,
            order_list: order_list
          }
      end
    end)
    |> get_statistic_info()
  end

  def get_year_report(user_id, year) do
    first_day = Date.new!(year, 1, 1)
    last_day = Date.new!(year, 12, 31)

    res =
      from(o in Order,
        where:
          o.sender_id == ^user_id and fragment("?::date", o.inserted_at) >= ^first_day and
            fragment("?::date", o.inserted_at) <= ^last_day,
        group_by: fragment("date_part('month', ?)::integer", o.inserted_at),
        order_by: fragment("date_part('month', ?)::integer", o.inserted_at),
        select: %{
          fragment("date_part('month', ?)::integer", o.inserted_at) => %{
            count: count(o),
            inserted: fragment("date_part('month', ?)::integer", o.inserted_at),
            price: sum(o.price),
            list: fragment("to_jsonb(array_agg(?))", o)
          }
        }
      )
      |> Repo.all()
      |> Enum.reduce(%{}, fn x, acc -> Map.merge(acc, x) end)

    order_statuses = LogisticService.Transfering.list_order_statuses()

    1..LogisticService.Dayparcers.get_year_mth(year)
    |> Enum.map(fn dt ->
      case Map.get(res, dt) do
        nil ->
          %{
            count: 0,
            inserted: LogisticService.Dayparcers.year_by_idx(dt),
            price: Decimal.new(0),
            order_list: []
          }

        %{count: count, inserted: inserted, price: price, list: list} ->
          order_list =
            morphyfy(list)
            |> Enum.map(fn
              %{order_status_id: nil} = order ->
                order |> Map.put(:order_status, nil)

              %{order_status_id: osid} = order ->
                os = Enum.find(order_statuses, &(&1.id === osid)) |> Map.get(:name)
                order |> Map.put(:order_status, os)
            end)

          %{
            count: count,
            inserted: LogisticService.Dayparcers.year_by_idx(inserted),
            price: price,
            order_list: order_list
          }
      end
    end)
    |> get_statistic_info()
  end

  def get_mth_report(nil, year, mth) do
    first_day = Date.new!(year, mth, 1)
    last_day = Date.new!(year, mth, LogisticService.Dayparcers.get_mth_days(year, mth))

    res =
      from(o in Order,
        where:
          fragment("?::date", o.inserted_at) >= ^first_day and
            fragment("?::date", o.inserted_at) <= ^last_day,
        group_by: fragment("?::date", o.inserted_at),
        order_by: fragment("?::date", o.inserted_at),
        select: %{
          fragment("?::date", o.inserted_at) => %{
            count: count(o),
            inserted: fragment("?::date", o.inserted_at),
            price: sum(o.price),
            list: fragment("to_jsonb(array_agg(?))", o)
          }
        }
      )
      |> Repo.all()
      |> Enum.reduce(%{}, fn x, acc -> Map.merge(acc, x) end)

    order_statuses = LogisticService.Transfering.list_order_statuses()

    1..LogisticService.Dayparcers.get_mth_days(year, mth)
    |> Enum.map(&Date.new!(year, mth, &1))
    |> Enum.map(fn dt ->
      case Map.get(res, dt) do
        nil ->
          %{count: 0, inserted: dt, price: Decimal.new(0), order_list: []}

        %{count: count, inserted: inserted, price: price, list: list} ->
          order_list =
            morphyfy(list)
            |> Enum.map(fn
              %{order_status_id: nil} = order ->
                order |> Map.put(:order_status, nil)

              %{order_status_id: osid} = order ->
                os = Enum.find(order_statuses, &(&1.id === osid)) |> Map.get(:name)
                order |> Map.put(:order_status, os)
            end)

          %{count: count, inserted: inserted, price: price, order_list: order_list}
      end
    end)
    |> get_statistic_info()
  end

  def get_mth_report(user_id, year, mth) do
    first_day = Date.new!(year, mth, 1)
    last_day = Date.new!(year, mth, LogisticService.Dayparcers.get_mth_days(year, mth))

    res =
      from(o in Order,
        where:
          o.sender_id == ^user_id and fragment("?::date", o.inserted_at) >= ^first_day and
            fragment("?::date", o.inserted_at) <= ^last_day,
        group_by: fragment("?::date", o.inserted_at),
        order_by: fragment("?::date", o.inserted_at),
        select: %{
          fragment("?::date", o.inserted_at) => %{
            count: count(o),
            inserted: fragment("?::date", o.inserted_at),
            price: sum(o.price),
            list: fragment("to_jsonb(array_agg(?))", o)
          }
        }
      )
      |> Repo.all()
      |> Enum.reduce(%{}, fn x, acc -> Map.merge(acc, x) end)

    order_statuses = LogisticService.Transfering.list_order_statuses()

    1..LogisticService.Dayparcers.get_mth_days(year, mth)
    |> Enum.map(&Date.new!(year, mth, &1))
    |> Enum.map(fn dt ->
      case Map.get(res, dt) do
        nil ->
          %{count: 0, inserted: dt, price: Decimal.new(0), order_list: []}

        %{count: count, inserted: inserted, price: price, list: list} ->
          order_list =
            morphyfy(list)
            |> Enum.map(fn
              %{order_status_id: nil} = order ->
                order |> Map.put(:order_status, nil)

              %{order_status_id: osid} = order ->
                os = Enum.find(order_statuses, &(&1.id === osid)) |> Map.get(:name)
                order |> Map.put(:order_status, os)
            end)

          %{count: count, inserted: inserted, price: price, order_list: order_list}
      end
    end)
    |> get_statistic_info()
  end

  def get_statistic_info(list) do
    list
    |> Enum.map(fn %{count: count, inserted: inserted, price: price, order_list: order_list} ->
      payed = order_list |> Enum.filter(&is_os(&1, "Заказ оплачен"))
      payed_count = Enum.count(payed)
      payed_sum = Enum.reduce(payed, 0, fn %{price: price}, acc -> acc + price end)

      not_payed =
        order_list
        |> Enum.filter(&(is_os(&1, "Заказ не оплачен") || is_os(&1, "Заказ доставлен")))

      not_payed_count = Enum.count(not_payed)
      not_payed_sum = Enum.reduce(not_payed, 0, fn %{price: price}, acc -> acc + price end)

      not_delivered =
        order_list
        |> Enum.filter(
          &(is_os(&1, "Заказ не доставлен") || is_os(&1, "Есть потери") ||
              is_os(&1, "Заказ отменен") || is_os(&1, "Возврат товара"))
        )

      not_delivered_count = Enum.count(not_delivered)

      not_delivered_sum =
        Enum.reduce(not_delivered, 0, fn %{price: price}, acc -> acc + price end)

      %{
        count: count,
        inserted: inserted,
        price: price,
        order_list: order_list,
        payed_count: payed_count,
        payed_sum: payed_sum,
        not_payed_count: not_payed_count,
        not_payed_sum: not_payed_sum,
        not_delivered_count: not_delivered_count,
        not_delivered_sum: not_delivered_sum
      }
    end)
  end

  def is_os(%{order_status: order_status}, status) do
    dk(order_status) === dk(status)
  end

  def dk(nil), do: nil
  def dk(string), do: String.downcase(string)

  def get_last_incompleted(user_id) do
    from(o in Order,
      where: o.sender_id == ^user_id and (is_nil(o.order_status_id) or o.can_be_sent == false),
      limit: 1
    )
    |> Repo.one()
    |> get_one_c
  end

  def can_order_be_deleted?(order_id) do
    case Order |> Repo.get(order_id) do
      nil ->
        {:error, "не существует"}

      order ->
        status = order |> Map.get(:order_status_id)
        if is_nil(status) || status < 2, do: :ok, else: :too_late
    end
  end

  @doc """
  Returns the list of orders.

  ## Examples

      iex> list_orders()
      [%Order{}, ...]

  """

  def list_orders(:all, filters) do
    DBR.find_all(Order, filters)
    |> elem(1)
    |> get_one_c
  end

  def list_orders(:all, filters, user_id: user_id) do
    from(o in Order, where: o.sender_id == ^user_id)
    |> DBR.find_all(filters)
    |> elem(1)
    |> get_one_c
  end

  def list_orders(sender_id: user_id, criteries: criteries) do
    from(o in Order, where: o.sender_id == ^user_id and is_nil(o.deleted_at))
    |> DBR.find_all(criteries)
    |> elem(1)
    |> get_one_c
  end

  def list_orders(courier_id: user_id, criteries: criteries) do
    from(o in Order, where: o.courier_id == ^user_id and is_nil(o.deleted_at))
    |> DBR.find_all(criteries)
    |> elem(1)
    |> get_one_c
  end

  def list_orders(sender_id: user_id, start_dt: start_dt, end_dt: end_dt) do
    end_dt =
      with end_dt <- Timex.parse!(end_dt, "{ISO:Extended}"),
           start_dt <- Timex.parse!(start_dt, "{ISO:Extended}"),
           0 <- Timex.diff(start_dt, end_dt) do
        Timex.shift(end_dt, minutes: 1)
      else
        _e -> end_dt
      end

    from(o in Order,
      where: o.sender_id == ^user_id and o.inserted_at >= ^start_dt and o.inserted_at < ^end_dt
    )
    |> Repo.all()
    |> get_one_c
  end

  def list_orders(start_dt: start_dt, end_dt: end_dt) do
    end_dt =
      with end_dt <- Timex.parse!(end_dt, "{ISO:Extended}"),
           start_dt <- Timex.parse!(start_dt, "{ISO:Extended}"),
           0 <- Timex.diff(start_dt, end_dt) do
        Timex.shift(end_dt, minutes: 1)
      else
        _e -> end_dt
      end

    from(o in Order, where: o.inserted_at >= ^start_dt and o.inserted_at < ^end_dt)
    |> Repo.all()
    |> get_one_c
  end

  def list_orders(filters) do
    from(o in Order, where: is_nil(o.deleted_at))
    |> DBR.find_all(filters)
    |> elem(1)
    |> get_one_c
  end

  def get_one_c({:ok, order}), do: get_one_c(order)
  def get_one_c({:error, any}), do: {:error, any}

  def get_one_c(%{entries: entries} = orders),
    do: Map.merge(orders, %{entries: get_one_c(entries)})

  def get_one_c(orders) when is_list(orders), do: Enum.map(orders, &get_one_c/1)
  def get_one_c(%{one_c_data: nil} = order), do: order

  def get_one_c(%{one_c_data: one_c_data} = order),
    do: Map.merge(order, %{one_c_data: one_c_data |> Morphix.atomorphiform!()})

  def get_one_c(any), do: any

  def crate_order(user, %{}) do
    create_order!(%{})
    |> Repo.preload(:sender)
    |> Order.put_sender(user)
    |> Repo.update!()
    |> get_one_c
  end

  def create_order(sender_id, attrs) when is_integer(sender_id) do
    user = Accounts.get_user!(sender_id)
    create_order(user, attrs)
  end

  def create_order(user, attrs) do
    osi = Map.get(attrs, :order_status_id)
    oti = Map.get(attrs, :order_tarif_id)
    pti = Map.get(attrs, :payment_type_id)
    prds = Map.get(attrs, :products)

    attrs
    |> create_order!()
    |> Repo.preload([:order_status, :sender, :order_tarif, :payment_type])
    |> Order.put_sender(user)
    |> Order.put_order_status(osi)
    |> Order.put_order_tarif(oti)
    |> Order.put_payment_type(pti)
    |> Repo.update!()
    |> put_products(prds)
    |> validate_order()
    |> get_one_c
  end

  def validate_order(order) do
    is_valid = true

    have_params = Enum.reduce(@valid_pms, true, &(!is_nil(Map.get(order, &1)) && &2))

    products_count =
      from(op in OrderProducts, where: op.order_id == ^order.id, select: count(op.id))
      |> Repo.one()

    can_be_sent = !is_nil(products_count) && products_count > 0 && have_params

    order
    |> Order.validate(~M{is_valid, can_be_sent})
    |> Repo.update!()
    |> get_one_c
  end

  def put_products(order_id, product) when is_integer(order_id) when is_map(product) do
    order = Order |> Repo.get!(order_id)
    OP.create_order_products(product, order)
  end

  def put_products(order, products) do
    case products do
      [] ->
        order

      nil ->
        order

      products ->
        OP.create_order_products(products, order)

        order
        |> Repo.preload(:products)
    end
  end

  @doc """
  Gets a single order.

  Raises `Ecto.NoResultsError` if the Order does not exist.

  ## Examples

      iex> get_order!(123)
      %Order{}

      iex> get_order!(456)
      ** (Ecto.NoResultsError)

  """
  def get_order!(:all, id) do
    case Repo.get(Order, id) do
      order when is_struct(order) -> order
      _ -> nil
    end
    |> get_one_c
  end

  def get_order!(id) do
    from(o in Order, where: is_nil(o.deleted_at))
    |> Repo.get(id)
    |> get_one_c
  end

  def get_order_with_preloads(id) do
    Order
    |> Repo.get!(id)
    |> case do
      nil ->
        nil

      order ->
        order
        |> Repo.preload(:products)
        |> Repo.preload(:sender)
        |> Repo.preload(:courier)
        |> get_one_c
    end
  end

  @doc """
  Creates a order.

  ## Examples

      iex> create_order(%{field: value})
      {:ok, %Order{}}

      iex> create_order(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_order(attrs \\ %{}) do
    %Order{}
    |> Order.changeset(attrs)
    |> Repo.insert()
  end

  def create_order!(attrs \\ %{}) do
    %Order{}
    |> Order.changeset(attrs)
    |> Repo.insert!()
  end

  def set_order_courier(order_id, courier_id) do
    courier = Accounts.get_user!(courier_id)

    Order
    |> Repo.get!(order_id)
    |> Order.put_courier(courier)
    |> Repo.update()
  end

  @doc """
  Updates a order.

  ## Examples

      iex> update_order(order, %{field: new_value})
      {:ok, %Order{}}

      iex> update_order(order, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_order(order_id, attrs) do
    Order
    |> Repo.get!(order_id)
    |> Order.changeset(attrs)
    |> Repo.update()
  end

  def update_order(user, order_id, %{products: prds} = attrs) do
    osi = Map.get(attrs, :order_status_id)
    oti = Map.get(attrs, :order_tarif_id)
    pti = Map.get(attrs, :payment_type_id)

    delete_products(order_id)

    upd_order(order_id, user, osi, oti, pti, attrs)
    |> put_products(prds)
    |> validate_order()
    |> get_one_c
  end

  def update_order(user, order_id, attrs) do
    osi = Map.get(attrs, :order_status_id)
    oti = Map.get(attrs, :order_tarif_id)
    pti = Map.get(attrs, :payment_type_id)

    upd_order(order_id, user, osi, oti, pti, attrs)
    |> validate_order()
    |> get_one_c
  end

  def upd_order(order_id, user, osi, oti, pti, attrs \\ %{}) do
    Order
    |> Repo.get!(order_id)
    |> Repo.preload([:order_status, :sender, :order_tarif, :payment_type])
    |> Order.changeset(attrs)
    |> case do
      %Ecto.Changeset{} = res -> Repo.update!(res)
      res -> res
    end
    |> Order.put_updater(user)
    |> Order.put_order_status(osi)
    |> Order.put_order_tarif(oti)
    |> Order.put_payment_type(pti)
    |> case do
      %Ecto.Changeset{} = res -> Repo.update!(res)
      res -> res
    end
  end

  def delete_products(order_id) when is_integer(order_id) do
    from(p in OrderProducts, where: p.order_id == ^order_id)
    |> Repo.delete_all()
  end

  def delete_products([]) do
    :ok
  end

  def delete_products(order_id) when is_list(order_id) do
    from(p in OrderProducts, where: p.order_id in ^order_id)
    |> Repo.delete_all()
  end

  def delete_order(:force, order_id) do
    delete_products(order_id)

    order = Repo.get!(Order, order_id)

    Repo.delete!(order)

    order
  end

  @doc """
  Deletes a order.

  ## Examples

      iex> delete_order(order)
      {:ok, %Order{}}

      iex> delete_order(order)
      {:error, %Ecto.Changeset{}}

  """
  def delete_order(order_id, user) do
    Order
    |> Repo.get!(order_id)
    |> Repo.preload(:deleted_by)
    |> Order.delete_changeset(user)
    |> Ecto.Changeset.put_assoc(:deleted_by, user)
    |> Repo.update!()
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking order changes.

  ## Examples

      iex> change_order(order)
      %Ecto.Changeset{data: %Order{}}

  """
  def change_order(%Order{} = order, attrs \\ %{}) do
    Order.changeset(order, attrs)
  end

  def prepare_products(products) do
    products
    |> Enum.map(fn %{id: id, name: name, price: price, count: count} ->
      %{id: id, name: name, price: price, count: count}
    end)
  end

  def update_status_at_order(order_id, status_id) do
    Order
    |> Repo.get(order_id)
    |> Order.put_order_status(status_id)
    |> case do
      %Ecto.Changeset{} = res -> Repo.update!(res)
      res -> res
    end
  end

  def force_validate(order_id) do
    Order
    |> Repo.get(order_id)
    |> Order.validate(%{is_valid: true, can_be_sent: true})
    |> Repo.update!()
  end

  def get_from_one_c(id) do
    addr = "https://terminal.scloud.ru/06/sc57108_base001/hs/api/act/#{id}"

    headers = [
      {"Authorization", "Basic #{Base.encode64("sc57108:Daria22031819")}"},
      {"content-type", "application/json"}
    ]

    case Tesla.get(addr, headers: headers) do
      {:ok, %{body: body, status: 200}} ->
        body

      {:error, _msg} = msg ->
        msg
    end
  end

  def confirm_order_and_send(order_id) do
    order = Order |> Repo.get(order_id)

    case Map.get(order, :can_be_sent) do
      true ->
        status_id =
          from(os in OrderStatus, where: os.name == "Подтвержден", limit: 1, select: os.id)
          |> Repo.one()

        order
        |> Order.put_order_status(status_id)
        |> case do
          %Ecto.Changeset{} = res -> Repo.update!(res)
          res -> res
        end

        send_prepared_order(order_id)

      nil ->
        {:error, "Проверьте поля заказа"}

      false ->
        {:error, "Заказ не достаточно заполнен"}
    end
  end

  def send_prepared_order(id) do
    case watch_prepared_order(id) do
      {:error, _any} = order ->
        order

      order ->
        addr = "https://terminal.scloud.ru/06/sc57108_base001/hs/api/act/"

        headers = [
          {"Authorization", "Basic #{Base.encode64("sc57108:Daria22031819")}"},
          {"content-type", "application/json"}
        ]

        data = Poison.encode!(%{order: order})

        case Tesla.post(addr, data, headers: headers) do
          {:ok, %{body: body, status: 200}} ->
            body =
              case Poison.decode!(body) |> Morphix.atomorphiform!() do
                %{response: nil, error: err} ->
                  if String.match?(err, ~r/exist/) do
                    Tesla.put!(addr, data, headers: headers)
                    |> Map.get(:body)
                    |> Poison.decode!()
                    |> Morphix.atomorphiform!()
                  else
                    %{error: inspect(%{error: "not sended"})}
                  end

                %{error: nil} = resp ->
                  resp

                resp ->
                  %{error: "not sended, #{resp}"}
              end

            update_order(id, %{one_c_data: body})
            body

          {:ok, %{body: body, status: st}} when st !== 200 ->
            {:error, "Ошибка сервера 1с. #{body}"}

          {:error, _any} = msg ->
            msg

          _any ->
            {:error, "Ошибка сервера 1с. Отменена передача данных."}
        end
    end
  end

  def watch_prepared_order(order_id) do
    order = Order |> Repo.get(order_id) |> Repo.preload([:sender, :products])
    order_status = OrderStatus |> Repo.get_by(name: "Подтвержден")
    order_status_id = Map.get(order, :order_status_id)

    cond do
      !is_nil(order_status_id) && order_status_id >= Map.get(order_status, :id) ->
        %{
          id: id,
          price: price,
          products: products,
          sender_id: sender_id,
          sender: sender,
          sender_fio: sender_fio,
          sender_phone: sender_phone,
          updated_at: updated_at,
          delivery_awaited_datetime: shipment_date,
          sender_comment: comments
        } = order

        receiver_fio = Map.get(order, :receiver_fio)
        receiver_phone = Map.get(order, :receiver_phone)
        receiver_address = Map.get(order, :receiver_address)
        receiver_comment = Map.get(order, :receiver_comment)

        sender_profile =
          sender
          |> fetch_profile()
          |> Map.get(:profile)
          |> case do
            nil -> %{}
            any -> any
          end

        %{
          id: id,
          orderSum: price,
          comment: comments,
          shipmentDate: fmt_dt(shipment_date),
          createDate: fmt_dt(updated_at),
          client: %{
            id: sender_id,
            name: sender_fio,
            isOrganization: !!Map.get(sender, :is_organization),
            isOrganisation: !!Map.get(sender, :is_organization),
            inn: Map.get(sender_profile, :inn),
            kpp: Map.get(sender_profile, :kpp),
            contacts: %{
              phone: sender_phone,
              email: Map.get(sender_profile, :mail)
            },
            bank: %{
              bik: Map.get(sender_profile, :bik)
            }
          },
          address: receiver_address,
          contactPerson: %{
            id: id,
            name: receiver_fio,
            phone: receiver_phone,
            address: receiver_address,
            email: receiver_comment
          },
          products: prepare_products(products)
        }

      !is_nil(order_status_id) ->
        status =
          from(os in OrderStatus, where: os.id == ^order_status_id, select: os.name) |> Repo.one()

        {:error, "Текущий статус заказа - #{status}. Требуется статус - \"Подтверждение\"."}

      true ->
        {:error, "Заказ должен быть полностью заполнен и статус должен быть подтвержден"}
    end
  end

  def calc_summ_res(nil, _, _, _, _), do: {:error, "invalid date-time"}
  def calc_summ_res(_, nil, _, _, _), do: {:error, "invalid date-time"}

  def calc_summ_res(start, finish, user_id, true) do
    res =
      cond do
        Timex.diff(finish, start, :year) > 0 ->
          from(o in Order,
            where:
              o.inserted_at >= ^start and
                o.inserted_at < ^finish and
                o.sender_id == ^user_id,
            group_by: fragment("1"),
            select: %{
              dt: fragment("extract(year from ?)::integer", o.inserted_at),
              count: fragment("sum(?)::integer", o.price)
            }
          )
          |> put_type("year")

        Timex.diff(finish, start, :month) > 0 ->
          from(o in Order,
            where:
              o.inserted_at >= ^start and
                o.inserted_at < ^finish and
                o.sender_id == ^user_id,
            group_by: fragment("1"),
            select: %{
              dt: fragment("extract(month from ?)::integer", o.inserted_at),
              count: fragment("sum(?)::integer", o.price)
            }
          )
          |> put_type("month")

        true ->
          from(o in Order,
            where:
              o.inserted_at >= ^start and
                o.inserted_at < ^finish and
                o.sender_id == ^user_id,
            group_by: fragment("1"),
            select: %{
              dt: fragment("extract(day from ?)::integer", o.inserted_at),
              count: fragment("sum(?)::integer", o.price)
            }
          )
          |> put_type("day")
      end

    {:ok, res}
  end

  def calc_summ_res(start, finish, user_id, false) do
    res =
      from(o in Order,
        where:
          o.inserted_at >= ^start and
            o.inserted_at < ^finish and
            o.sender_id == ^user_id,
        select: fragment("sum(?)::integer", o.price)
      )
      |> put_type("*")

    {:ok, res}
  end

  def calc_summ_res(start, finish, nil, false) do
    res =
      from(o in Order,
        where:
          o.inserted_at >= ^start and
            o.inserted_at < ^finish,
        select: fragment("sum(?)::integer", o.price)
      )
      |> put_type("*")

    {:ok, res}
  end

  def calc_summ_res(start, finish, nil, true) do
    res =
      cond do
        Timex.diff(finish, start, :year) > 0 ->
          from(o in Order,
            where:
              o.inserted_at >= ^start and
                o.inserted_at < ^finish,
            group_by: fragment("1"),
            select: %{
              dt: fragment("extract(year from ?)::integer", o.inserted_at),
              count: fragment("sum(?)::integer", o.price)
            }
          )
          |> put_type("year")

        Timex.diff(finish, start, :month) > 0 ->
          from(o in Order,
            where:
              o.inserted_at >= ^start and
                o.inserted_at < ^finish,
            group_by: fragment("1"),
            select: %{
              dt: fragment("extract(month from ?)::integer", o.inserted_at),
              count: fragment("sum(?)::integer", o.price)
            }
          )
          |> put_type("month")

        true ->
          from(o in Order,
            where:
              o.inserted_at >= ^start and
                o.inserted_at < ^finish,
            group_by: fragment("1"),
            select: %{
              dt: fragment("extract(day from ?)::integer", o.inserted_at),
              count: fragment("sum(?)::integer", o.price)
            }
          )
          |> put_type("day")
      end

    {:ok, res}
  end

  def calc_summ_res(_, _, _, _), do: {:error, "invalid date-time"}

  def count_order_summ(dt_start, dt_finish, divided, user_id \\ nil) do
    start = format_dt(dt_start)
    finish = format_dt(dt_finish) |> Timex.shift(minutes: 1)

    calc_summ_res(start, finish, user_id, divided)
  end

  def calc_res(nil, _, _, _, _, _), do: {:error, "invalid date-time"}
  def calc_res(_, nil, _, _, _, _), do: {:error, "invalid date-time"}

  def calc_res(start, finish, user_id, true, order_id) do
    res =
      cond do
        Timex.diff(finish, start, :year) > 0 ->
          from(o in Order,
            where:
              o.inserted_at >= ^start and
                o.inserted_at < ^finish and
                o.order_status_id == ^order_id and
                o.sender_id == ^user_id,
            group_by: fragment("1"),
            select: %{
              dt: fragment("extract(year from ?)::integer", o.inserted_at),
              count: fragment("sum(?)::integer", o.price)
            }
          )
          |> put_type("year")

        Timex.diff(finish, start, :month) > 0 ->
          from(o in Order,
            where:
              o.inserted_at >= ^start and
                o.inserted_at < ^finish and
                o.order_status_id == ^order_id and
                o.sender_id == ^user_id,
            group_by: fragment("1"),
            select: %{
              dt: fragment("extract(month from ?)::integer", o.inserted_at),
              count: fragment("sum(?)::integer", o.price)
            }
          )
          |> put_type("month")

        true ->
          from(o in Order,
            where:
              o.inserted_at >= ^start and
                o.inserted_at < ^finish and
                o.order_status_id == ^order_id and
                o.sender_id == ^user_id,
            group_by: fragment("1"),
            select: %{
              dt: fragment("extract(day from ?)::integer", o.inserted_at),
              count: fragment("sum(?)::integer", o.price)
            }
          )
          |> put_type("day")
      end

    {:ok, res}
  end

  def calc_res(start, finish, user_id, false, order_id) do
    res =
      from(o in Order,
        where:
          o.inserted_at >= ^start and
            o.inserted_at < ^finish and
            o.order_status_id == ^order_id and
            o.sender_id == ^user_id,
        select: fragment("sum(?)::integer", o.price)
      )
      |> put_type("*")

    {:ok, res}
  end

  def calc_res(start, finish, nil, false, order_id) do
    res =
      from(o in Order,
        where:
          o.inserted_at >= ^start and
            o.order_status_id == ^order_id and
            o.inserted_at < ^finish,
        select: fragment("sum(?)::integer", o.price)
      )
      |> put_type("year")

    {:ok, res}
  end

  def calc_res(start, finish, nil, true, order_id) do
    res =
      cond do
        Timex.diff(finish, start, :year) > 0 ->
          from(o in Order,
            where:
              o.inserted_at >= ^start and
                o.order_status_id == ^order_id and
                o.inserted_at < ^finish,
            group_by: fragment("1"),
            select: %{
              dt: fragment("extract(year from ?)::integer", o.inserted_at),
              count: fragment("sum(?)::integer", o.price)
            }
          )
          |> put_type("year")

        Timex.diff(finish, start, :month) > 0 ->
          from(o in Order,
            where:
              o.inserted_at >= ^start and
                o.order_status_id == ^order_id and
                o.inserted_at < ^finish,
            group_by: fragment("1"),
            select: %{
              dt: fragment("extract(month from ?)::integer", o.inserted_at),
              count: fragment("sum(?)::integer", o.price)
            }
          )
          |> put_type("month")

        true ->
          from(o in Order,
            where:
              o.inserted_at >= ^start and
                o.order_status_id == ^order_id and
                o.inserted_at < ^finish,
            group_by: fragment("1"),
            select: %{
              dt: fragment("extract(day from ?)::integer", o.inserted_at),
              count: fragment("sum(?)::integer", o.price)
            }
          )
          |> put_type("day")
      end

    {:ok, res}
  end

  def calc_res(_, _, _, _, _), do: {:error, "invalid date-time"}

  def count_transfered_orders(dt_start, dt_finish, divided, user_id \\ nil) do
    start = format_dt(dt_start)
    finish = format_dt(dt_finish) |> Timex.shift(minutes: 1)

    order_id =
      LogisticService.Transfering.Delegates.OrderStatus.get_order_status!(name: "Доставлен")
      |> Map.get(:id)

    calc_res(start, finish, user_id, divided, order_id)
  end

  def calc_order_res(nil, _, _, _, _), do: {:error, "invalid date-time"}
  def calc_order_res(_, nil, _, _, _), do: {:error, "invalid date-time"}

  def calc_order_res(start, finish, user_id, true) do
    res =
      cond do
        Timex.diff(finish, start, :year) > 0 ->
          from(o in Order,
            where:
              o.inserted_at >= ^start and
                o.inserted_at < ^finish and
                o.sender_id == ^user_id,
            group_by: fragment("1"),
            select: %{
              dt: fragment("extract(year from ?)::integer", o.inserted_at),
              count: count(o.id)
            }
          )
          |> put_type("year")

        Timex.diff(finish, start, :month) > 0 ->
          from(o in Order,
            where:
              o.inserted_at >= ^start and
                o.inserted_at < ^finish and
                o.sender_id == ^user_id,
            group_by: fragment("1"),
            select: %{
              dt: fragment("extract(month from ?)::integer", o.inserted_at),
              count: count(o.id)
            }
          )
          |> put_type("month")

        true ->
          from(o in Order,
            where:
              o.inserted_at >= ^start and
                o.inserted_at < ^finish and
                o.sender_id == ^user_id,
            group_by: fragment("1"),
            select: %{
              dt: fragment("extract(day from ?)::integer", o.inserted_at),
              count: count(o.id)
            }
          )
          |> put_type("day")
      end

    {:ok, res}
  end

  def calc_order_res(start, finish, user_id, false) do
    res =
      from(o in Order,
        where:
          o.inserted_at >= ^start and
            o.inserted_at < ^finish and
            o.sender_id == ^user_id,
        select: count(o.id)
      )
      |> put_type("*")

    {:ok, res}
  end

  def calc_order_res(start, finish, nil, false) do
    res =
      from(o in Order,
        where:
          o.inserted_at >= ^start and
            o.inserted_at < ^finish,
        select: count(o.id)
      )
      |> put_type("*")

    {:ok, res}
  end

  def calc_order_res(start, finish, nil, true) do
    res =
      cond do
        Timex.diff(finish, start, :year) > 0 ->
          from(o in Order,
            where:
              o.inserted_at >= ^start and
                o.inserted_at < ^finish,
            group_by: fragment("1"),
            select: %{
              dt: fragment("extract(year from ?)::integer", o.inserted_at),
              count: count(o.id)
            }
          )
          |> put_type("year")

        Timex.diff(finish, start, :month) > 0 ->
          from(o in Order,
            where:
              o.inserted_at >= ^start and
                o.inserted_at < ^finish,
            group_by: fragment("1"),
            select: %{
              dt: fragment("extract(month from ?)::integer", o.inserted_at),
              count: count(o.id)
            }
          )
          |> put_type("month")

        true ->
          from(o in Order,
            where:
              o.inserted_at >= ^start and
                o.inserted_at < ^finish,
            group_by: fragment("1"),
            select: %{
              dt: fragment("extract(day from ?)::integer", o.inserted_at),
              count: count(o.id)
            }
          )
          |> put_type("day")
      end

    {:ok, res}
  end

  def calc_order_res(_, _, _, _), do: {:error, "invalid date-time"}

  def count_orders(dt_start, dt_finish, divided, user_id \\ nil) do
    start = format_dt(dt_start)
    finish = format_dt(dt_finish) |> Timex.shift(minutes: 1)

    calc_order_res(start, finish, user_id, divided)
  end

  def reduce_price(p1, p2) when is_integer(p1) and is_integer(p2), do: p1 + p2
  def reduce_price(p1, p2) when is_nil(p1) and is_nil(p2), do: 0
  def reduce_price(p1, p2) when is_nil(p1), do: p2
  def reduce_price(p1, p2) when is_nil(p2), do: p1
  def reduce_price(_p1, _p2), do: 0

  def format_dt(dt, format \\ "%Y-%m-%d %H:%M:%S") do
    dt =
      case String.length(dt) do
        8 -> "20#{dt} 00:00:00"
        10 -> "#{dt} 00:00:00"
        _dt -> dt
      end

    try do
      Timex.parse!(dt, format, :strftime)
    rescue
      _e -> nil
    end
  end

  def fmt_dt(dt) do
    try do
      Timex.format!(dt, "%Y%m%d%H%M%S", :strftime)
    rescue
      _e -> nil
    end
  end

  def put_type(query, type) do
    case query |> Repo.all() do
      nil ->
        [%{count: 0, dt: nil, type: type}]

      [nil] ->
        [%{count: 0, dt: nil, type: type}]

      [num] when is_integer(num) ->
        [%{count: num, dt: nil, type: type}]

      map when is_map(map) ->
        [Map.put(map, :type, type)]

      list when is_list(list) ->
        list
        |> Enum.map(&Map.put(&1, :count, Map.get(&1, :count) || 0))
        |> Enum.map(&Map.put(&1, :type, type))

      _any ->
        [%{count: 0, dt: nil, type: type}]
    end
  end

  def remove_old_invalid do
    dt = Timex.now() |> Timex.shift(weeks: -1)

    case Repo.all(
           from(o in Order,
             where: o.updated_at <= ^dt and o.is_valid == false and o.can_be_sent == false
           )
         ) do
      [] ->
        :ok

      deletion_orders ->
        deletion_orders
        |> Enum.map(&Map.get(&1, :id))
        |> delete_products()

        deletion_orders
        |> Repo.delete_all()
    end
  end

  def morphyfy(vals) when is_list(vals), do: Enum.map(vals, &morphyfy/1)

  def morphyfy(vals) when is_struct(vals),
    do:
      vals |> Map.from_struct() |> Map.delete(:__meta__) |> Map.delete(:__struct__) |> morphyfy()

  def morphyfy(vals) when is_map(vals), do: Morphix.atomorphiform!(vals)
  def morphyfy(vals), do: vals
end
