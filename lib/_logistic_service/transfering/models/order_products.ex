defmodule LogisticService.Transfering.Models.OrderProducts do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset
  alias LogisticService.Transfering.Models.Order

  schema "order_products" do
    field :name, :string
    field :g_height, :float
    field :g_length, :float
    field :g_width, :float
    field :places, :string
    field :volume, :float
    field :weight, :float
    field :price, :integer
    field :count, :integer
    field :placement_info, :map
    field :comment, :string

    timestamps()
    belongs_to :order, Order
  end

  @doc false
  def changeset(order_products, attrs) do
    order_products
    |> cast(attrs, [
      :name,
      :places,
      :weight,
      :g_height,
      :g_length,
      :g_width,
      :volume,
      :placement_info,
      :price,
      :count
    ])
    |> validate_required([:name, :price, :count])
  end

  @doc false
  def put_order(order_products, order) do
    order_products
    |> change()
    |> put_assoc(:order, order)
  end
end
