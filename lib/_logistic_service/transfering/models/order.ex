defmodule LogisticService.Transfering.Models.Order do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset
  import ShorterMaps
  alias LogisticService.Accounts.Models.User
  alias LogisticService.Transfering.Models.{OrderStatus, OrderTarif, PaymentType, OrderProducts}

  @required ~w()a
  @optional ~w(one_c_data relog_data track_link sendage_email invoice_number payment_comment insurance odd_money payer
    receiver_address receiver_float receiver_apartment receiver_area receiver_fio receiver_phone receiver_state receiver_town receiver_comment
    sender_address sender_float sender_apartment sender_area sender_fio sender_phone sender_state sender_town sender_comment
    is_accepted acceptance_datetime is_delivered delivery_proof_datetime is_organization deleted_at delivery_awaited_datetime price inserted_at updated_at)a

  schema "orders" do
    field :one_c_data, :map
    field :relog_data, :map
    field :track_link, :string

    field :price, :integer

    field :receiver_apartment, :string
    field :receiver_float, :string
    field :receiver_address, :string
    field :receiver_area, :string
    field :receiver_fio, :string
    field :receiver_phone, :string
    field :receiver_state, :string
    field :receiver_town, :string
    field :receiver_comment, :string

    field :sender_apartment, :string
    field :sender_float, :string
    field :sender_address, :string
    field :sender_area, :string
    field :sender_fio, :string
    field :sender_phone, :string
    field :sender_state, :string
    field :sender_town, :string
    field :sender_comment, :string

    field :is_accepted, :boolean
    field :acceptance_datetime, :naive_datetime

    field :is_delivered, :boolean
    field :delivery_proof_datetime, :naive_datetime

    field :delivery_awaited_datetime, :naive_datetime

    field :is_organization, :boolean
    field :can_be_sent, :boolean
    field :is_valid, :boolean
    field :sendage_email, :string
    field :invoice_number, :string
    field :payment_comment, :string
    field :insurance, :string
    field :odd_money, :string
    field :payer, :string

    timestamps()
    field :deleted_at, :naive_datetime

    belongs_to :order_status, OrderStatus, on_replace: :nilify
    belongs_to :order_tarif, OrderTarif, on_replace: :nilify
    belongs_to :payment_type, PaymentType, on_replace: :nilify

    has_many :products, OrderProducts

    belongs_to :sender, User, on_replace: :nilify
    belongs_to :courier, User, on_replace: :nilify
    belongs_to :updated_by, User, on_replace: :nilify
    belongs_to :deleted_by, User, on_replace: :nilify
  end

  @doc false
  def changeset(order, attrs) do
    order
    |> cast(attrs, @optional ++ @required)
    |> validate_required(@required)
    |> put_order_status(attrs)
    |> put_payment_type(attrs)
  end

  def put_sender(order, sender),
    do: cast(order, ~M{sender_id: Map.get(sender, :id)}, [:sender_id])

  def validate(order, attrs) do
    order
    |> cast(attrs, [:is_valid, :can_be_sent])
  end

  def put_courier(order, courier),
    do: cast(order, ~M{courier_id: Map.get(courier, :id)}, [:courier_id])

  def put_products(order, products) do
    order
    |> change()
    |> put_assoc(:products, products)
  end

  def put_order_status(order, nil), do: order

  def put_order_status(%Ecto.Changeset{} = order, %{order_status_id: order_status_id}) do
    case LogisticService.Transfering.get_order_status(order_status_id) do
      nil -> order
      status -> put_assoc(order, :order_status, status)
    end
  end

  def put_order_status(order, _any), do: order

  def put_updater(order, _any), do: order

  def put_order_tarif(order, nil), do: order

  def put_order_tarif(%Ecto.Changeset{} = order, %{order_tarif_id: order_tarif_id}) do
    case LogisticService.Transfering.get_order_tarif(order_tarif_id) do
      nil -> order
      ot -> put_assoc(order, :payment_type, ot)
    end
  end

  def put_order_tarif(order, _any), do: order

  def put_payment_type(order, nil), do: order

  def put_payment_type(%Ecto.Changeset{} = order, %{payment_type_id: nil}), do: order
  def put_payment_type(%Ecto.Changeset{} = order, %{payment_type_id: payment_type_id}) do
    case LogisticService.Transfering.get_payment_type(payment_type_id) do
      nil -> order
      pt -> put_assoc(order, :payment_type, pt)
    end
  end

  def put_payment_type(order, _any), do: order

  def delete_changeset(order, user) do
    order
    |> change()
    |> put_assoc(:deleted_by, user)
    |> cast(%{deleted_at: Timex.now()}, [:deleted_at])
  end
end
