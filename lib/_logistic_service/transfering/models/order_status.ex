defmodule LogisticService.Transfering.Models.OrderStatus do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset
  alias LogisticService.Transfering.Models.Order

  @required_fields ~w(name)a
  @optional_fields ~w(rules)a

  schema "order_statuses" do
    field :name, :string
    field :rules, :map
    has_many :order, Order
  end

  @doc false
  def changeset(order_status, attrs) do
    order_status
    |> cast(attrs, @optional_fields ++ @required_fields)
    |> validate_required(@required_fields)
  end
end
