defmodule LogisticService.Transfering.Models.PaymentType do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset
  alias LogisticService.Transfering.Models.Order

  @required_fields ~w(name)a
  @optional_fields ~w(rules comment)a

  schema "payment_types" do
    field :name, :string
    field :rules, :map
    field :comment, :string
    has_many :order, Order
  end

  @doc false
  def changeset(payment_type, attrs) do
    payment_type
    |> cast(attrs, @optional_fields ++ @required_fields)
    |> validate_required(@required_fields)
  end
end
