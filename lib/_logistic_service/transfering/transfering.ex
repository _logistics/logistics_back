defmodule LogisticService.Transfering do
  @moduledoc """
  The Transfering context.
  """
  alias LogisticService.Transfering.Delegates.{
    OrderStatus,
    OrderTarifs,
    PaymentType,
    Order,
    OrderProducts
  }

  defdelegate multisert_order_statuses!(order_statuses), to: OrderStatus
  defdelegate list_order_statuses, to: OrderStatus
  defdelegate get_order_status!(id), to: OrderStatus
  defdelegate get_order_status(id), to: OrderStatus
  defdelegate create_order_status(attrs), to: OrderStatus
  defdelegate update_order_status(order, attrs), to: OrderStatus
  defdelegate delete_order_status(order), to: OrderStatus

  defdelegate multisert_order_tarifs!(order_statuses), to: OrderTarifs
  defdelegate list_order_tarifs, to: OrderTarifs
  defdelegate get_order_tarif!(id), to: OrderTarifs
  defdelegate get_order_tarif(id), to: OrderTarifs
  defdelegate create_order_tarif(attrs), to: OrderTarifs
  defdelegate update_order_tarif(order, attrs), to: OrderTarifs
  defdelegate delete_order_tarif(order), to: OrderTarifs

  defdelegate multisert_payment_types!(payment_types), to: PaymentType
  defdelegate list_payment_types, to: PaymentType
  defdelegate get_payment_type!(id), to: PaymentType
  defdelegate get_payment_type(id), to: PaymentType
  defdelegate create_payment_type(attrs), to: PaymentType
  defdelegate update_payment_type(order, attrs), to: PaymentType
  defdelegate delete_payment_type(order), to: PaymentType

  defdelegate list_orders(attrs), to: Order
  defdelegate list_orders(param, attrs), to: Order
  defdelegate create_order(user, attrs), to: Order
  defdelegate update_order(user, order_id, attrs), to: Order
  defdelegate get_order!(id), to: Order
  defdelegate get_order!(any, id), to: Order
  defdelegate get_order_with_preloads(id), to: Order
  defdelegate delete_order(order_id, user), to: Order
  defdelegate put_products(order_id, product), to: Order
  defdelegate get_last_incompleted(user_id), to: Order
  defdelegate remove_old_invalid, to: Order
  defdelegate watch_prepared_order(id), to: Order
  defdelegate send_prepared_order(id), to: Order
  defdelegate confirm_order_and_send(id), to: Order
  defdelegate get_from_one_c(id), to: Order
  defdelegate force_validate(order_id), to: Order
  defdelegate update_status_at_order(order_id, status_id), to: Order
  defdelegate can_order_be_deleted?(order_id), to: Order
  defdelegate get_mth_report(user_id, year, mth), to: Order
  defdelegate get_year_report(user_id, year), to: Order
  defdelegate get_all_time_report(user_id), to: Order

  defdelegate count_order_summ(dt_start, dt_finish, divided), to: Order
  defdelegate count_order_summ(dt_start, dt_finish, divided, user_id), to: Order

  defdelegate count_transfered_orders(dt_start, dt_finish, divided), to: Order
  defdelegate count_transfered_orders(dt_start, dt_finish, divided, user_id), to: Order

  defdelegate count_orders(dt_start, dt_finish, divided), to: Order
  defdelegate count_orders(dt_start, dt_finish, divided, user_id), to: Order
  defdelegate set_order_courier(order_id, courier_id), to: Order

  defdelegate update_product(product_id, product), to: OrderProducts
  defdelegate delete_product(product_id), to: OrderProducts
  defdelegate can_product_be_deleted?(product_id), to: OrderProducts
end
