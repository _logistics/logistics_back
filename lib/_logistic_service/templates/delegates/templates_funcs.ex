defmodule LogisticService.Templates.Delegates.TemplatesFuncs do
  @moduledoc """
  The Templates context.
  """

  import Ecto.Query, warn: false
  alias LogisticService.Repo

  alias LogisticService.Templates.Models.OrderTemplate

  @doc """
  Returns the list of order_templates.

  ## Examples

      iex> list_order_templates()
      [%OrderTemplate{}, ...]

  """
  def list_order_templates do
    Repo.all(OrderTemplate)
  end

  @doc """
  Gets a single order_template.

  Raises `Ecto.NoResultsError` if the Order template does not exist.

  ## Examples

      iex> get_order_template!(123)
      %OrderTemplate{}

      iex> get_order_template!(456)
      ** (Ecto.NoResultsError)

  """
  def get_order_template!(id), do: Repo.get!(OrderTemplate, id)

  @doc """
  Creates a order_template.

  ## Examples

      iex> create_order_template(%{field: value})
      {:ok, %OrderTemplate{}}

      iex> create_order_template(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_order_template(attrs \\ %{}) do
    %OrderTemplate{}
    |> OrderTemplate.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a order_template.

  ## Examples

      iex> update_order_template(order_template, %{field: new_value})
      {:ok, %OrderTemplate{}}

      iex> update_order_template(order_template, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_order_template(%OrderTemplate{} = order_template, attrs) do
    order_template
    |> OrderTemplate.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a order_template.

  ## Examples

      iex> delete_order_template(order_template)
      {:ok, %OrderTemplate{}}

      iex> delete_order_template(order_template)
      {:error, %Ecto.Changeset{}}

  """
  def delete_order_template(%OrderTemplate{} = order_template) do
    Repo.delete(order_template)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking order_template changes.

  ## Examples

      iex> change_order_template(order_template)
      %Ecto.Changeset{data: %OrderTemplate{}}

  """
  def change_order_template(%OrderTemplate{} = order_template, attrs \\ %{}) do
    OrderTemplate.changeset(order_template, attrs)
  end
end
