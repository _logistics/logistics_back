defmodule LogisticService.Templates do
  @moduledoc false

  alias LogisticService.Templates.Delegates.TemplatesFuncs

  defdelegate list_order_templates(), to: TemplatesFuncs
  defdelegate get_order_template!(id), to: TemplatesFuncs
  defdelegate create_order_template(attrs), to: TemplatesFuncs
  defdelegate update_order_template(template, attrs), to: TemplatesFuncs
  defdelegate delete_order_template(template), to: TemplatesFuncs
end
