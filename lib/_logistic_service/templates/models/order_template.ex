defmodule LogisticService.Templates.Models.OrderTemplate do
  @moduledoc false
  alias LogisticService.Templates.Models.OrderTemplate
  use Ecto.Schema
  import Ecto.Changeset

  @required_fields ~w(name user_id order_id)a
  @optional_fields ~w(updated_at)a
  @private_fields ~w()a

  @type t :: %OrderTemplate{}
  schema "order_templates" do
    field :name, :string
    field :order_id, :id
    field :user_id, :id

    timestamps()
  end

  @doc false
  def changeset(order_template, attrs) do
    order_template
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end

  def update_changeset(user, attrs) do
    user
    |> cast(attrs, @optional_fields ++ @required_fields)
    |> validate_length(:phone, is: 10)
  end

  def admin_changeset(entity, attrs) do
    entity
    |> cast(attrs, @optional_fields ++ @required_fields ++ @private_fields)
  end
end
