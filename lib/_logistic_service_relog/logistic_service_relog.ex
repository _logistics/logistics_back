defmodule LogisticServiceRelog do
  @moduledoc """
  LogisticServiceRelog - контекст обеспечивающий соединение с Relog
  """
  require Logger
  @api_key "a6b8d806b00f4ecaaa0e7d9647d8bae9"

  def send_client(id) do
    headers = [{"content-type", "application/json"}]

    client =
      LogisticService.Accounts.get_user_with_profile(id)
      |> Map.delete(:__meta__)
      |> Map.delete(:__struct__)

    case Map.get(client, :profile) do
      nil -> {:error, "У пользователя отсутствует профиль"}
      profile -> prepare_client(client, profile)
    end
    |> case do
      {:error, _any} = err ->
        err

      prepared ->
        Tesla.post("https://app.relog.kz/api/v2/clients/", send_map(prepared), headers: headers)
        |> send_message()
    end
  end

  def send_courier(id, password) do
    headers = [{"content-type", "application/json"}]

    client =
      LogisticService.Accounts.get_user_with_profile(id)
      |> Map.delete(:__meta__)
      |> Map.delete(:__struct__)

    case Map.get(client, :profile) do
      nil -> {:error, "У пользователя отсутствует профиль"}
      profile -> prepare_courier(client, profile, password)
    end
    |> case do
      {:error, _any} = err ->
        err

      prepared ->
        Tesla.post("https://app.relog.kz/api/v2/couriers", Poison.encode!(prepared),
          headers: headers
        )
        |> send_message
    end
  end

  def send_order(id) do
    headers = [{"content-type", "application/json"}]
    order = LogisticService.Transfering.get_order_with_preloads(id)

    if is_nil(order) or is_nil(Map.get(order, :products)) do
      {:error, "заказ недозаполнен"}
    else
      products = Map.get(order, :products)

      case prepare_order(order, products) do
        {:error, _} = err ->
          err

        prepared_order ->
          Tesla.post("https://app.relog.kz/api/v2/applications", Poison.encode!(prepared_order),
            headers: headers
          )
          |> send_message
      end
    end
  end

  defp send_map(map) do
    %{api_key: @api_key}
    |> Map.merge(map)
    |> Poison.encode!()
  end

  defp send_message(parse) do
    res =
      parse
      |> case do
        {:ok, %{status: 401, body: body}} ->
          {:error, body}

        {:ok, %{status: 403, body: body}} ->
          {:error, body}

        {:ok, %{body: body}} ->
          case Poison.decode!(body) |> Map.get("data") do
            link when is_binary(link) ->
              Process.sleep(5000)

              try do
                body =
                  Tesla.get!(link)
                  |> Map.get(:body)

                Logger.warn(%{link: link, body: body})

                response =
                  body
                  |> Poison.decode!()
                  |> Map.get("data")
                  |> List.first()

                if !is_nil(response) && response |> Map.get("status") == "failure" do
                  {:error, response |> Map.get("message")}
                else
                  {:ok, body}
                end
              rescue
                _e ->
                  {:error, "unfetcheble link from relog or no result (#{link})"}
              end

            _ ->
              {:error, "unknown response (#{body})"}
          end

        any ->
          any
      end

    res
  end

  defp prepare_order(
         %{
           id: id,
           price: price,
           delivery_awaited_datetime: delivery_awaited_datetime,
           receiver_address: receiver_address,
           receiver_fio: receiver_fio,
           receiver_phone: receiver_phone,
           receiver_town: receiver_town,
           sender_address: sender_address,
           sender_fio: sender_fio,
           sender_phone: sender_phone,
           sender_town: sender_town
         } = order,
         products
       )
       when not is_nil(delivery_awaited_datetime) do
    %{
      api_key: @api_key,
      options: %{multi: true},
      data: [
        %{
          client: %{
            phone: "7#{sender_phone}",
            name: make_name(String.trim(sender_fio), "7#{sender_phone}"),
            description:
              "sender_comment: #{Map.get(order, :sender_comment)}, receiver_comment: #{
                Map.get(order, :receiver_comment)
              }"
          },
          application: %{
            externalId: id,
            price: price,
            planDeliveryPeriod: %{
              startDate:
                delivery_awaited_datetime
                |> Timex.shift(minutes: -15)
                |> Timex.to_unix()
                |> Kernel.*(1000),
              endDate:
                delivery_awaited_datetime
                |> Timex.shift(minutes: 15)
                |> Timex.to_unix()
                |> Kernel.*(1000)
            },
            planPickupPeriod: %{
              startDate:
                delivery_awaited_datetime
                |> Timex.shift(minutes: -15)
                |> Timex.to_unix()
                |> Kernel.*(1000),
              endDate:
                delivery_awaited_datetime
                |> Timex.shift(minutes: 15)
                |> Timex.to_unix()
                |> Kernel.*(1000)
            },
            addressFrom: %{
              address: "#{sender_town} #{sender_address}",
              details: "phone: #{sender_phone}, имя - #{sender_fio}"
            },
            addressTo: %{
              address: "#{receiver_town} #{receiver_address}",
              details: "phone: #{receiver_phone}, имя - #{receiver_fio}"
            },
            appType: "default"
          },
          goods: Enum.map(products, &prepare_products/1) |> Enum.reject(&is_nil(&1))
        }
      ]
    }
  end

  defp prepare_order(_, _) do
    {:error,
     "Заказ недозаполнен. Требуемые поля не заполнены (заполните обязательные поля заказа)"}
  end

  defp make_name("", phone) when is_binary(phone), do: phone
  defp make_name(name, _phone) when is_binary(name), do: name

  def prepare_products(products) do
    case products do
      %{name: name, volume: volume, count: quantity, weight: weight} = product ->
        %{
          name: name,
          code: "0",
          price: Map.get(product, :price) || 0,
          volume: volume,
          count: quantity,
          weight: weight
        }

      _ ->
        nil
    end
  end

  defp prepare_courier(
         %{phone: phone},
         %{first_name: first_name, middle_name: middle_name, last_name: last_name, mail: mail} =
           profile,
         password
       ) do
    %{
      api_key: @api_key,
      courier: %{
        phone: "7#{phone}",
        fullName: "#{first_name || ""} #{middle_name || ""} #{last_name || ""}",
        email: mail || "test@mail.ru",
        additionalInfo: Map.get(profile, :comments) || "no info",
        password: password
      }
    }
  end

  defp prepare_courier(_, _, _) do
    {:error,
     "Профиль пользователя недозаполненен. Требуемые поля - first_name, middle_name, last_name, mail"}
  end

  defp prepare_client(
         %{phone: phone},
         %{
           first_name: first_name,
           middle_name: middle_name,
           last_name: last_name,
           mail: mail
           # town: town, street: street, house_number: house_number
         } = profile
       ) do
    %{
      client: %{
        phone: phone,
        name: "#{first_name || ""} #{middle_name || ""} #{last_name || ""}",
        email: mail,
        description: Map.get(profile, :comments)
        # addresses: [
        #   %{
        #     address: "#{town} #{Map.get(profile, :area) || ""} #{street} #{house_number}"
        #   }
        # ]
      }
    }
  end

  defp prepare_client(_, _) do
    {:error,
     "Профиль пользователя недозаполненен. Требуемые поля - first_name, middle_name, last_name, mail, town, area, street, house_number"}
  end
end
