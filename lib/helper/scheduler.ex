defmodule Helper.Scheduler do
  @moduledoc """
  job scheduler
  """
  use Quantum, otp_app: :logistic_service

  alias Helper.Cache

  @doc """
  clear all the cache in Cachex
  just in case the cache system broken
  """
  def clear_all_cache, do: Cache.clear_all()
end
