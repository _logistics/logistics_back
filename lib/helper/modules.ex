defmodule Helper.Modules do
  @moduledoc """
  Этот хэлпер нужен для более удобной работы с модулями
  """

  def fetch_main_module_name do
    name_to_module_name(:logistic_service)
  end

  def name_to_module_name(name) when is_atom(name) do
    name
    |> to_string()
    |> String.split("_")
    |> Enum.map(&String.capitalize(&1))
    |> Enum.join()
  end

  def name_to_module_name(name) when is_binary(name) do
    name
    |> String.split("_")
    |> Enum.map(&String.capitalize(&1))
    |> Enum.join()
  end
end
