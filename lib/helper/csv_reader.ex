defmodule Helper.CSVReader do
  @moduledoc """
  Получет список объектов из csv файлов
  """
  def get(path) do
    path
    |> File.stream!()
    |> CSV.decode(headers: true)
    |> Enum.to_list()
    |> Enum.map(fn {:ok, map} ->
      map
      |> Morphix.atomorphiform!()
    end)
  end
end
