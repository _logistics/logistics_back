defmodule Helper.Factory do
  @moduledoc """
    Помошник для создания фабрик для автозаполнения базы
    @filecwd - путь к корневой директории проекта (т.к. проект запускается с помощью mix который в корне)
  """
  alias Helper.CSVReader
  import Helper.Modules, only: [name_to_module_name: 1, fetch_main_module_name: 0]

  @project_root_dir File.cwd!()

  @doc """
    Запускает получение сущностей для множества сушностей
    На вход принимает список из сущностей и название контекста
    Порядок сущностей ИМЕЕТ значение
  """
  def entity_mass_getter(list, context) do
    ctx_runner =
      context
      |> String.downcase()
      |> entity_func_factory()

    list
    |> Enum.map(&ctx_runner.(&1))
  end

  @doc """
    HOF функция для создания функции, которая позволяет получить данные и запустить execute для засеивания данными базу
  """
  def entity_func_factory(context) do
    fn entity ->
      "priv/repo/csv/#{context}/#{entity}.csv"
      |> Path.expand(@project_root_dir)
      |> CSVReader.get()
      |> execute(context, entity)
    end
  end

  @doc """
   Находит необходимый модуль (пространство имен и запускает в нем функцию multisert)
   !! Необходимо правильно соблюдать название модулей {{Project Name}}.Factories.{{Context}}.{{EntityName}}Factory,
   где все слова пишутся camelcase,
   Context - название контекста,
   EntityName - название сущности,
   ProjectName - название проекта берется автоматически из mix.exs
  """
  def execute(params, context, entity) do
    "Elixir.#{fetch_main_module_name()}.Factories.#{String.capitalize(context)}.#{
      name_to_module_name(entity)
    }Factory"
    |> String.to_existing_atom()
    |> apply(:multisert, [params])
  end
end
