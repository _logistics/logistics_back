defmodule Helper.Validators do
  @moduledoc """
  Utils Functions
  """

  @doc """
  Проверяет значения пришедшие в сприске - int
  reduce_while прерывается на {:halt}

  ## Пример
    iex> Helper.Utils.is_list_of_int?([1,2,3,4])
    true

    iex> Helper.Utils.is_list_of_int?([1,2,3,"a"])
    false
  """

  @spec is_list_of_int?(term) :: boolean
  def is_list_of_int?(list) when is_list(list) do
    list
    |> Enum.reduce_while(true, fn x, _acc ->
      if is_integer(x),
        do: {:cont, true},
        else: {:halt, false}
    end)
  end

  @doc """
  Проверяет значения пришедшие в сприске - string
  reduce_while прерывается на {:halt}

  ## Пример
    iex> Helper.Utils.is_list_of_int?(["a","b","c","d"])
    true

    iex> Helper.Utils.is_list_of_int?([1,2,3,"a"])
    false
  """

  @spec is_list_of_string?(term) :: boolean
  def is_list_of_string?(list) when is_list(list) do
    list
    |> Enum.reduce_while(true, fn x, _acc ->
      if is_binary(x),
        do: {:cont, true},
        else: {:halt, false}
    end)
  end

  @doc """
  Проверяет значения пришедшие в сприске - struct
  reduce_while прерывается на {:halt}

  ## Пример
    iex> Helper.Utils.is_list_of_int?(["a","b","c","d"])
    true

    iex> Helper.Utils.is_list_of_int?([1,2,3,"a"])
    false
  """

  @spec is_list_of_structs?(term) :: boolean
  def is_list_of_structs?(list) when is_list(list) do
    list
    |> Enum.reduce_while(true, fn x, _acc ->
      if is_struct(x),
        do: {:cont, true},
        else: {:halt, false}
    end)
  end
end
