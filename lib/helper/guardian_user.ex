defmodule Helper.GuardianUser do
  @moduledoc """
  This module defines some helper functions used by encode/decode Json Web Token
  """

  use Guardian, otp_app: :logistic_service
  # 336 hours = 14 days
  @token_expiration 336

  def subject_for_token(resource, _claims), do: {:ok, to_string(resource.phone)}
  def resource_from_claims(claims), do: {:ok, %{phone: claims["sub"]}}

  def jwt_encode(source, args \\ %{}),
    do: encode_and_sign(source, args, ttl: {@token_expiration, :hour})

  def jwt_decode(token), do: resource_from_token(token)
end
