defmodule Helper.QueryBuilder do
  @moduledoc """
  Общие функции для построения запроса
  """
  import Ecto.Query, warn: false

  @doc """
  inserted in latest x mounth
  """
  def recent_inserted(queryable, months: count) do
    end_of_today = Timex.now() |> Timex.end_of_day()
    x_months_ago = Timex.today() |> Timex.shift(months: -count) |> Timex.to_datetime()

    queryable
    |> where([q], q.inserted_at >= ^x_months_ago)
    |> where([q], q.inserted_at <= ^end_of_today)
  end

  def sort_by_count(queryable, field, param) do
    queryable
    |> join(:left, [p], s in assoc(p, ^field))
    |> group_by([p], p.id)
    |> select([p], p)
    |> order_by([_, s], {^param, fragment("count(?)", s.id)})
  end

  def param_reducer(queryable, filter) when is_map(filter) do
    filter
    |> Enum.reduce(queryable, fn
      {:sort, :desc_inserted}, queryable ->
        queryable |> order_by(desc: :inserted_at)

      {:sort, :asc_inserted}, queryable ->
        queryable |> order_by(asc: :inserted_at)

      {:sort, :desc_id}, queryable ->
        queryable |> order_by(desc: :id)

      {:sort, :asc_id}, queryable ->
        queryable |> order_by(asc: :id)

      {:when, :today}, queryable ->
        date = Timex.now()

        queryable
        |> where([p], p.inserted_at >= ^Timex.beginning_of_day(date))
        |> where([p], p.inserted_at <= ^Timex.end_of_day(date))

      {:when, :this_week}, queryable ->
        date = Timex.now()

        queryable
        |> where([p], p.inserted_at >= ^Timex.beginning_of_week(date))
        |> where([p], p.inserted_at <= ^Timex.end_of_week(date))

      {:when, :this_month}, queryable ->
        date = Timex.now()

        queryable
        |> where([p], p.inserted_at >= ^Timex.beginning_of_month(date))
        |> where([p], p.inserted_at <= ^Timex.end_of_month(date))

      {:when, :this_year}, queryable ->
        date = Timex.now()

        queryable
        |> where([p], p.inserted_at >= ^Timex.beginning_of_year(date))
        |> where([p], p.inserted_at <= ^Timex.end_of_year(date))

      {:first, first}, queryable ->
        queryable |> limit(^first)

      {:start_dt, dt}, queryable ->
        queryable
        |> where([p], p.inserted_at >= ^Timex.parse!(dt, "%Y-%m-%d %H:%M:%S", :strftime))

      {:end_dt, dt}, queryable ->
        queryable
        |> where([p], p.inserted_at <= ^Timex.parse!(dt, "%Y-%m-%d %H:%M:%S", :strftime))

      {_, _}, queryable ->
        queryable
    end)
  end
end
