defmodule Helper.GqlSchemaSuite do
  @moduledoc """
  boilerplate for gql modules
  """
  defmacro __using__(_opts) do
    quote do
      use Absinthe.Schema.Notation
      import Absinthe.Resolution.Helpers, only: [dataloader: 3]
      import SafeResolver

      alias LogisticServiceWeb.Schema.Middleware

      alias LogisticServiceWeb.Resolvers, as: R
      alias LogisticServiceWeb.Middleware, as: M
    end
  end
end
