defmodule Helper.ErrorHandler do
  @moduledoc """
  Module defines some error helper functions
  """

  alias LogisticServiceWeb.Gettext, as: Translate

  def not_found_formater(queryable, id) when is_integer(id) or is_binary(id) do
    model = queryable_to_model(queryable)

    Translate
    |> Gettext.dgettext("404", "#{model}(%{id}) not found", id: id)
  end

  def not_found_formater(queryable, clauses) do
    model = queryable_to_model(queryable)
    name = clause_to_name(clauses)

    Translate
    |> Gettext.dgettext("404", "#{model}(%{name}) not found", name: name)
  end

  defp queryable_to_model(q) do
    q
    |> to_string()
    |> String.split(".")
    |> List.last()
  end

  defp clause_to_name(c) do
    c
    |> Enum.into(%{})
    |> Map.values()
    |> List.first()
    |> to_string
  end
end
