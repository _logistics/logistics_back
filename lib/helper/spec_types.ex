defmodule Helper.SpecTypes do
  @moduledoc """
  Собственные типы данных
  """

  @typedoc """
  Type GraphQL flavor the error format
  """
  @type gq_error :: {:error, [message: String.t()]}

  @typedoc """
  general response conventions
  """
  @type done :: {:ok, map} | {:error, map}
end
