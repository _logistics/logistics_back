defmodule Helper.DBR do
  @moduledoc """
  Этот модуль помогает управлять запросами в нативную базу данных
  использует стандартную Ecto Repo
  """

  import Ecto.Query, warn: false

  import Helper.Utils, only: [done: 1, done: 3, add: 1]

  import Helper.ErrorHandler

  alias LogisticService.Repo
  alias Helper.{QueryBuilder}

  def create(model, attrs) do
    model
    |> struct
    |> model.changeset(attrs)
    |> Repo.insert()
  end

  def count(query, filter \\ %{}) do
    query
    |> QueryBuilder.param_reducer(filter)
    |> select([f], count(f.id))
    |> Repo.one()
  end

  @spec next_count(any) :: integer
  def next_count(query) do
    query
    |> count()
    |> add()
  end

  def paginate(query, page: page, size: size),
    do: query |> Repo.paginate(page: page, page_size: size)

  def find(query, id, preload: preload) do
    query
    |> preload(^preload)
    |> Repo.get(id)
    |> done(query, id)
  end

  def find(query, id) do
    query
    |> Repo.get(id)
    |> done(query, id)
  end

  def find_by(query, clauses) do
    query
    |> Repo.get_by(clauses)
    |> catch_empty_result(query, clauses)
  end

  def find_all(query, %{page: page, size: size} = filter) do
    query
    |> QueryBuilder.param_reducer(filter)
    |> paginate(page: page, size: size)
    |> done()
  end

  def find_all(query, filter) do
    query
    |> QueryBuilder.param_reducer(filter)
    |> Repo.all()
    |> done()
  end

  def update(model, attrs) do
    model
    |> model.__struct__.update_changeset(attrs)
    |> Repo.update()
  end

  def find_update(query, id, attrs), do: do_find_update(query, id, attrs)
  def find_update(query, %{id: id} = attrs), do: do_find_update(query, id, attrs)

  defp do_find_update(query, id, attrs) do
    with {:ok, result} <- find(query, id) do
      result
      |> result.__struct__.changeset(attrs)
      |> Repo.update()
    end
  end

  def update_by(query, clauses, attrs) do
    with {:ok, result} <- find_by(query, clauses) do
      result
      |> Ecto.Changeset.change(attrs)
      |> Repo.update()
    end
  end

  def upsert_by(query, clauses, attrs) do
    case query |> find_by(clauses) do
      {:ok, result} ->
        result
        |> result.__struct__.changeset(attrs)
        |> Repo.update()

      {:error, _err} ->
        query
        |> create(attrs)
    end
  end

  @doc """
  Функция производит удаление !!! ОБЯЗАТЕЛЬНО применять Middlleware Authorization PermissionCheck
  Просто так не используем
  """
  def delete(query), do: Repo.delete(query)

  def find_delete(query, id) do
    with {:ok, result} <- find_by(query, id) do
      delete(result)
    end
  end

  def findby_delete(query, clauses) do
    with {:ok, result} <- find_by(query, clauses) do
      delete(result)
    end
  end

  def findby_or_insert(query, clauses, attrs) do
    case query |> find_by(clauses) do
      {:ok, result} -> {:ok, result}
      {:error, _err} -> query |> create(attrs)
    end
  end

  defp catch_empty_result(result, query, params) do
    case result do
      nil ->
        {:error, not_found_formater(query, params)}

      result ->
        {:ok, result}
    end
  end

  defdelegate transaction(query, opts), to: Repo
  defdelegate transaction(query), to: Repo
end
