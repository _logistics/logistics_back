defmodule Helper.Cache do
  @moduledoc """
  memory cache
  """

  @cache :site_cache

  @doc """
  Получить значение из кэша

  ## Пример
  iex> Helper.Cache.get(a)
  {:ok, "b"}
  """
  def get(key) do
    case Cachex.get(@cache, key) do
      {:ok, nil} ->
        {:error, nil}

      {:ok, result} ->
        {:ok, result}
    end
  end

  @doc """
  Поместить данные в кэш

  ## Пример
  iex> Helper.Cache.put(a, "x")
  {:ok, "x"}
  """
  def put(key, val), do: Cachex.put(@cache, key, val)

  def put(key, val, expire: expire_time) do
    put(key, val)
    Cachex.expire(@cache, key, expire_time)
  end

  @doc """
  Очистить кэш

  ## Пример
  iex> Helper.Cache.clear()
  {:ok, 1}
  """
  def clear_all(), do: Cachex.clear(@cache)

  @doc """
  cache scope of community contributes digest
  """
  def get_scope(:community_contributes, id), do: "community.contributes.#{id}"
end
