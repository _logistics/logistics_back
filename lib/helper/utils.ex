defmodule Helper.Utils do
  @moduledoc """
  Utils Functions
  """
  import Ecto.Query, warn: false
  import Helper.ErrorHandler

  alias Helper.Cache

  def get_config(section, key, app \\ :logistic_service)

  def get_config(section, :all, app) do
    app
    |> Application.get_env(section)
    |> case do
      nil -> ""
      config -> config
    end
  end

  def get_config(section, key, app) do
    app
    |> Application.get_env(section)
    |> case do
      nil -> ""
      config -> Keyword.get(config, key)
    end
  end

  @doc """
  handle General {:ok, ..} or {:error, ..} return
  """
  def done(nil, :boolean), do: {:ok, false}
  def done(_, :boolean), do: {:ok, true}
  def done(nil, err_msg), do: {:error, err_msg}

  def done({:ok, _}, with: result), do: {:ok, result}
  def done({:error, reason}, with: _result), do: {:error, reason}
  def done({:ok, %{id: id}}, :status), do: {:ok, %{done: true, id: id}}
  def done({:error, _}, :status), do: {:ok, %{done: false}}
  def done(nil, queryable, id), do: {:error, not_found_formater(queryable, id)}
  def done(result, _, _), do: {:ok, result}
  def done(nil), do: {:error, "record not found."}

  def done({n, nil}) when is_integer(n), do: {:ok, %{done: true}}
  def done(result), do: {:ok, result}

  def done_and_cache(result, scope, expire: expire_time) do
    with {:ok, res} <- done(result) do
      Cache.put(scope, res, expire: expire_time)
      {:ok, res}
    end
  end

  def done_and_cache(result, scope) do
    with {:ok, res} <- done(result) do
      Cache.put(scope, res)
      {:ok, res}
    end
  end

  def handle_absinthe_error(resolution, err_msg, code) when is_integer(code) do
    resolution
    |> Absinthe.Resolution.put_result({:error, message: err_msg, code: code})
  end

  def handle_absinthe_error(resolution, err_msg) when is_list(err_msg) do
    resolution
    |> Absinthe.Resolution.put_result({:error, message: err_msg})
  end

  def handle_absinthe_error(resolution, err_msg) when is_binary(err_msg) do
    resolution
    |> Absinthe.Resolution.put_result({:error, message: err_msg})
  end

  def map_key_stringify(%{__struct__: _} = map) when is_map(map) do
    map
    |> Map.from_struct()
    |> map_key_stringify
  end

  def map_key_stringify(map) when is_map(map) do
    map
    |> Enum.reduce(%{}, fn {key, val}, acc ->
      Map.put(acc, to_string(key), val)
    end)
  end

  @doc """
  Рекурсивно переводит мапу в кэмел кейс
  нужно, чтобы переводить параметры к формату в Graphql
  """
  def camelize_map_key(map, v_trans \\ :ignore) do
    map
    |> Enum.map(fn {k, v} ->
      v =
        cond do
          is_datetime?(v) ->
            DateTime.to_iso8601(v)

          is_map(v) ->
            camelize_map_key(safe_map(v))

          is_binary(v) ->
            handle_camelize_value_trans(v, v_trans)

          true ->
            v
        end

      map_to_camel({k, v})
    end)
    |> Enum.into(%{})
  end

  defp handle_camelize_value_trans(v, :ignore), do: v
  defp handle_camelize_value_trans(v, :downcase), do: String.downcase(v)
  defp handle_camelize_value_trans(v, :upcase), do: String.upcase(v)

  defp safe_map(%{__struct__: _} = map), do: Map.from_struct(map)
  defp safe_map(map), do: map

  defp map_to_camel({k, v}), do: {Recase.to_camel(to_string(k)), v}

  def is_datetime?(%DateTime{}), do: true
  def is_datetime?(_), do: false

  @doc """
  Склеивает две мапы первого уровня вложения

  ## Пример
  iex> Helper.Utils.deep_merge(%{a: "b", c: "d", d: %{e: "f", g: "h"}}, %{a: "c", b: "l", d: %{e: "g", g: "f"}})
  %{a: "c", b: "l", c: "d", d: %{e: "g", g: "f"}}
  """

  def deep_merge(left, right), do: Map.merge(left, right, &deep_resolve/3)
  defp deep_resolve(_key, %{} = left, %{} = right), do: deep_merge(left, right)
  defp deep_resolve(_key, _left, right), do: right

  @doc """
  Переводит значение в строку
  """

  def stringfy(v) when is_binary(v), do: v
  def stringfy(v) when is_integer(v), do: to_string(v)
  def stringfy(v) when is_atom(v), do: to_string(v)
  def stringfy(v), do: v

  @doc """
  Приводит значение к целочисленному
  """
  def integerfy(n) when is_binary(n), do: String.to_integer(n)
  def integerfy(n), do: n

  @doc """
  Повторяет значение x раз

  ## Пример
  iex> Helper.Utils.repeat(10, 3)
  [3, 3, 3, 3, 3, 3, 3, 3, 3, 3]

  iex> Helper.Utils.repeat(10, [3])
  <<3, 3, 3, 3, 3, 3, 3, 3, 3, 3>>
  """
  def repeat(times, [x]) when is_integer(x), do: to_string(repeat(times, x))
  def repeat(times, x), do: for(_ <- 1..times, do: x)

  @doc """
  Складывает два значения либо добавляет к первому значению единицу

  ## Пример
  iex> Helper.Utils.add(1)
  2

  iex> Helper.Utils.add(1, 2)
  3
  """
  def add(num, offset \\ 1) when is_integer(num) and is_integer(offset), do: num + offset

  @doc """
  Возвращает массив из значений с заданым ключом

  ## Пример
  iex> Helper.Utils.pick_by([%{test: "some data", any: "another"}, %{test: "some data 2", any: "another"}], :test)
  ["some data", "some data 2"]

  iex> Helper.Utils.pick_by([%{test: "some data", any: "another"}, %{test: "some data 2", any: "another"}], :any)
  ["another", "another"]
  """
  def pick_by(source, key) when is_list(source) and is_atom(key) do
    Enum.reduce(source, [], fn t, acc ->
      acc ++ [Map.get(t, key)]
    end)
  end

  @doc """
  Превращает именованый список в мапу

  ## Пример
  iex> Helper.Utils.map_atom_value([{:string, "struct"}, {:super, 2}], :string)
  %{string: "struct", super: 2}
  """

  def map_atom_value(attrs, :string) do
    results =
      Enum.map(attrs, fn {k, v} ->
        cond do
          v == true or v == false ->
            {k, v}

          is_atom(v) ->
            {k, v |> to_string() |> String.downcase()}

          true ->
            {k, v}
        end
      end)

    results |> Enum.into(%{})
  end

  def empty_pagination_data do
    %{entries: [], total_count: 0, page_size: 0, total_pages: 1, page_number: 1}
  end

  @doc """
  Подсчитывает число повторов

  ## Пример
  iex> Helper.Utils.count_words(["a", "b", "c", "c"])
  %{"a" => 1, "b" => 1, "c" => 2}
  """
  def count_words(words) when is_list(words), do: Enum.reduce(words, %{}, &count_words/2)
  defp count_words(word, acc), do: Map.update(acc, to_string(word), 1, &(&1 + 1))
end
