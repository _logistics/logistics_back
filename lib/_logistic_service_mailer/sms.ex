defmodule SMS do
  @moduledoc false
  @addr "https://smsc.ru/sys/send.php"
  @headers [{"content-type", "application/json"}]
  @login "atctemp"
  @password "CebyybNjl123"

  @doc """
  изначальное сообщение - Ваш секретный ключ для подтверждения аккаунта на сайте tempkurier.ru - #(code), или обратитесь в поддержку по адресу - tempkurier@yandex.ru
  """
  def make_message(code) do
    ~s(Ваш ключ для tempkurier.ru - #{code}. Поддержка - tempkurier@yandex.ru)
  end

  def send_to_provider(message, phone) do
    query = [login: @login, psw: @password, phones: phone, mes: message]
    Tesla.get!(@addr, query: query, headers: @headers)
  end

  def send_sms(phone, code) do
    %{body: body} = code |> make_message() |> send_to_provider(phone)

    if String.match?(body, ~r/ERROR/) do
      {:error, "неверный номер телефона"}
    else
      :ok
    end
  end
end
