defmodule Email do
  @moduledoc false
  @addrs "atctemp@yandex.ru cd5fddffsd@yandex.ru tempkurier@yandex.ru atktemp.spb@gmail.com"

  def make_message(order_id) do
    """
    Поступил заказ на доставку (либо был обновлен)
    Номер: #{order_id}
    Ссылка: http://tempkurier.ru/order/#{order_id}
    Адрес поддержки - tempkurier@yandex.ru
    """
  end

  def make_mail(message) do
    'echo "#{message}" | mail -s "NEW ORDER" -r "NOREPLY<noreply@tempkurier.ru>" so #{@addrs}'
  end

  def make_mail(message, title, emails) do
    'echo "#{message}" | mail -s "#{title}" -r "NOREPLY<noreply@tempkurier.ru>" so #{emails}'
  end

  def send_message(order_id), do: send_input_message(make_message(order_id), "NEW ORDER", @addrs)

  def send_input_message(message, title, emails) do
    message
    |> make_mail(title, emails)
    |> :os.cmd()

    {:ok, "Успешно отправлено"}
  end
end
