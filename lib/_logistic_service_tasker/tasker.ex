defmodule LogisticServiceTasker do
  @moduledoc false

  use GenServer
  require Logger
  @init_time 10_000
  @task_time 1000 * 60 * 60 * 24
  @smses_time 1000 * 60 * 60

  def start_link(_initial), do: GenServer.start_link(LogisticServiceTasker, nil)

  def init(_) do
    initial_job()
    {:ok, nil}
  end

  def handle_info(:receive, state) do
    try do
      LogisticService.Transfering.remove_old_invalid()
    rescue
      _e -> :ok
    end

    job()
    {:noreply, state}
  end

  def handle_info(:remove_smses, state) do
    LogisticService.Accounts.remove_old_smses()
    Process.send_after(self(), :remove_smses, @smses_time)
    {:noreply, state}
  end

  def mkdirs do
    File.mkdir("priv/static")
    File.mkdir("priv/static/docs")
  end

  def initial_job do
    Logger.info("first task succed")
    mkdirs()
    Process.send_after(self(), :receive, @init_time)
    Process.send_after(self(), :remove_smses, @smses_time)
  end

  def job do
    Process.send_after(self(), :receive, @task_time)
  end
end
