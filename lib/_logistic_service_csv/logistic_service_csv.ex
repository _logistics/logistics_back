defmodule LogisticServiceCsv do
  def read_csv(path, user) do
    path
    |> File.stream!()
    |> CSV.decode(headers: true)
    |> Stream.map(fn {:ok, map} ->
      map |> Morphix.atomorphiform!()
    end)
    |> Stream.map(&nilify_emptyness(&1))
    |> Enum.to_list()
    |> parse_names()
    |> remove_no_good()
    |> divide_product_order(user)
  end

  def namify({key, val}) do
    key =
      key
      |> Atom.to_string()
      |> String.downcase()
      |> String.split("(")
      |> List.first()
      |> String.to_atom()

    {key, val}
  end

  def parse_names(list) when is_list(list), do: Enum.map(list, &parse_names/1)

  def parse_names(map) when is_map(map) do
    map
    |> Enum.map(&namify/1)
    |> Enum.reduce(%{}, fn
      {:отношение, val}, acc -> acc |> Map.put(:relation, intify(val))
      {:"№", val}, acc -> acc |> Map.put(:number, intify(val))
      {:"время доставки", val}, acc -> acc |> Map.put(:delivery_awaited_datetime, timefy(val))
      {:"наименование продукта", val}, acc -> acc |> Map.put(:name, val)
      {:высота, val}, acc -> acc |> Map.put(:g_height, floatify(val))
      {:длинна, val}, acc -> acc |> Map.put(:g_length, floatify(val))
      {:ширина, val}, acc -> acc |> Map.put(:g_width, floatify(val))
      {:вес, val}, acc -> acc |> Map.put(:weight, floatify(val))
      {:"цена продукта", val}, acc -> acc |> Map.put(:price, intify(val))
      {:количество, val}, acc -> acc |> Map.put(:count, intify(val))
      {:"комментрий продукта", val}, acc -> acc |> Map.put(:comment, val)
      {:"адрес доставки", val}, acc -> acc |> Map.put(:receiver_address, val)
      {:"этаж доставки", val}, acc -> acc |> Map.put(:receiver_float, val)
      {:"квартира доставки", val}, acc -> acc |> Map.put(:receiver_apartment, val)
      {:"фио доставки", val}, acc -> acc |> Map.put(:receiver_fio, val)
      {:"телефон доставки", val}, acc -> acc |> Map.put(:receiver_phone, val)
      {:"область доставки", val}, acc -> acc |> Map.put(:receiver_area, val)
      {:"город доставки", val}, acc -> acc |> Map.put(:receiver_town, val)
      {:"комментарий доставки", val}, acc -> acc |> Map.put(:receiver_comment, val)
      {:"адрес отправки", val}, acc -> acc |> Map.put(:sender_address, val)
      {:"этаж отправки", val}, acc -> acc |> Map.put(:sender_float, val)
      {:"квартира отправки", val}, acc -> acc |> Map.put(:sender_apartment, val)
      {:"фио отправки", val}, acc -> acc |> Map.put(:sender_fio, val)
      {:"телефон отправки", val}, acc -> acc |> Map.put(:sender_phone, val)
      {:"область отправки", val}, acc -> acc |> Map.put(:sender_area, val)
      {:"город отправки", val}, acc -> acc |> Map.put(:sender_town, val)
      {:"комментарий отправки", val}, acc -> acc |> Map.put(:sender_comment, val)
      _any, acc -> acc
    end)
  end

  def divide_product_order(list, user) do
    products = list |> Enum.reject(&(&1.relation == 0))

    list
    |> Enum.reject(&(&1.relation > 0))
    |> Enum.map(fn order ->
      op =
        products
        |> Enum.filter(fn prod ->
          rel = Map.get(prod, :relation)
          rel && Map.get(order, :number) == rel
        end)

      order = Map.put(order, :products, op)

      order = LogisticService.Transfering.create_order(user, order)
      %{number: Map.get(order, :number), order: order}
    end)
  end

  def remove_no_good(list) when is_list(list),
    do: Enum.reject(list, fn el -> is_n?(el, :number) || is_n?(el, :relation) end)

  def intify(nil), do: nil
  def intify(""), do: nil
  def intify(val), do: Integer.parse(val) |> elem(0)
  def floatify(nil), do: nil
  def floatify(""), do: nil

  def floatify(val) do
    try do
      Float.parse(val) |> elem(0)
    rescue
      _e -> nil
    end
  end

  def timefy(nil), do: nil
  def timefy(""), do: nil

  def timefy(val) do
    Timex.parse!(val, "{ISO:Extended}")
  end

  defp is_n?(el, key) when is_map(el) do
    el |> Map.get(key) |> is_nil()
  end

  def nilify_emptyness(t) do
    Map.keys(t)
    |> Enum.reduce(%{}, fn key, obj ->
      cond do
        t[key] == "" or is_nil(t[key]) -> obj
        Regex.scan(~r/_id/, to_string(key)) != [] -> Map.put(obj, key, to_int(t[key]))
        t[key] -> Map.put(obj, key, validate_data(t[key]))
      end
    end)
  end

  def validate_data(data) when is_binary(data) do
    cond do
      Regex.scan(~r/{/, data) != [] and Regex.scan(~r/}/, data) != [] -> str_to_list(data)
      true -> data
    end
  end

  def to_int(str) do
    try do
      String.to_integer(str)
    rescue
      _e -> str
    end
  end

  def str_to_list(str) do
    str
    |> String.split("{")
    |> List.last()
    |> String.split("}")
    |> List.first()
    |> String.split(",")
    |> Enum.map(&String.trim(&1))
  end
end
