defmodule LogisticService.Factories.Makro do
  @moduledoc """
  Макросы для создания фабрик
  """

  defmacro base_multisert(context, entity) do
    quote do
      import Helper.Modules, only: [name_to_module_name: 1, fetch_main_module_name: 0]

      def unquote(:multisert)(data) do
        try do
          "Elixir.#{fetch_main_module_name()}.#{name_to_module_name(unquote(context))}"
          |> String.to_existing_atom()
          |> apply(unquote(:"multisert_#{entity}"), [data])
        rescue
          e -> {:error, e}
        end
      end
    end
  end
end
