defmodule LogisticService.Factories.PaymentTypes do
  @moduledoc """
  Этот модуль позволяет быстро генерировать пользователей
  """
  alias LogisticService.Transfering

  def multisert(users) do
    try do
      Transfering.multisert_payment_types!(users)
    rescue
      e -> {:error, e}
    end
  end
end
