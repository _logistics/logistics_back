defmodule LogisticService.Factories.Users do
  @moduledoc """
  Этот модуль позволяет быстро генерировать пользователей
  """
  alias LogisticService.Accounts

  def make_users(users) do
    users
    |> Enum.map(fn user ->
      roles =
        case Map.get(user, :roles) do
          nil ->
            nil

          roles ->
            roles
            |> String.split(",")
            |> Enum.map(&String.trim/1)
            |> Enum.map(&Accounts.get_role_by_slug/1)
        end

      Map.merge(user, %{
        phone: user.phone,
        password: user.password,
        password_confirmation: user.password,
        comments: "Init password: #{user.password}",
        roles: roles
      })
    end)
  end

  def multisert(users) do
    try do
      users
      |> make_users
      |> Accounts.multisert_users!()
    rescue
      e -> {:error, e}
    end
  end
end
