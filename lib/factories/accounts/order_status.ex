defmodule LogisticService.Factories.OrderStatus do
  @moduledoc """
  Этот модуль позволяет быстро генерировать пользователей
  """
  alias LogisticService.Transfering

  def multisert(users) do
    try do
      Transfering.multisert_order_statuses!(users)
    rescue
      e -> {:error, e}
    end
  end
end
