defmodule LogisticService.Factories.Roles do
  @moduledoc """
  Фабрика позволяющая быстро создать необходимые роли
  """
  import ShorterMaps
  alias LogisticService.Accounts
  import LogisticService.Factories.Makro

  base_multisert(:accounts, :roles)

  def random_attrs do
    slug = Faker.Company.name()

    ~M{slug}
  end

  def random(count \\ 1) do
    1..count
    |> Enum.map(fn _n ->
      random_attrs()
      |> prepare_role
    end)
  end

  defp prepare_role(role) do
    %{}
    |> Map.put(:slug, role.slug)
    |> Map.put(:notes, role[:notes])
  end

  def make_roles(roles) do
    roles
    |> Enum.map(fn role ->
      role
      |> prepare_role()
    end)
  end

  def multi_gen_insert(count \\ 1) do
    try do
      count
      |> random()
      |> Accounts.multisert_roles!()
    rescue
      e -> {:error, e}
    end
  end
end
