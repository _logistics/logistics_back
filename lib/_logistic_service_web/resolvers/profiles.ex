defmodule LogisticServiceWeb.Resolvers.Profiles do
  @moduledoc """
  Resolvers for Profiles context
  """
  import ShorterMaps
  alias LogisticService.Accounts

  # @admin_only_error {:error, "Этот запрос работает только для администратора"}
  @admin_or_user_error {:error,
                        "Этот запрос работает только для администратора или самого пользователя"}

  def get_user_profile(_, ~M{user_id}, _) do
    {:ok, Accounts.show_user_profile(user_id)}
  end

  def create_user_profile(_, attrs, %{context: ~M{current_user}}) do
    case verify_user_change_access(current_user, attrs) do
      true -> Accounts.create_user_profile(attrs[:user_id], attrs)
      false -> @admin_or_user_error
      _any -> @admin_or_user_error
    end
  end

  def create_user_profile(_, _user, _), do: @admin_or_user_error

  def update_user_profile(_, attrs, %{context: ~M{current_user}}) do
    case verify_user_change_access(current_user, attrs) do
      true -> Accounts.update_user_profile(attrs[:user_id], attrs)
      false -> @admin_or_user_error
      _any -> @admin_or_user_error
    end
  end

  def update_user_profile(_, _user, _), do: @admin_or_user_error

  def delete_user_profile(_, ~M{user_id} = attrs, %{context: ~M{current_user}}) do
    case verify_user_change_access(current_user, attrs) do
      true ->
        case Accounts.delete_user_profile(user_id) do
          {:ok, profile} ->
            {:ok,
             %{
               org_profile: profile,
               deletion_time: Timex.now(),
               deleted: true
             }}

          {:error, any} ->
            {:error, any}
        end

      false ->
        @admin_or_user_error

      _any ->
        @admin_or_user_error
    end
  end

  def get_org_profile(_, ~M{user_id}, _) do
    {:ok, Accounts.show_org_profile(user_id)}
  end

  def create_org_profile(_, attrs, %{context: ~M{current_user}}) do
    case verify_user_change_access(current_user, attrs) do
      true -> Accounts.create_org_profile(attrs[:user_id], attrs)
      false -> @admin_or_user_error
      _any -> @admin_or_user_error
    end
  end

  def create_org_profile(_, _user, _), do: @admin_or_user_error

  def update_org_profile(_, attrs, %{context: ~M{current_user}}) do
    case verify_user_change_access(current_user, attrs) do
      true -> Accounts.update_org_profile(attrs[:user_id], attrs)
      false -> @admin_or_user_error
      _any -> @admin_or_user_error
    end
  end

  def update_org_profile(_, _user, _), do: @admin_or_user_error

  def delete_org_profile(_, ~M{user_id} = attrs, %{context: ~M{current_user}}) do
    case verify_user_change_access(current_user, attrs) do
      true ->
        case Accounts.delete_org_profile(user_id) do
          {:ok, profile} ->
            {:ok,
             %{
               org_profile: profile,
               deletion_time: Timex.now(),
               deleted: true
             }}

          {:error, any} ->
            {:error, any}
        end

      false ->
        @admin_or_user_error

      _any ->
        @admin_or_user_error
    end
  end

  # defp verify_admin_access(cu), do: Accounts.is_admin?(cu)
  defp verify_user_change_access(curr_user, changed_user),
    do: Accounts.is_current?(curr_user, changed_user) || Accounts.is_admin?(curr_user)
end
