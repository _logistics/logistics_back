defmodule LogisticServiceWeb.Resolvers.Transfering do
  @moduledoc """
  Resolvers for Transfering context
  """
  import ShorterMaps
  alias LogisticService.Transfering
  # alias LogisticService.Accounts

  # @admin_only_error {:error, "Этот запрос работает только для администратора"}
  # @admin_or_user_error {:error, "Этот запрос работает только для администратора или самого пользователя"}
  @adm_error {:error, "Этот запрос работает только для пользователя"}

  def send_message(_, %{message: message, title: title, emails: emails}, _),
    do: Email.send_input_message(message, title, emails)

  def list_order_statuses(_, _, _) do
    {:ok, Transfering.list_order_statuses()}
  end

  def get_order_statuses(_, ~M{id}, _) do
    {:ok, Transfering.get_order_status!(id)}
  end

  def create_order_statuses(_, attrs, _) do
    {:ok, Transfering.create_order_status(attrs)}
  end

  def update_order_statuses(_, ~M{id} = attrs, _) do
    with order_status <- Transfering.get_order_status!(id) do
      {:ok, Transfering.update_order_status(order_status, attrs)}
    end
  end

  def delete_order_statuses(_, ~M{id}, _) do
    with order_status <- Transfering.get_order_status!(id) do
      {:ok, Transfering.delete_order_status(order_status)}
    end
  end

  def make_user_docs(_, ~M{user_id, dt_start, dt_finish}, _) do
    {:ok, LogisticServiceDocs.make_user_doc(user_id, dt_start, dt_finish)}
  end

  def make_all_docs(_, ~M{dt_start, dt_finish, user_id: 0}, _) do
    {:ok, LogisticServiceDocs.make_all_doc(dt_start, dt_finish)}
  end

  def make_all_docs(_, ~M{dt_start, dt_finish, user_id}, _) do
    {:ok, LogisticServiceDocs.make_all_doc(dt_start, dt_finish, user_id)}
  end

  def list_order_tarifs(_, _, _) do
    {:ok, Transfering.list_order_tarifs()}
  end

  def get_order_tarif(_, ~M{id}, _) do
    {:ok, Transfering.get_order_tarif!(id)}
  end

  def create_order_tarif(_, attrs, _) do
    {:ok, Transfering.create_order_tarif(attrs)}
  end

  def update_order_tarifs(_, ~M{id} = attrs, _) do
    with order_tarif <- Transfering.get_order_tarif!(id) do
      {:ok, Transfering.update_order_tarif(order_tarif, attrs)}
    end
  end

  def delete_order_tarif(_, ~M{id}, _) do
    with order_tarif <- Transfering.get_order_tarif!(id) do
      {:ok, Transfering.delete_order_tarif(order_tarif)}
    end
  end

  def send_courier(_, ~M{id, password}, _) do
    case LogisticServiceRelog.send_courier(id, password) do
      {:ok, any} -> {:ok, Poison.encode!(any)}
      {:error, _any} = data -> data
    end
  end

  def send_order(_, ~M{id}, _) do
    case LogisticServiceRelog.send_order(id) do
      {:ok, any} -> {:ok, Poison.encode!(any)}
      {:error, _any} = data -> data
    end
  end

  def send_client(_, ~M{id}, _) do
    case LogisticServiceRelog.send_client(id) do
      {:ok, any} -> {:ok, Poison.encode!(any)}
      {:error, _any} = data -> data
    end
  end

  def list_payment_types(_, _, _) do
    {:ok, Transfering.list_payment_types()}
  end

  def get_payment_types(_, ~M{id}, _) do
    {:ok, Transfering.get_payment_type!(id)}
  end

  def create_payment_types(_, attrs, _) do
    Transfering.create_payment_type(attrs)
  end

  def update_payment_types(_, ~M{id} = attrs, _) do
    with payment_type <- Transfering.get_payment_type!(id) do
      Transfering.update_payment_type(payment_type, attrs)
    end
  end

  def delete_payment_types(_, ~M{id}, _) do
    with payment_type <- Transfering.get_payment_type!(id) do
      Transfering.delete_payment_type(payment_type)
    end
  end

  def list_orders_sender(_, %{sender_id: user_id, filter: filter}, _) do
    {:ok, Transfering.list_orders(sender_id: user_id, criteries: filter)}
  end

  def list_orders_courier(_, %{courier_id: user_id, filter: filter}, _) do
    {:ok, Transfering.list_orders(courier_id: user_id, criteries: filter)}
  end

  def list_orders_for_graph(_, %{year: year, mth: mth} = args, _) do
    {:ok, Transfering.get_mth_report(Map.get(args, :user_id), year, mth)}
  end

  def list_orders_for_graph(_, %{year: year} = args, _) do
    {:ok, Transfering.get_year_report(Map.get(args, :user_id), year)}
  end

  def list_orders_for_graph(_, %{month: _mth} = _args, _) do
    {:error, "заказ неверно оформлен"}
  end

  def list_orders_for_graph(_, args, _) do
    {:ok, Transfering.get_all_time_report(Map.get(args, :user_id))}
  end

  def list_orders(_, %{user_id: 0, start_dt: start_dt, end_dt: end_dt}, _) do
    res = Transfering.list_orders(start_dt: start_dt, end_dt: end_dt)
    {:ok, res}
  end

  def list_orders(_, %{user_id: user_id, start_dt: start_dt, end_dt: end_dt}, _) do
    res = Transfering.list_orders(sender_id: user_id, start_dt: start_dt, end_dt: end_dt)
    {:ok, res}
  end

  def list_orders(_, ~m(filter)a, _) do
    {:ok, Transfering.list_orders(filter)}
  end

  def get_order(_, %{order_id: id, all: true}, _) do
    {:ok, Transfering.get_order!(:all, id)}
  end

  def get_order(_, %{order_id: id}, _) do
    {:ok, Transfering.get_order!(id)}
  end

  def get_order(_, _, _), do: @adm_error

  def create_order(_, attrs, %{context: %{current_user: user}}) do
    order = Transfering.create_order(user, attrs)

    {:ok, order}
  end

  def create_order(_, _, _), do: @adm_error

  def update_order(_, %{id: order_id} = attrs, %{context: %{current_user: user}}) do
    {:ok, Transfering.update_order(user, order_id, attrs)}
  end

  def set_order_courier(_, %{order_id: order_id, courier_id: courier_id}, _) do
    Transfering.set_order_courier(order_id, courier_id)
  end

  def delete_order(_, %{order_id: order_id, force: true}, _) do
    {:ok, Transfering.delete_order(:force, order_id)}
  end

  def delete_order(_, %{order_id: order_id}, %{context: %{current_user: user}}) do
    case Transfering.can_order_be_deleted?(order_id) do
      :ok ->
        {:ok, Transfering.delete_order(order_id, user)}

      {:error, _e} = e ->
        e

      :too_late ->
        {:error,
         "заказ не может быть удален так как уже прошел оформление, обратитесь к администратору"}
    end
  end

  def delete_order(_, _, _), do: @adm_error

  def put_products(_, %{order_id: order_id} = product, _) do
    {:ok, Transfering.put_products(order_id, product)}
  end

  def put_products(_, _, _), do: @adm_error

  def update_product(_, %{product_id: product_id} = product, _),
    do: Transfering.update_product(product_id, product)

  def update_product(_, _, _), do: @adm_error

  def delete_product(_, %{product_id: product_id}, _) do
    # case Transfering.can_product_be_deleted?(product_id) do
    #   :ok ->
    #   {:error, _e} = e -> e
    #   :too_late -> {:error, "продукт не может быть удален так как заказ уже прошел оформление, обратитесь к администратору"}
    # end
    {:ok, Transfering.delete_product(product_id)}
  end

  def delete_product(_, _, _), do: @adm_error

  def get_last_incompleted(_, %{user_id: user_id}, _) do
    {:ok, Transfering.get_last_incompleted(user_id)}
  end

  def get_last_incompleted(_, _, _), do: @adm_error

  def watch_prepared_order(_, %{id: id}, _) do
    case Transfering.watch_prepared_order(id) do
      {:error, msg} ->
        {:error, msg}

      order ->
        client = Map.merge(order.client, %{is_organization: order.client.isOrganization})

        ord =
          Map.merge(order, %{
            client: client,
            order_sum: order.orderSum,
            create_date: order.createDate,
            shipment_date: order.createDate,
            contact_person: order.contactPerson
          })

        {:ok, %{order: ord}}
    end
  end

  def send_prepared_order(_, %{id: id}, _) do
    case Transfering.send_prepared_order(id) do
      {:error, msg} -> {:error, msg}
      response -> {:ok, %{message: "order_sended", response: response}}
    end
  end

  def confirm_order_and_send(_, %{id: id}, _) do
    case Transfering.confirm_order_and_send(id) do
      {:error, msg} -> {:error, msg}
      response -> {:ok, %{message: "order_sended", response: response}}
    end
  end

  def force_validate(_, %{id: id}, _) do
    {:ok, Transfering.force_validate(id)}
  end

  def update_status_at_order(_, %{id: id, status_id: status_id}, _) do
    {:ok, Transfering.update_status_at_order(id, status_id)}
  end

  def get_from_one_c(_, %{id: id}, _) do
    {:ok, Transfering.get_from_one_c(id)}
  end

  # defp verify_admin_access(cu), do: Accounts.is_admin?(cu)
  # defp verify_user_chansge_access(curr_user, changed_user), do: Accounts.is_current?(curr_user, changed_user) || Accounts.is_admin?(curr_user)
end
