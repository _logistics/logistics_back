defmodule LogisticServiceWeb.Resolvers.Accounts do
  @moduledoc """
  Resolvers for Accounts context
  """
  import ShorterMaps
  alias LogisticService.{Accounts, Transfering}

  @admin_only_error {:error, "Этот запрос работает только для администратора"}
  @admin_or_user_error {:error,
                        "Этот запрос работает только для администратора или самого пользователя"}

  def users(_root, ~m(filter, is_active)a, _info) do
    case is_active do
      :all -> Accounts.all_users(filter)
      :deleted -> Accounts.deleted_users(filter)
      :active -> Accounts.users(filter)
    end
  end

  def couriers(_root, ~m(filter)a, _info), do: Accounts.list_users(:courier, filter)

  def get_unique_users(_root, ~m(user_ids)a, _info),
    do: {:ok, Accounts.get_unique_users(user_ids)}

  def new_users(_root, ~m(dt_start, dt_finish, divided)a, _info) do
    case Accounts.new_users(dt_start, dt_finish, divided) do
      {:ok, count} ->
        {:ok,
         %{
           count: count,
           message: "Количество новых пользователей за период: c #{dt_start} по #{dt_finish}"
         }}

      other ->
        other
    end
  end

  def count_users(_root, _, _info) do
    try do
      {:ok, count} = Accounts.count_users()
      {:ok, %{count: count, message: "Общее количество пользователей"}}
    rescue
      _r -> {:error, "cant get"}
    end
  end

  def count_order_summ(_root, ~m(dt_start, dt_finish, divided)a = attrs, _info) do
    case Map.get(attrs, :user_id) do
      nil ->
        case Transfering.count_order_summ(dt_start, dt_finish, divided) do
          {:ok, count} ->
            {:ok,
             %{
               count: count,
               message: "Сумма заказов на сайте за период: c #{dt_start} по #{dt_finish}"
             }}

          other ->
            other
        end

      user_id ->
        case Transfering.count_order_summ(dt_start, dt_finish, divided, user_id) do
          {:ok, count} ->
            {:ok,
             %{
               count: count,
               message:
                 "Сумма заказов пользователя #{user_id} за период: c #{dt_start} по #{dt_finish}"
             }}

          other ->
            other
        end
    end
  end

  def count_transfered_orders(_root, ~m(dt_start, dt_finish, divided)a = attrs, _info) do
    case Map.get(attrs, :user_id) do
      nil ->
        case Transfering.count_transfered_orders(dt_start, dt_finish, divided) do
          {:ok, count} ->
            {:ok,
             %{
               count: count,
               message:
                 "Количество заказов со статусом 'доставлен' за период: c #{dt_start} по #{
                   dt_finish
                 }"
             }}

          other ->
            other
        end

      user_id ->
        case Transfering.count_transfered_orders(dt_start, dt_finish, divided, user_id) do
          {:ok, count} ->
            {:ok,
             %{
               count: count,
               message:
                 "Количество заказов со статусом 'доставлен' пользователя #{user_id} за период: c #{
                   dt_start
                 } по #{dt_finish}"
             }}

          other ->
            other
        end
    end
  end

  def count_orders(_root, ~m(dt_start, dt_finish, divided)a = attrs, _info) do
    case Map.get(attrs, :user_id) do
      nil ->
        case Transfering.count_orders(dt_start, dt_finish, divided) do
          {:ok, count} ->
            {:ok,
             %{
               count: count,
               message: "Количество заказов за период: c #{dt_start} по #{dt_finish}"
             }}

          other ->
            other
        end

      user_id ->
        case Transfering.count_orders(dt_start, dt_finish, divided, user_id) do
          {:ok, count} ->
            {:ok,
             %{
               count: count,
               message:
                 "Количество заказов пользователя #{user_id} за период: c #{dt_start} по #{
                   dt_finish
                 }"
             }}

          other ->
            other
        end
    end
  end

  def signup(_, %{phone: phone}, _), do: Accounts.signup(phone)
  def reset_password(_, %{phone: phone}, _), do: Accounts.signup(phone)

  def submit_signup(_, %{phone: phone, code: code} = attrs, _),
    do: Accounts.submit_signup(phone, code, attrs)

  def update_password(
        _,
        %{
          phone: phone,
          code: code,
          password: password,
          password_confirmation: password_confirmation
        },
        _
      ),
      do: Accounts.update_password(phone, code, password, password_confirmation)

  def admin_signup(_, attrs, %{context: %{current_user: cu}}) do
    case verify_admin_access(cu) do
      true -> Accounts.signup(attrs)
      _any -> @admin_only_error
    end
  end

  def signin(_, args, _), do: Accounts.signin(args)

  def get_user(_, %{id: id}, _), do: {:ok, Accounts.get_user!(id)}

  def upsert_user_roles(_, %{user_id: id, roles: roles}, _),
    do: Accounts.upsert_user_roles(id, roles)

  def update_user(_, user, %{context: %{current_user: cu}}) do
    case verify_user_change_access(cu, user) do
      true -> Accounts.update_user(user)
      false -> @admin_or_user_error
      _any -> @admin_or_user_error
    end
  end

  def update_user(_, _user, _), do: @admin_or_user_error

  def soft_delete_user(_, %{id: id} = user, %{context: %{current_user: cu}}) do
    case verify_user_change_access(cu, user) do
      true ->
        Accounts.soft_delete_user(id)

      false ->
        @admin_or_user_error

      _any ->
        @admin_or_user_error
    end
  end

  def soft_delete_user(_, _user, _), do: @admin_or_user_error

  def restore_user(_, %{id: id}, %{context: %{current_user: cu}}) do
    case verify_admin_access(cu) do
      true -> Accounts.restore_user(id)
      _any -> @admin_only_error
    end
  end

  def restore_user(_, _user, _), do: @admin_only_error

  def me(_, _, %{context: %{current_user: cu}}), do: {:ok, cu}
  def me(_, _, _), do: {:ok, nil}

  defp verify_admin_access(cu), do: Accounts.is_admin?(cu)

  defp verify_user_change_access(curr_user, changed_user),
    do: Accounts.is_current?(curr_user, changed_user) || Accounts.is_admin?(curr_user)
end
