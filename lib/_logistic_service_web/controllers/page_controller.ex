defmodule LogisticServiceWeb.PageController do
  use LogisticServiceWeb, :controller

  def index(conn, _params) do
    render(conn, "index.json", message: "Добро пожаловать на сервер логистики!")
  end
end
