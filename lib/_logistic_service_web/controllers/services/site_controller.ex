defmodule LogisticServiceWeb.SiteController do
  use LogisticServiceWeb, :controller

  def index(conn, _params) do
    render(conn, "index.json", sites: [])
  end
end
