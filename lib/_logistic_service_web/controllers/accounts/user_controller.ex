defmodule LogisticServiceWeb.UserController do
  use LogisticServiceWeb, :controller

  alias LogisticService.Accounts
  alias LogisticService.Accounts.Models.User

  def index_all(conn, _params) do
    users = Accounts.list_users()
    render(conn, "index.json", users: users)
  end

  def deleted_all(conn, _params) do
    users = Accounts.list_users(false)
    render(conn, "index.json", users: users)
  end

  def all_all(conn, _params) do
    users = Accounts.list_users(:all)
    render(conn, "index.json", users: users)
  end

  def show(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    render(conn, "show.json", user: user)
  end

  def by_phone(conn, %{"phone" => phone}) do
    user = Accounts.get_user_by_phone!(%{phone: phone})
    render(conn, "show.json", user: user)
  end

  # def usr_perm(conn, %{"params" => %{"user_id" => user_id, "permissions_id" => permissions_id}}) do
  #   user_permissions = Accounts.create_user_permission(%{user_id: user_id, permission_id: permissions_id})
  #   render(conn, "usr_permission.json", user_permissions: user_permissions)
  # end

  # def usr_perm_del(conn, %{"params" => %{"user_id" => user_id, "permissions_id" => permissions_id}}) do
  #   user_permissions = Accounts.delete_user_permission(%{user_id: user_id, permission_id: permissions_id})
  #   render(conn, "usr_permission.json", user_permissions: user_permissions)
  # end

  def get_user_session(conn, %{"phone" => phone, "password" => password}) do
    {:ok, session} = Accounts.signin(%{phone: phone, password: password})
    render(conn, "get_user_session.json", user_session: session)
  end

  def create(conn, %{"user" => user_params}) do
    try do
      %{
        "first_name" => _first_name,
        "phone" => _phone,
        "password" => _password,
        "password_confirmation" => _password_confirmation
      } = user_params

      with {:ok, %User{} = user} <- Accounts.create_user(user_params) do
        render(conn, "updated_user.json",
          upd: %{user: user, upd_time: Time.to_string(Time.utc_now())}
        )
      else
        e -> render(conn, "error_user.json", error: e)
      end
    rescue
      _e ->
        render(conn, "error_user.json",
          error:
            {:error,
             %{
               errors: [
                 {:fields_error,
                  {"поля phone, password и password_confirmation - обязательны", 3}}
               ]
             }}
        )
    end
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Accounts.get_user!(id)

    with {:ok, %User{} = user} <- Accounts.update_user(user, user_params) do
      render(conn, "updated_user.json",
        upd: %{user: user, upd_time: Time.to_string(Time.utc_now())}
      )
    else
      e -> render(conn, "error_user.json", error: e)
    end
  end

  def delete(conn, %{"id" => id}) do
    with {:ok, %User{} = user} <- Accounts.soft_delete_user(id) do
      render(conn, "deleted_user.json",
        del: %{user: user, del_time: Time.to_string(Time.utc_now())}
      )
    else
      e -> render(conn, "error_user.json", error: e)
    end
  end

  def recover(conn, %{"id" => id}) do
    with {:ok, %User{} = user} <- Accounts.restore_user(id) do
      render(conn, "recovered_user.json",
        rec: %{user: user, recovery_time: Time.to_string(Time.utc_now())}
      )
    else
      e -> render(conn, "error_user.json", error: e)
    end
  end
end
