defmodule LogisticServiceWeb.FallbackController do
  @moduledoc """
  Translates controller action results into valid `Plug.Conn` responses.

  See `Phoenix.Controller.action_fallback/1` for more details.
  """
  use LogisticServiceWeb, :controller

  # This clause handles errors returned by Ecto's insert/update/delete.
  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(LogisticServiceWeb.ChangesetView)
    |> render("error.json", changeset: changeset)
  end

  # This clause is an example of how to handle resources that cannot be found.
  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> put_view(LogisticServiceWeb.ErrorView)
    |> render(:"404")
  end

  def no_context(conn) do
    conn
    |> put_status(401)
    |> put_view(LogisticServiceWeb.ErrorView)
    |> render(:unauth_site)
  end

  def admin_only(conn) do
    conn
    |> put_status(401)
    |> put_view(LogisticServiceWeb.ErrorView)
    |> render(:admin_only)
  end
end
