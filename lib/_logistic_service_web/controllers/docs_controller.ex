defmodule LogisticServiceWeb.DocsController do
  use LogisticServiceWeb, :controller

  def index(conn, _params) do
    docs = [
      13_213,
      32_132,
      412
    ]

    render(conn, "index.json", docs: docs)
  end

  def file_reader(conn, %{"b64" => base64, "user" => user}) do
    {:ok, data} = Base.decode64(base64)
    File.write("sheet.csv", data, [:binary])
    res = LogisticServiceCsv.read_csv("sheet.csv", user)

    render(conn, "readed.json", res: res)
  end
end
