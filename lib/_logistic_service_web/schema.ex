defmodule LogisticServiceWeb.Schema do
  @moduledoc """
  this is Graphql Api Schema
  """
  use Absinthe.Schema

  alias LogisticServiceWeb.Middleware, as: M
  alias LogisticServiceWeb.Schema.{Accounts, Utils.CommonTypes, Profiles, Transfering}

  import_types(Absinthe.Type.Custom)
  import_types(CommonTypes)

  import_types(Accounts.Mutations)
  import_types(Accounts.Queries)
  import_types(Accounts.Types)

  import_types(Profiles.Mutations)
  import_types(Profiles.Queries)
  import_types(Profiles.Types)

  import_types(Transfering.Mutations)
  import_types(Transfering.Queries)
  import_types(Transfering.Types)

  query do
    import_fields(:accounts_queries)
    import_fields(:profiles_queries)
    import_fields(:transfering_queries)
  end

  mutation do
    import_fields(:accounts_mutations)
    import_fields(:profiles_mutations)
    import_fields(:transfering_mutations)
  end

  def middleware(middleware, _field, %{identifier: :query}),
    do: [ApolloTracing.Middleware.Caching] ++ middleware ++ [M.Error]

  def middleware(middleware, _field, %{identifier: :mutation}),
    do: middleware ++ [M.ChangesetErrors]

  def middleware(middleware, _field, _), do: [ApolloTracing.Middleware.Tracing] ++ middleware

  def plugins do
    [Absinthe.Middleware.Dataloader | Absinthe.Plugin.defaults()]
  end

  @spec dataloader :: Dataloader.t()
  def dataloader do
    alias LogisticService.{Accounts, Transfering}

    Dataloader.new()
    |> Dataloader.add_source(Accounts, Accounts.Utils.Loader.datasource())
    |> Dataloader.add_source(Transfering, Transfering.Utils.Loader.datasource())
  end

  def context(ctx), do: ctx |> Map.put(:loader, dataloader())
end
