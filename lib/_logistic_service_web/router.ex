defmodule LogisticServiceWeb.Router do
  @dialyzer {:no_return, __checks__: 0}
  # @dialyzer {:nowarn_function, rollback: 1}
  @moduledoc """
  Модуль отвечающий за роутинг
  Pipeline - то, что проходит запрос до того, как отвечает на него
  Scope - группа запросов
  """
  use LogisticServiceWeb, :router
  alias LogisticServiceWeb.Plugs.Context, as: Ctx
  alias LogisticServiceWeb.Plugs.Verification, as: Verify

  pipeline :api do
    plug :accepts, ["json"]
    plug Ctx.User
    plug Ctx.Site
  end

  pipeline :site_auth_api do
    plug :accepts, ~w(json)
    plug Ctx.User
    plug Ctx.Site
    plug Verify.Site
  end

  pipeline :admin_api do
    plug :accepts, ~w(json)
    plug Verify.AdminUser
  end

  pipeline :user_changes do
    plug :accepts, ~w(json)
    plug Ctx.User
    plug Ctx.Site
    plug Verify.Site
    plug Verify.UserAccessToChange
  end

  scope "/" do
    pipe_through :api

    get "/", LogisticServiceWeb.DocsController, :index
  end

  scope "/help" do
    pipe_through :api

    get "/", LogisticServiceWeb.PageController, :index

    forward "/graphiql", Absinthe.Plug.GraphiQL,
      pipeline: {ApolloTracing.Pipeline, :plug},
      schema: LogisticServiceWeb.Schema,
      socket: LogisticServiceWeb.UserSocket,
      interface: :playground,
      origins: "*"
  end

  scope "/api" do
    pipe_through [:api, :site_auth_api]

    post "/file_reader", LogisticServiceWeb.DocsController, :file_reader

    forward "/gql_api", Absinthe.Plug,
      schema: LogisticServiceWeb.Schema,
      origins: "*"
  end

  scope "/resources" do
    pipe_through [:fetch_session, :protect_from_forgery]
    import Phoenix.LiveDashboard.Router
    live_dashboard "/", metrics: LogisticServiceWeb.Telemetry
  end
end
