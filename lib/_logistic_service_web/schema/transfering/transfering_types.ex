defmodule LogisticServiceWeb.Schema.Transfering.Types do
  @moduledoc """
    chat GraphQL mutations
  """
  use Helper.GqlSchemaSuite
  alias LogisticService.Transfering

  # alias LogisticService.Transfering.Models.{OrderTarif, Order, PaymentType, OrderProducts, OrderStatus}
  import LogisticServiceWeb.Schema.Utils.Helper

  @desc "заказ"
  object :order do
    @desc "данные от 1 с"
    field :one_c_data, :http_one_c_response
    # field :relog_data, :json
    @desc "идентификатор"
    field :id, :integer
    @desc "ссылка на карту"
    field :track_link, :string

    field :receiver_apartment, :string
    field :receiver_float, :string
    field :receiver_address, :string
    field :receiver_area, :string
    field :receiver_fio, :string
    field :receiver_phone, :string
    field :receiver_state, :string
    field :receiver_town, :string
    field :receiver_comment, :string

    @desc "цена"
    field :price, :integer

    field :sender_apartment, :string
    field :sender_float, :string
    field :sender_address, :string
    field :sender_area, :string
    field :sender_fio, :string
    field :sender_phone, :string
    field :sender_state, :string
    field :sender_town, :string
    field :sender_comment, :string
    @desc "ожидаемое время доставки"
    field :delivery_awaited_datetime, :naive_datetime

    @desc "заполнен ли достаточно"
    field :is_accepted, :boolean
    @desc "время заполнения"
    field :acceptance_datetime, :naive_datetime

    @desc "доставлен ли"
    field :is_delivered, :boolean
    @desc "подтверждена ли доставка"
    field :delivery_proof_datetime, :naive_datetime
    @desc "время создания"
    field :inserted_at, :naive_datetime
    @desc "время обновления"
    field :updated_at, :naive_datetime

    @desc "отправлен организаций"
    field :is_organization, :boolean
    @desc "может ли быть отправлен"
    field :can_be_sent, :boolean
    @desc "валиден ли"
    field :is_valid, :boolean

    @desc "почта на которую будут отсылаться письма"
    field :sendage_email, :string
    @desc "номер накладной"
    field :invoice_number, :string
    @desc "комментарий к платежу"
    field :payment_comment, :string
    @desc "страховка"
    field :insurance, :string

    @desc "сдача с"
    field :odd_money, :string
    @desc "кто является плательщиком"
    field :payer, :string

    @desc "дата удаления"
    field :deleted_at, :naive_datetime

    @desc "статус заказа"
    field :order_status, :order_status,
      resolve: dataloader(Transfering, :order_status, args: %{scope: :order_status})

    @desc "тариф заказа"
    field :order_tarif, :order_tarif,
      resolve: dataloader(Transfering, :order_tarif, args: %{scope: :order_tarif})

    @desc "тип платежа"
    field :payment_type, :payment_type,
      resolve: dataloader(Transfering, :payment_type, args: %{scope: :payment_type})


    @desc "список продуктов"
    field :products, list_of(:products),
      resolve: dataloader(Transfering, :products, args: %{scope: :products})

    @desc "отправитель"
    field :sender, :user, resolve: dataloader(Transfering, :sender, args: %{scope: :sender})
    @desc "курьер"
    field :courier, :user, resolve: dataloader(Transfering, :courier, args: %{scope: :courier})

    @desc "кем обновлен"
    field :updated_by, :user,
      resolve: dataloader(Transfering, :updated_by, args: %{scope: :updated_by})

    @desc "кем удален"
    field :deleted_by, :user,
    resolve: dataloader(Transfering, :deleted_by, args: %{scope: :deleted_by})
  end

  @desc "отчет о заказах для графиков (устарел)"
  object :order_report do
    @desc "количество"
    field :count, :integer
    @desc "цена"
    field :price, :decimal
    @desc "список заказов"
    field :order_list, list_of(:order_for_graph)
    @desc "дата создания"
    field :inserted, :string
    @desc "количество оплаченых"
    field :payed_count, :integer
    @desc "сумма оплаченых"
    field :payed_sum, :integer
    @desc "количество не оплаченых"
    field :not_payed_count, :integer
    @desc "сумма оплаченых"
    field :not_payed_sum, :integer
    @desc "количество не доставленных"
    field :not_delivered_count, :integer
    @desc "сумма не доставленных"
    field :not_delivered_sum, :integer
  end

  @desc "заказ для графиков (устарел)"
  object :order_for_graph do
    @desc "данные от 1 с"
    field :one_c_data, :http_one_c_response
    # field :relog_data, :json
    @desc "идентификатор"
    field :id, :integer
    @desc "ссылка на карту"
    field :track_link, :string

    field :receiver_apartment, :string
    field :receiver_float, :string
    field :receiver_address, :string
    field :receiver_area, :string
    field :receiver_fio, :string
    field :receiver_phone, :string
    field :receiver_state, :string
    field :receiver_town, :string
    field :receiver_comment, :string

    @desc "цена"
    field :price, :integer

    field :sender_apartment, :string
    field :sender_float, :string
    field :sender_address, :string
    field :sender_area, :string
    field :sender_fio, :string
    field :sender_phone, :string
    field :sender_state, :string
    field :sender_town, :string
    field :sender_comment, :string
    @desc "ожидаемое время доставки"
    field :delivery_awaited_datetime, :naive_datetime

    @desc "заполнен ли достаточно"
    field :is_accepted, :boolean
    @desc "время заполнения"
    field :acceptance_datetime, :naive_datetime

    @desc "доставлен ли"
    field :is_delivered, :boolean
    @desc "подтверждена ли доставка"
    field :delivery_proof_datetime, :naive_datetime
    @desc "время создания"
    field :inserted_at, :naive_datetime
    @desc "время обновления"
    field :updated_at, :naive_datetime

    @desc "отправлен организаций"
    field :is_organization, :boolean
    @desc "может ли быть отправлен"
    field :can_be_sent, :boolean
    @desc "валиден ли"
    field :is_valid, :boolean

    @desc "почта на которую будут отсылаться письма"
    field :sendage_email, :string
    @desc "номер накладной"
    field :invoice_number, :string
    @desc "комментарий к платежу"
    field :payment_comment, :string
    @desc "страховка"
    field :insurance, :string

    @desc "сдача с"
    field :odd_money, :string
    @desc "кто является плательщиком"
    field :payer, :string

    field :deleted_at, :naive_datetime

    field :order_status, :string
    field :order_status_id, :integer
  end

  @desc "обработанный заказ"
  object :prepared_order do
    @desc "заказ"
    field :order, :prepared_order_info
  end

  @desc "статус отправки"
  object :sendage_status do
    @desc "сообщение"
    field :message, :string
    @desc "ответ от 1с"
    field :response, :http_one_c_response
  end

  @desc "ответ от 1с"
  object :http_one_c_response do
    @desc "ошибки"
    field :error, :string
    @desc "ответ от 1 с"
    field :one_c_response, :one_c_response
    @desc "статус записи"
    field :status, :boolean
  end

  object :one_c_response do
    @desc "идентификатор"
    field :id, :integer
  end

  @desc "информация о заказе"
  object :prepared_order_info do
    @desc "идентификатор"
    field :id, :integer
    @desc "стоимость заказ"
    field :order_sum, :integer
    @desc "комментарий доставки"
    field :comment, :string
    @desc "дата доставки"
    field :shipment_date, :string
    @desc "дата создания"
    field :create_date, :string
    @desc "клиент"
    field :client, :prepared_client
    @desc "контактное лицо"
    field :contact_person, :prepared_contact_person
    @desc "список продуктов"
    field :products, list_of(:prepared_product)
  end

  @desc "контактное лицо"
  object :prepared_contact_person do
    @desc "id"
    field :id, :integer
    @desc "имя"
    field :name, :string
    @desc "почта"
    field :email, :string
    @desc "телефон"
    field :phone, :string
    @desc "адрес"
    field :address, :string
  end

  @desc "клиент"
  object :prepared_client do
    @desc "идентификатор"
    field :id, :integer
    @desc "имя"
    field :name, :string
    @desc "телефон"
    field :phone, :string
    @desc "инн"
    field :inn, :string
    @desc "кпп"
    field :kpp, :string
    @desc "является ли организацией"
    field :is_organization, :boolean
    @desc "почта"
    field :contacts, :prepared_contacts
  end

  @desc "контакты"
  object :prepared_contacts do
    @desc "почта"
    field :email, :string
    @desc "телефон"
    field :phone, :string
  end

  @desc "адреса"
  object :prepared_bank do
    @desc "бик"
    field :bik, :string
  end

  @desc "продукты"
  object :prepared_product do
    field :id, :integer
    @desc "количество"
    field :count, :integer
    @desc "цена"
    field :price, :integer
    @desc "наименование"
    field :name, :string
  end

  @desc "пагинированный список заказов"
  object :paged_orders do
    field :entries, list_of(:order)
    pagination_fields()
  end

  @desc "статус заказа"
  object :order_status do
    @desc "id заказа"
    field :id, :integer
    @desc "наименование статуса"
    field :name, :string
    # field :rules, :json

    field :order, :order, resolve: dataloader(Transfering, :order, args: %{scope: :order})
  end

  @desc "тариф заказа"
  object :order_tarif do
    @desc "идентификатор"
    field :id, :integer
    @desc "наименование"
    field :name, :string
    # field :rules, :json

    @desc "заказы по указанному тарифу"
    field :order, :order, resolve: dataloader(Transfering, :order, args: %{scope: :order})
  end

  @desc "тип платежа"
  object :payment_type do
    @desc "идентификатор"
    field :id, :integer
    @desc "наименование"
    field :name, :string
    @desc "комментарий"
    field :comment, :string
    # field :rules, :json

    @desc "заказы по указанному типу"
    field :order, :order, resolve: dataloader(Transfering, :order, args: %{scope: :order})
  end

  @desc "продукт"
  object :products do
    @desc "идентификатор"
    field :id, :integer
    @desc "имя продукта"
    field :name, :string
    @desc "высота"
    field :g_height, :float
    @desc "длинна"
    field :g_length, :float
    @desc "ширина"
    field :g_width, :float
    @desc "местоположение на складе"
    field :places, :string
    @desc "комментарий"
    field :comment, :string
    @desc "объем"
    field :volume, :float
    @desc "вес"
    field :weight, :float
    @desc "цена"
    field :price, :integer
    @desc "количество"
    field :count, :integer
    field :order, :order, resolve: dataloader(Transfering, :order, args: %{scope: :order})
  end

  @desc "ссылка на сгенерированные документы"
  object :docs_links do
    @desc "ссылка на сгенерированный документ excel"
    field :excel, :string
    @desc "ссылка на сгенерированный документ pdf"
    field :pdf, :string
  end

  @desc "вставка продуктов"
  input_object :i_product do
    @desc "имя продукта"
    field :name, :string
    @desc "высота"
    field :g_height, :float
    @desc "длинна"
    field :g_length, :float
    @desc "ширина"
    field :g_width, :float
    @desc "местоположение на складе"
    field :places, :string
    @desc "комментарий"
    field :comment, :string
    @desc "объем"
    field :volume, :float
    @desc "вес"
    field :weight, :float
    @desc "цена"
    field :price, :integer
    @desc "количество"
    field :count, :integer
  end
end
