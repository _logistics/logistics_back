defmodule LogisticServiceWeb.Schema.Transfering.Mutations do
  @moduledoc """
  Accounts Mutations Schema
  """
  use Helper.GqlSchemaSuite

  object :transfering_mutations do
    @desc "Отправить сообщение о статусе заказа"
    field :send_message, :string do
      @desc "сообщение"
      arg(:message, non_null(:string))
      @desc "заголовок"
      arg(:title, non_null(:string))
      @desc "почта"
      arg(:emails, non_null(:string))
      resolve(safely(&R.Transfering.send_message/3))
    end

    @desc "Создать статус заказов"
    field :create_order_statuses, :order_status do
      @desc "название"
      arg(:name, :string)
      resolve(safely(&R.Transfering.create_order_statuses/3))
    end

    @desc "Создать тариф заказов"
    field :create_order_tarif, :order_tarif do
      @desc "название"
      arg(:name, :string)
      resolve(safely(&R.Transfering.create_order_tarif/3))
    end

    @desc "Создать тип платежа"
    field :create_payment_types, :payment_type do
      @desc "название"
      arg(:name, :string)
      @desc "комментарий"
      arg(:comment, :string)
      resolve(safely(&R.Transfering.create_payment_types/3))
    end

    @desc "Обновить статус заказов"
    field :update_order_statuses, :order_status do
      @desc "идентификатор"
      arg(:id, :integer)
      @desc "название"
      arg(:name, :string)
      resolve(safely(&R.Transfering.update_order_statuses/3))
    end

    @desc "Обновить тариф заказов"
    field :update_order_tarifs, :order_tarif do
      @desc "идентификатор"
      arg(:id, :integer)
      @desc "название"
      arg(:name, :string)
      resolve(safely(&R.Transfering.update_order_tarifs/3))
    end

    @desc "Обновить тип платежа"
    field :update_payment_types, :payment_type do
      @desc "идентификатор"
      arg(:id, :integer)
      @desc "название"
      arg(:name, :string)
      @desc "комментарий"
      arg(:comment, :string)
      resolve(safely(&R.Transfering.update_payment_types/3))
    end

    @desc "Удалить статус заказов"
    field :delete_order_statuses, :order_status do
      @desc "идентификатор"
      arg(:id, :integer)
      resolve(safely(&R.Transfering.delete_order_statuses/3))
    end

    @desc "Удалить тариф заказов"
    field :delete_order_tarif, :order_tarif do
      @desc "идентификатор"
      arg(:id, :integer)
      resolve(safely(&R.Transfering.delete_order_tarif/3))
    end

    @desc "Удалить тип платежа"
    field :delete_payment_types, :payment_type do
      @desc "идентификатор"
      arg(:id, :integer)
      resolve(safely(&R.Transfering.delete_payment_types/3))
    end

    @desc "Прикрепить новый продукт к заказу "
    field :put_product, :products do
      @desc "id заказа"
      arg(:order_id, non_null(:integer))
      @desc "название продукта"
      arg(:name, non_null(:string))
      @desc "высота"
      arg(:g_height, :float)
      @desc "длинна"
      arg(:g_length, :float)
      @desc "ширина"
      arg(:g_width, :float)
      @desc "цена"
      arg(:price, non_null(:integer))
      @desc "количество"
      arg(:count, non_null(:integer))
      # arg :placement_info, :json
      @desc "место на складе"
      arg(:places, :string)
      @desc "объем"
      arg(:volume, :float)
      @desc "вес"
      arg(:weight, :float)
      resolve(safely(&R.Transfering.put_products/3))
    end

    @desc "Обновить продукт"
    field :update_product, :products do
      @desc "id продукта"
      arg(:product_id, non_null(:integer))
      @desc "название продукта"
      arg(:name, non_null(:string))
      @desc "высота"
      arg(:g_height, :float)
      @desc "длинна"
      arg(:g_length, :float)
      @desc "ширина"
      arg(:g_width, :float)
      @desc "цена"
      arg(:price, non_null(:integer))
      @desc "количество"
      arg(:count, non_null(:integer))
      # arg :placement_info, :json
      @desc "место на складе"
      arg(:places, :string)
      @desc "объем"
      arg(:volume, :float)
      @desc "вес"
      arg(:weight, :float)
      resolve(safely(&R.Transfering.update_product/3))
    end

    @desc "Удалить продукт"
    field :delete_product, :products do
      arg(:product_id, non_null(:integer))
      resolve(safely(&R.Transfering.delete_product/3))
    end

    @desc "Посмотреть не заполненный заказ"
    field :get_last_incompleted, :order do
      arg(:user_id, non_null(:integer))
      resolve(safely(&R.Transfering.get_last_incompleted/3))
    end

    @desc "Посмотреть заказов для граффика существует 3 варианта - все время (не указаны даты), годовой - указан год, месячный - указан месяц и год"
    field :list_orders_for_graph, list_of(:order_report) do
      arg(:user_id, :integer)
      arg(:year, :integer)
      arg(:mth, :integer)
      resolve(safely(&R.Transfering.list_orders_for_graph/3))
    end

    @desc "Создать заказ"
    field :create_order, :order do
      # field :relog_data, :json
      @desc "ссылка на карту"
      arg :track_link, :string

      arg :receiver_apartment, :string
      arg :receiver_float, :string
      arg :receiver_address, :string
      arg :receiver_area, :string
      arg :receiver_fio, :string
      arg :receiver_phone, :string
      arg :receiver_state, :string
      arg :receiver_town, :string
      arg :receiver_comment, :string

      @desc "цена"
      arg :price, :integer

      arg :sender_apartment, :string
      arg :sender_float, :string
      arg :sender_address, :string
      arg :sender_area, :string
      arg :sender_fio, :string
      arg :sender_phone, :string
      arg :sender_state, :string
      arg :sender_town, :string
      arg :sender_comment, :string
      @desc "ожидаемое время доставки"
      arg :delivery_awaited_datetime, :naive_datetime

      @desc "заполнен ли достаточно"
      arg :is_accepted, :boolean
      @desc "время заполнения"
      arg :acceptance_datetime, :naive_datetime

      @desc "доставлен ли"
      arg :is_delivered, :boolean
      @desc "подтверждена ли доставка"
      arg :delivery_proof_datetime, :naive_datetime
      @desc "время создания"
      arg :inserted_at, :naive_datetime
      @desc "время обновления"
      arg :updated_at, :naive_datetime

      @desc "отправлен организаций"
      arg :is_organization, :boolean
      @desc "может ли быть отправлен"
      arg :can_be_sent, :boolean
      @desc "валиден ли"
      arg :is_valid, :boolean

      @desc "почта на которую будут отсылаться письма"
      arg :sendage_email, :string
      @desc "номер накладной"
      arg :invoice_number, :string
      @desc "комментарий к платежу"
      arg :payment_comment, :string
      @desc "страховка"
      arg :insurance, :string

      @desc "сдача с"
      arg :odd_money, :string
      @desc "кто является плательщиком"
      arg :payer, :string

      @desc "дата удаления"
      arg :deleted_at, :naive_datetime

      arg(:order_status_id, :integer)
      arg(:order_tarif_id, :integer)
      arg(:payment_type_id, :integer)
      arg(:products, list_of(:i_product))
      arg(:sender_id, :integer)
      arg(:courier_id, :integer)
      arg(:updated_by, :integer)
      arg(:deleted_by, :integer)

      resolve(safely(&R.Transfering.create_order/3))
    end

    @desc "Обновить статус заказов"
    field :update_order, :order do
      arg :id, :integer
      @desc "ссылка на карту"
      arg :track_link, :string

      arg :receiver_apartment, :string
      arg :receiver_float, :string
      arg :receiver_address, :string
      arg :receiver_area, :string
      arg :receiver_fio, :string
      arg :receiver_phone, :string
      arg :receiver_state, :string
      arg :receiver_town, :string
      arg :receiver_comment, :string

      @desc "цена"
      arg :price, :integer

      arg :sender_apartment, :string
      arg :sender_float, :string
      arg :sender_address, :string
      arg :sender_area, :string
      arg :sender_fio, :string
      arg :sender_phone, :string
      arg :sender_state, :string
      arg :sender_town, :string
      arg :sender_comment, :string
      @desc "ожидаемое время доставки"
      arg :delivery_awaited_datetime, :naive_datetime

      @desc "заполнен ли достаточно"
      arg :is_accepted, :boolean
      @desc "время заполнения"
      arg :acceptance_datetime, :naive_datetime

      @desc "доставлен ли"
      arg :is_delivered, :boolean
      @desc "подтверждена ли доставка"
      arg :delivery_proof_datetime, :naive_datetime
      @desc "время создания"
      arg :inserted_at, :naive_datetime
      @desc "время обновления"
      arg :updated_at, :naive_datetime

      @desc "отправлен организаций"
      arg :is_organization, :boolean
      @desc "может ли быть отправлен"
      arg :can_be_sent, :boolean
      @desc "валиден ли"
      arg :is_valid, :boolean

      @desc "почта на которую будут отсылаться письма"
      arg :sendage_email, :string
      @desc "номер накладной"
      arg :invoice_number, :string
      @desc "комментарий к платежу"
      arg :payment_comment, :string
      @desc "страховка"
      arg :insurance, :string

      @desc "сдача с"
      arg :odd_money, :string
      @desc "кто является плательщиком"
      arg :payer, :string

      @desc "дата удаления"
      arg :deleted_at, :naive_datetime

      arg(:order_status_id, :integer)
      arg(:order_tarif_id, :integer)
      arg(:payment_type_id, :integer)
      arg(:products, list_of(:i_product))
      arg(:sender_id, :integer)
      arg(:courier_id, :integer)
      arg(:updated_by, :integer)
      arg(:deleted_by, :integer)

      resolve((&R.Transfering.update_order/3))
    end

    @desc "Удалить статус заказа"
    field :delete_order, :order do
      @desc "id заказа"
      arg(:order_id, non_null(:integer))
      @desc "удаление/блокировка"
      arg(:force, :boolean)
      resolve(safely(&R.Transfering.delete_order/3))
    end

    @desc "Отправляет курьера в релог"
    field :relog_send_courier, :string do
      arg(:id, non_null(:integer))
      arg(:password, non_null(:string))

      resolve(safely(&R.Transfering.send_courier/3))
    end

    @desc "Отправляет заказ в релог"
    field :relog_send_order, :string do
      arg(:id, non_null(:integer))

      resolve(safely(&R.Transfering.send_order/3))
    end

    @desc "Отправляет клиента в релог"
    field :relog_send_client, :string do
      arg(:id, non_null(:integer))

      resolve(safely(&R.Transfering.send_client/3))
    end

    @desc "Насильно сделать заказ валидным"
    field :force_validate, :order do
      arg(:id, non_null(:integer))

      resolve(safely(&R.Transfering.force_validate/3))
    end

    @desc "Насильно сделать заказ валидным"
    field :set_order_courier, :order do
      arg(:order_id, non_null(:integer))
      arg(:courier_id, non_null(:integer))

      resolve(safely(&R.Transfering.set_order_courier/3))
    end

    @desc "Получить заказ в виде, отправляемом в 1с"
    field :watch_prepared_order, :prepared_order do
      arg(:id, non_null(:integer))

      resolve(safely(&R.Transfering.watch_prepared_order/3))
    end

    @desc "Отправить заказ в 1с"
    field :send_prepared_order, :sendage_status do
      arg(:id, non_null(:integer))

      resolve(safely(&R.Transfering.send_prepared_order/3))
    end

    @desc "Отправить заказ в 1с"
    field :confirm_order_and_send, :sendage_status do
      arg(:id, non_null(:integer))

      resolve(safely(&R.Transfering.confirm_order_and_send/3))
    end

    @desc "Получить из 1с заказ"
    field :get_from_one_c, :string do
      arg(:id, non_null(:integer))

      resolve(safely(&R.Transfering.get_from_one_c/3))
    end

    @desc "Создать документацию"
    field :make_all_docs, :docs_links do
      arg(:dt_start, non_null(:string))
      arg(:dt_finish, non_null(:string))
      arg(:user_id, non_null(:integer))

      resolve(safely(&R.Transfering.make_all_docs/3))
    end

    @desc "Создать пользовательскую документацию"
    field :make_user_docs, :docs_links do
      arg(:user_id, non_null(:integer))
      arg(:dt_start, non_null(:string))
      arg(:dt_finish, non_null(:string))

      resolve(safely(&R.Transfering.make_user_docs/3))
    end

    @desc "Обновить стату заказа"
    field :update_status_at_order, :order do
      arg(:id, non_null(:integer))
      arg(:status_id, non_null(:integer))

      resolve(safely(&R.Transfering.update_status_at_order/3))
    end
  end
end
