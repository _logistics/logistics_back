defmodule LogisticServiceWeb.Schema.Transfering.Queries do
  @moduledoc """
  Profiles Queries Schema
  """
  use Helper.GqlSchemaSuite

  object :transfering_queries do
    @desc "Получить список статусов заказов"
    field :list_order_statuses, list_of(:order_status) do
      resolve(safely(&R.Transfering.list_order_statuses/3))
    end

    @desc "Получить статус заказов по id"
    field :get_order_statuses, list_of(:order_status) do
      @desc "id"
      arg(:id, non_null(:integer))
      resolve(safely(&R.Transfering.get_order_statuses/3))
    end

    @desc "Посмотреть заказов для граффика существует 3 варианта - все время (не указаны даты), годовой - указан год, месячный - указан месяц и год"
    field :list_orders_for_graph, list_of(:order_report) do
      arg(:user_id, :integer)
      arg(:year, :integer)
      arg(:mth, :integer)
      resolve(safely(&R.Transfering.list_orders_for_graph/3))
    end

    @desc "Получить список тарифов заказов"
    field :list_order_tarifs, list_of(:order_tarif) do
      resolve(safely(&R.Transfering.list_order_tarifs/3))
    end

    @desc "Получить список статусов заказов"
    field :get_order_tarif, list_of(:order_tarif) do
      arg(:id, non_null(:integer))
      resolve(safely(&R.Transfering.get_order_tarif/3))
    end

    @desc "Получить список тип оплаты"
    field :list_payment_types, list_of(:payment_type) do
      resolve(safely(&R.Transfering.list_payment_types/3))
    end

    @desc "Получить список статусов заказов"
    field :get_payment_types, list_of(:payment_type) do
      arg(:id, non_null(:integer))
      resolve(safely(&R.Transfering.get_payment_types/3))
    end

    @desc "Получить список заказов для отправителя"
    field :list_orders_sender, :paged_orders do
      arg(:sender_id, non_null(:integer))
      arg(:filter, :paged_users_filter)

      middleware(M.PageSizeCheck)
      resolve(safely(&R.Transfering.list_orders_sender/3))
    end

    @desc "Получить список заказов для курьера"
    field :list_orders_courier, :paged_orders do
      arg(:courier_id, non_null(:integer))
      arg(:filter, non_null(:paged_users_filter))

      middleware(M.PageSizeCheck)
      resolve(safely(&R.Transfering.list_orders_courier/3))
    end

    @desc "Получить список заказов"
    field :list_orders, :paged_orders do
      arg(:filter, :paged_users_filter)
      arg(:all, :boolean)

      middleware(M.PageSizeCheck)
      resolve(safely(&R.Transfering.list_orders/3))
    end

    @desc "Получить список заказов"
    field :list_orders_no_filters, list_of(:order) do
      arg(:user_id, :integer)
      arg(:start_dt, :string)
      arg(:end_dt, :string)

      resolve(safely(&R.Transfering.list_orders/3))
    end

    @desc "Получить заказ"
    field :get_order, list_of(:order) do
      arg(:order_id, non_null(:integer))
      arg(:all, :boolean)
      resolve(safely(&R.Transfering.get_order/3))
    end
  end
end
