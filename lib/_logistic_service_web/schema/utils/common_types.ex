defmodule LogisticServiceWeb.Schema.Utils.CommonTypes do
  @moduledoc """
    GraphQL commont types
  """
  import LogisticServiceWeb.Schema.Utils.Helper

  use Helper.GqlSchemaSuite

  @desc "статус выолнения задачи"
  object :status do
    field :done, :boolean
    field :id, :id
  end

  object :done do
    field :done, :boolean
  end

  input_object :ids do
    field :id, :id
  end


  @desc "общее правило пагинации"
  input_object :common_paged_filter do
    pagination_fields()
    @desc "сортировка"
    field :sort, :sort_order_inserted, default_value: :desc_inserted
  end

  object :data do
    field :var, :string
  end

  @desc "результат выполнения"
  object :success_result do
    field :message, non_null(:string)
  end

  @desc "описание пагинационных полей"
  object :pagination do
    field :limit, non_null(:integer)
    field :page, non_null(:integer)
  end

  @desc "сортировка (asc, desc)"
  enum :sort_order do
    value(:asc)
    value(:desc)
  end

  @desc "сортировка по дате и времени создания"
  enum :sort_order_inserted do
    value(:asc_inserted)
    value(:desc_inserted)
  end

  enum :on_off_s do
    value(:all)
    value(:onn)
    value(:off)
  end

  @desc "активный/удаленный пользователь"
  enum :is_active do
    value(:active)
    value(:deleted)
    value(:all)
  end

  @desc "страны пользователя (предполагался выбор)"
  enum :countries do
    value(:ru)
  end

  @desc "деление данных согласно периоду"
  enum :divided do
    value(true)
    value(false)
  end
end
