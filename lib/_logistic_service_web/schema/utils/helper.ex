defmodule LogisticServiceWeb.Schema.Utils.Helper do
  @moduledoc """
  Вспомогательные функции для Graphql API
  """

  import Helper.Utils, only: [get_config: 2]
  @page_size get_config(:general, :page_size)

  defmacro timestamp_fields do
    quote do
      field :inserted_at, :datetime
      field :updated_at, :datetime
    end
  end

  defmacro pagination_args do
    quote do
      field :page, :integer, default_value: 1
      field :size, :integer, default_value: unquote(@page_size)
    end
  end

  defmacro pagination_fields do
    quote do
      field :total_entries, :integer
      field :page_size, :integer
      field :total_pages, :integer
      field :page_number, :integer
    end
  end
end
