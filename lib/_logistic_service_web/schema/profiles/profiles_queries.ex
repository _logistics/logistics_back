defmodule LogisticServiceWeb.Schema.Profiles.Queries do
  @moduledoc """
  Profiles Queries Schema
  """
  use Helper.GqlSchemaSuite

  object :profiles_queries do
    @desc "Получить профиль"
    field :get_user_profile, :user_profile do
      @desc "id пользователя"
      arg(:user_id, non_null(:integer))

      resolve(safely(&R.Profiles.get_user_profile/3))
    end

    @desc "Список пользователей"
    field :get_org_profile, :org_profile do
      @desc "id пользователя"
      arg(:user_id, non_null(:integer))

      resolve(safely(&R.Profiles.get_org_profile/3))
    end
  end
end
