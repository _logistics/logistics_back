defmodule LogisticServiceWeb.Schema.Profiles.Types do
  @moduledoc """
    chat GraphQL mutations
  """
  use Helper.GqlSchemaSuite
  alias LogisticService.Accounts
  # import LogisticServiceWeb.Schema.Utils.Helper

  @desc "пользователь или организация, после определения пользователя подгрузит его активный профиль по параметру is_org"
  object :user_or_org do
    @desc "id пользователя"
    field(:user_id, non_null(:integer))
    @desc "имя пользователя"
    field(:first_name, :string)
    @desc "отчество пользователя"
    field(:middle_name, :string)
    @desc "фамилия пользователя"
    field(:last_name, :string)
    @desc "область"
    field(:state, :string)
    @desc "город"
    field(:town, :string)
    @desc "район"
    field(:area, :string)
    @desc "улица"
    field(:street, :string)
    @desc "номер дома"
    field(:house_number, :string)
    @desc "этаж"
    field(:floor, :string)
    @desc "номер квартиры"
    field(:apartment, :string)
    @desc "комментарии"
    field(:comments, :string)
    @desc "заблокирован"
    field(:locked_at, :naive_datetime)
    @desc "адрес вк"
    field(:vk, :string)
    @desc "телефон whatsapp"
    field(:whatsapp, :string)
    @desc "адрес telegram"
    field(:telegram, :string)
    @desc "адрес почты"
    field(:mail, :string)
    @desc "предпочтительный канал связи"
    field(:prefered_chanel, :string)
    @desc "контактное лицо (фио)"
    field(:contact_person_fio, :string)
    @desc "bik"
    field(:bik, :string)
    @desc "инн"
    field(:inn, :string)
    @desc "название организации"
    field(:org_name, :string)
    @desc "рс"
    field(:rs, :string)

    @desc "пользователь по связи"
    field :user, :user, resolve: dataloader(Accounts, :user, args: %{scope: :user})
  end

  object :user_profile do
    @desc "имя пользователя"
    field(:first_name, :string)
    @desc "отчество пользователя"
    field(:middle_name, :string)
    @desc "фамилия пользователя"
    field(:last_name, :string)
    @desc "область"
    field(:state, :string)
    @desc "город"
    field(:town, :string)
    @desc "район"
    field(:area, :string)
    @desc "улица"
    field(:street, :string)
    @desc "номер дома"
    field(:house_number, :string)
    @desc "этаж"
    field(:floor, :string)
    @desc "номер квартиры"
    field(:apartment, :string)
    @desc "комментарии"
    field(:comments, :string)
    @desc "заблокирован"
    field(:locked_at, :naive_datetime)
    @desc "адрес вк"
    field(:vk, :string)
    @desc "телефон whatsapp"
    field(:whatsapp, :string)
    @desc "адрес telegram"
    field(:telegram, :string)
    @desc "адрес почты"
    field(:mail, :string)
    @desc "предпочтительный канал связи"
    field(:prefered_chanel, :string)

    field :user, :user, resolve: dataloader(Accounts, :user, args: %{scope: :user})
  end

  object :org_profile do
    @desc "bik"
    field(:bik, :string)
    @desc "контактное лицо (фио)"
    field(:contact_person_fio, :string)
    @desc "инн"
    field(:inn, :string)
    @desc "адрес почты"
    field(:mail, :string)
    @desc "комментарии"
    field(:comments, :string)
    @desc "название организации"
    field(:org_name, :string)
    @desc "область"
    field(:state, :string)
    @desc "рс"
    field(:rs, :string)
    @desc "город"
    field(:town, :string)
    @desc "район"
    field(:area, :string)
    @desc "улица"
    field(:street, :string)
    @desc "номер дома"
    field(:house_number, :string)
    @desc "этаж"
    field(:floor, :string)
    @desc "квартира"
    field(:apartment, :string)
    @desc "адрес телеграм"
    field(:telegram, :string)
    @desc "адрес вк"
    field(:vk, :string)
    @desc "телефон whatsapp"
    field(:whatsapp, :string)
    @desc "предпочтительный канал связи"
    field(:prefered_chanel, :string)
    @desc "подтверждена ли организация"
    field(:is_confirmed, :boolean)

    field :user, :user, resolve: dataloader(Accounts, :user, args: %{scope: :user})
  end

  object :deleted_user_profile do
    field :user_profile, :user_profile
    field :deleted, :boolean
    field :deletion_time, :naive_datetime
  end

  object :deleted_org_profile do
    field :org_profile, :org_profile
    field :deleted, :boolean
    field :deletion_time, :naive_datetime
  end
end
