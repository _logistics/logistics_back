defmodule LogisticServiceWeb.Schema.Profiles.Mutations do
  @moduledoc """
  Accounts Mutations Schema
  """
  use Helper.GqlSchemaSuite

  object :profiles_mutations do
    @desc "Создать пользовательский профиль пользователя"
    field :create_user_profile, :user_profile do
      @desc "id пользователя"
      arg(:user_id, non_null(:integer))
      @desc "имя пользователя"
      arg(:first_name, :string)
      @desc "отчество пользователя"
      arg(:middle_name, :string)
      @desc "фамилия пользователя"
      arg(:last_name, :string)
      @desc "область"
      arg(:state, :string)
      @desc "город"
      arg(:town, :string)
      @desc "район"
      arg(:area, :string)
      @desc "улица"
      arg(:street, :string)
      @desc "номер дома"
      arg(:house_number, :string)
      @desc "этаж"
      arg(:floor, :string)
      @desc "номер квартиры"
      arg(:apartment, :string)
      @desc "комментарии"
      arg(:comments, :string)
      @desc "заблокирован"
      arg(:locked_at, :naive_datetime)
      @desc "адрес вк"
      arg(:vk, :string)
      @desc "телефон whatsapp"
      arg(:whatsapp, :string)
      @desc "адрес telegram"
      arg(:telegram, :string)
      @desc "адрес почты"
      arg(:mail, :string)
      @desc "предпочтительный канал связи"
      arg(:prefered_chanel, :string)

      resolve(safely(&R.Profiles.create_user_profile/3))
    end

    @desc "Обновить пользовательский профиль пользователя"
    field :update_user_profile, :user_profile do
      @desc "id пользователя"
      arg(:user_id, non_null(:integer))
      @desc "имя пользователя"
      arg(:first_name, :string)
      @desc "отчество пользователя"
      arg(:middle_name, :string)
      @desc "фамилия пользователя"
      arg(:last_name, :string)
      @desc "область"
      arg(:state, :string)
      @desc "город"
      arg(:town, :string)
      @desc "район"
      arg(:area, :string)
      @desc "улица"
      arg(:street, :string)
      @desc "номер дома"
      arg(:house_number, :string)
      @desc "этаж"
      arg(:floor, :string)
      @desc "номер квартиры"
      arg(:apartment, :string)
      @desc "комментарии"
      arg(:comments, :string)
      @desc "заблокирован"
      arg(:locked_at, :naive_datetime)
      @desc "адрес вк"
      arg(:vk, :string)
      @desc "телефон whatsapp"
      arg(:whatsapp, :string)
      @desc "адрес telegram"
      arg(:telegram, :string)
      @desc "адрес почты"
      arg(:mail, :string)
      @desc "предпочтительный канал связи"
      arg(:prefered_chanel, :string)

      resolve(safely(&R.Profiles.update_user_profile/3))
    end

    @desc "Удалить пользовательский профиль пользователя"
    field :delete_user_profile, :deleted_user_profile do
      @desc "id пользователя"
      arg(:user_id, non_null(:integer))

      resolve(safely(&R.Profiles.delete_user_profile/3))
    end

    @desc "Создать организационный профиль пользователя"
    field :create_org_profile, :org_profile do
      @desc "id пользователя"
      arg(:user_id, non_null(:integer))
      @desc "контактное лицо (фио)"
      arg(:contact_person_fio, :string)
      @desc "bik"
      arg(:bik, :string)
      @desc "инн"
      arg(:inn, :string)
      @desc "адрес почты"
      arg(:mail, :string)
      @desc "комментарии"
      arg(:comments, :string)
      @desc "область"
      arg(:state, :string)
      @desc "город"
      arg(:town, :string)
      @desc "район"
      arg(:area, :string)
      @desc "улица"
      arg(:street, :string)
      @desc "номер дома"
      arg(:house_number, :string)
      @desc "этаж"
      arg(:floor, :string)
      @desc "квартира"
      arg(:apartment, :string)
      @desc "адрес телеграм"
      arg(:telegram, :string)
      @desc "адрес вк"
      arg(:vk, :string)
      @desc "телефон whatsapp"
      arg(:whatsapp, :string)
      @desc "предпочтительный канал связи"
      arg(:prefered_chanel, :string)
      @desc "название организации"
      arg(:org_name, :string)
      @desc "рс"
      arg(:rs, :string)
      @desc "подтверждена ли организация"
      arg(:is_confirmed, :boolean)

      resolve(safely(&R.Profiles.create_org_profile/3))
    end

    @desc "Обновить пользовательский профиль пользователя"
    field :update_org_profile, :org_profile do
      @desc "id пользователя"
      arg(:user_id, non_null(:integer))
      @desc "bik"
      arg(:bik, :string)
      @desc "контактное лицо (фио)"
      arg(:contact_person_fio, :string)
      @desc "инн"
      arg(:inn, :string)
      @desc "адрес почты"
      arg(:mail, :string)
      @desc "комментарии"
      arg(:comments, :string)
      @desc "название организации"
      arg(:org_name, :string)
      @desc "область"
      arg(:state, :string)
      @desc "рс"
      arg(:rs, :string)
      @desc "город"
      arg(:town, :string)
      @desc "район"
      arg(:area, :string)
      @desc "улица"
      arg(:street, :string)
      @desc "номер дома"
      arg(:house_number, :string)
      @desc "этаж"
      arg(:floor, :string)
      @desc "квартира"
      arg(:apartment, :string)
      @desc "адрес телеграм"
      arg(:telegram, :string)
      @desc "адрес вк"
      arg(:vk, :string)
      @desc "телефон whatsapp"
      arg(:whatsapp, :string)
      @desc "предпочтительный канал связи"
      arg(:prefered_chanel, :string)
      @desc "подтверждена ли организация"
      arg(:is_confirmed, :boolean)

      resolve(safely(&R.Profiles.update_org_profile/3))
    end

    @desc "Удалить пользовательский профиль пользователя"
    field :delete_org_profile, :deleted_org_profile do
      @desc "id пользователя"
      arg(:user_id, non_null(:integer))

      resolve(safely(&R.Profiles.delete_org_profile/3))
    end
  end
end
