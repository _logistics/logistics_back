defmodule LogisticServiceWeb.Schema.Accounts.Mutations do
  @moduledoc """
  Accounts Mutations Schema
  """
  use Helper.GqlSchemaSuite

  object :accounts_mutations do
    @desc "Отправить пользователю сообщение с регистрацией"
    field :signup, :string do
      @desc "телефон пользователя без цифры страны"
      arg(:phone, non_null(:string))
      resolve(safely(&R.Accounts.signup/3))
    end

    @desc "Создать пользователя"
    field :submit_signup, :session_with_token do
      @desc "телефон пользователя без цифры страны"
      arg(:phone, non_null(:string))
      @desc "id роли"
      arg(:roles, list_of(:integer))
      @desc "код пришедший по смс"
      arg(:code, non_null(:integer))
      @desc "является ли пользователь организацией"
      arg(:is_organization, :boolean)
      @desc "пароль"
      arg(:password, non_null(:string))
      @desc "подтвержние пароля"
      arg(:password_confirmation, non_null(:string))
      resolve(safely(&R.Accounts.submit_signup/3))
    end

    @desc "Создать пользователя"
    field :admin_signup, :session_with_token do
      @desc "телефон пользователя без цифры страны"
      arg(:phone, non_null(:string))
      @desc "id роли"
      arg(:roles, list_of(:integer))
      @desc "является ли пользователь организацией"
      arg(:is_organization, :boolean)
      @desc "пароль"
      arg(:password, non_null(:string))
      @desc "подтвержние пароля"
      arg(:password_confirmation, non_null(:string))
      resolve(safely(&R.Accounts.admin_signup/3))
    end

    @desc "Запрос на сообщение о восстановлении пароля"
    field :reset_password, :string do
      @desc "телефон пользователя без цифры страны"
      arg(:phone, non_null(:string))
      resolve(safely(&R.Accounts.reset_password/3))
    end

    @desc "Обновить пароль"
    field :update_password, :session_with_token do
      @desc "телефон пользователя без цифры страны"
      arg(:phone, non_null(:string))
      @desc "код пришедший по смс"
      arg(:code, non_null(:integer))
      @desc "пароль"
      arg(:password, non_null(:string))
      @desc "подтвержние пароля"
      arg(:password_confirmation, non_null(:string))
      resolve(safely(&R.Accounts.update_password/3))
    end

    @desc "Войти в сесию"
    field :signin, :session_with_token do
      @desc "телефон пользователя без цифры страны"
      arg(:phone, non_null(:string))
      @desc "пароль"
      arg(:password, non_null(:string))
      resolve(safely(&R.Accounts.signin/3))
    end

    @desc "Получить количество новых пользователей"
    field :new_users, :statistic_message do
      @desc "начало промежутка"
      arg(:dt_start, non_null(:string))
      @desc "конец промежутка"
      arg(:dt_finish, non_null(:string))
      @desc "нужно ли дробление (если дата < полугода, помесячно, если дата > года, по годам и т.д.)"
      arg(:divided, non_null(:divided))
      resolve(safely(&R.Accounts.new_users/3))
    end

    @desc "Получить общее количество пользователей"
    field :count_users, :statistic_message do
      @desc "нужно ли дробление (если дата < полугода, помесячно, если дата > года, по годам и т.д.)"
      arg(:divided, non_null(:divided))
      resolve(safely(&R.Accounts.count_users/3))
    end

    @desc "Получить сумму заказов"
    field :count_order_summ, :statistic_message do
      @desc "начало промежутка"
      arg(:dt_start, non_null(:string))
      @desc "конец промежутка"
      arg(:dt_finish, non_null(:string))
      @desc "id пользователя"
      arg(:user_id, :integer)
      @desc "нужно ли дробление (если дата < полугода, помесячно, если дата > года, по годам и т.д.)"
      arg(:divided, non_null(:divided))
      resolve(safely(&R.Accounts.count_order_summ/3))
    end

    @desc "Получить количество новых заказов"
    field :count_orders, :statistic_message do
      @desc "начало промежутка"
      arg(:dt_start, non_null(:string))
      @desc "конец промежутка"
      arg(:dt_finish, non_null(:string))
      @desc "id пользователя"
      arg(:user_id, :integer)
      @desc "нужно ли дробление (если дата < полугода, помесячно, если дата > года, по годам и т.д.)"
      arg(:divided, non_null(:divided))
      resolve(safely(&R.Accounts.count_orders/3))
    end

    @desc "Получить доставленные заказы"
    field :count_transfered_orders, :statistic_message do
      @desc "начало промежутка"
      arg(:dt_start, non_null(:string))
      @desc "конец промежутка"
      arg(:dt_finish, non_null(:string))
      @desc "id пользователя"
      arg(:user_id, :integer)
      @desc "нужно ли дробление (если дата < полугода, помесячно, если дата > года, по годам и т.д.)"
      arg(:divided, non_null(:divided))
      resolve(safely(&R.Accounts.count_transfered_orders/3))
    end

    @desc "Обновить роли пользователя"
    field :upsert_user_roles, :user do
      @desc "id пользователя"
      arg(:user_id, non_null(:integer))
      @desc "id роли"
      arg(:roles, non_null(list_of(:integer)))

      resolve(safely(&R.Accounts.upsert_user_roles/3))
    end

    @desc "Мягкое удаление пользователя"
    field :delete_user, :user do
      @desc "id пользователя"
      arg(:id, non_null(:integer))

      resolve(safely(&R.Accounts.soft_delete_user/3))
    end

    @desc "Возвращает уникальных пользователей"
    field :get_unique_users, list_of(:user) do
      @desc "список id"
      arg(:user_ids, non_null(list_of(:integer)))

      resolve(safely(&R.Accounts.get_unique_users/3))
    end

    @desc "Восстановление пользователя"
    field :restore_user, :user do
      @desc "id пользователя"
      arg(:id, non_null(:integer))

      resolve(safely(&R.Accounts.restore_user/3))
    end

    @desc "Обновление пользователя"
    field :update_user, :user do
      @desc "id пользователя"
      arg(:id, non_null(:integer))
      @desc "телефон пользователя без цифры страны"
      arg(:phone, :string)
      @desc "записи о пользователе"
      arg(:notes, :string)

      @desc "ссылка на конракт"
      arg(:contract_link, :string)
      @desc "возможность создавать заказы"
      arg(:opportunity_to_create, :boolean)

      @desc "id ролей"
      arg(:roles, list_of(:integer))
      @desc "является ли организацией"
      arg(:is_organization, :boolean)

      @desc "старый пароль"
      arg(:old_password, :string)
      @desc "новый пароль"
      arg(:password, :string)
      @desc "подтверждение пароля"
      arg(:password_confirmation, :string)

      resolve(safely(&R.Accounts.update_user/3))
    end
  end
end
