defmodule LogisticServiceWeb.Schema.Accounts.Types do
  @moduledoc """
    chat GraphQL mutations
  """
  use Helper.GqlSchemaSuite
  import LogisticServiceWeb.Schema.Utils.Helper
  alias LogisticService.Accounts

  object :user do
    meta(:cache, max_age: 30)
    field :id, :id
    field :phone, non_null(:string)
    field :comments, :string
    field :logged_at, :datetime
    field :inserted_at, :string
    field :is_organization, :boolean
    field :deleted_at, :string
    field :contract_link, :string
    field :opportunity_to_create, :boolean
    field :roles, list_of(:roles), resolve: dataloader(Accounts, :roles, args: %{scope: :user})

    field :user_profile, :user_profile,
      resolve: dataloader(Accounts, :user_profile, args: %{scope: :user_profile})

    field :org_profile, :org_profile,
      resolve: dataloader(Accounts, :org_profile, args: %{scope: :org_profile})

    field :profile, :user_or_org
  end

  object :paged_users do
    field :entries, list_of(:user)
    pagination_fields()
  end

  input_object :paged_users_filter do
    pagination_args()
    field :sort, :sort_order_inserted, default_value: :desc_inserted
    field :start_dt, :string
    field :end_dt, :string
  end

  object :session do
    field :is_valid, non_null(:boolean)
    field :user, non_null(:user)
  end

  object :session_with_token do
    field :token, non_null(:string)
    field :user, non_null(:user)
  end

  object :roles do
    field :id, :integer
    field :slug, non_null(:string)
    field :user, list_of(:user), resolve: dataloader(Accounts, :users, args: %{scope: :roles})
  end

  object :customization do
    field :theme, :string
  end

  object :statistic_message do
    field :count, list_of(:count)
    field :message, :string
  end

  object :count do
    field :count, :integer
    field :dt, :integer
    field :type, :string
  end
end
