defmodule LogisticServiceWeb.Schema.Accounts.Queries do
  @moduledoc """
  Accounts Queries Schema
  """
  use Helper.GqlSchemaSuite

  object :accounts_queries do
    @desc "Список пользователей"
    field :users, :paged_users do
      @desc "список фильтров"
      arg(:filter, non_null(:paged_users_filter))
      @desc "активные (дефолт активные - т.е. без удаленных)"
      arg(:is_active, :is_active, default_value: :active)

      middleware(M.PageSizeCheck)
      resolve(safely(&R.Accounts.users/3))
    end

    @desc "Список курьеров"
    field :couriers, :paged_users do
      @desc "список фильтров"
      arg(:filter, non_null(:paged_users_filter))

      middleware(M.PageSizeCheck)
      resolve(safely(&R.Accounts.couriers/3))
    end

    @desc "Получить количество новых пользователей"
    field :new_users, :statistic_message do
      @desc "начало промежутка"
      arg(:dt_start, non_null(:string))
      @desc "конец промежутка"
      arg(:dt_finish, non_null(:string))
      @desc "нужно ли дробление (если дата < полугода, помесячно, если дата > года, по годам и т.д.)"
      arg(:divided, non_null(:divided))
      resolve(safely(&R.Accounts.new_users/3))
    end

    @desc "Получить общее количество пользователей"
    field :count_users, :statistic_message do
      @desc "нужно ли дробление (если дата < полугода, помесячно, если дата > года, по годам и т.д.)"
      arg(:divided, non_null(:divided))
      resolve(safely(&R.Accounts.count_users/3))
    end

    @desc "Получить сумму заказов"
    field :count_order_summ, :statistic_message do
      @desc "начало промежутка"
      arg(:dt_start, non_null(:string))
      @desc "конец промежутка"
      arg(:dt_finish, non_null(:string))
      @desc "id пользователя"
      arg(:user_id, :integer)
      @desc "нужно ли дробление (если дата < полугода, помесячно, если дата > года, по годам и т.д.)"
      arg(:divided, non_null(:divided))
      resolve(safely(&R.Accounts.count_order_summ/3))
    end

    @desc "Получить количество новых заказов"
    field :count_orders, :statistic_message do
      @desc "начало промежутка"
      arg(:dt_start, non_null(:string))
      @desc "конец промежутка"
      arg(:dt_finish, non_null(:string))
      @desc "id пользователя"
      arg(:user_id, :integer)
      @desc "нужно ли дробление (если дата < полугода, помесячно, если дата > года, по годам и т.д.)"
      arg(:divided, non_null(:divided))
      resolve(safely(&R.Accounts.count_orders/3))
    end

    @desc "Получить доставленные заказы"
    field :count_transfered_orders, :statistic_message do
      @desc "начало промежутка"
      arg(:dt_start, non_null(:string))
      @desc "конец промежутка"
      arg(:dt_finish, non_null(:string))
      @desc "id пользователя"
      arg(:user_id, :integer)
      @desc "нужно ли дробление (если дата < полугода, помесячно, если дата > года, по годам и т.д.)"
      arg(:divided, non_null(:divided))
      resolve(safely(&R.Accounts.count_transfered_orders/3))
    end

    @desc "Список пользователей"
    field :user, :user do
      arg(:id, :integer)

      resolve(safely(&R.Accounts.get_user/3))
    end

    @desc "Получить своего пользователя"
    field :me, :user do
      resolve(safely(&R.Accounts.me/3))
    end
  end
end
