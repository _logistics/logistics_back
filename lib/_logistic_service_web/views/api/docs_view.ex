defmodule LogisticServiceWeb.DocsView do
  use LogisticServiceWeb, :view

  def render("index.json", %{docs: docs}) do
    %{
      docs: inspect(docs)
    }
  end

  def render("readed.json", %{res: res}) do
    %{
      res: inspect(res)
    }
  end
end
