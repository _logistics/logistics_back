defmodule LogisticServiceWeb.UserView do
  alias LogisticServiceWeb.UserView
  use LogisticServiceWeb, :view

  def render("index.json", %{users: users}),
    do: %{data: render_many(users, UserView, "user.json")}

  def render("show.json", %{user: user}), do: %{data: render_one(user, UserView, "user.json")}

  def render("updated_user.json", %{upd: %{user: user, upd_time: upd_time}}),
    do: %{data: render_one(user, UserView, "user.json"), update_time: upd_time}

  def render("deleted_user.json", %{del: %{user: user, del_time: del_time}}),
    do: %{data: render_one(user, UserView, "user.json"), deleted_time: del_time}

  def render("recovered_user.json", %{rec: %{user: user, recovery_time: recovery_time}}),
    do: %{data: render_one(user, UserView, "user.json"), recovery_time: recovery_time}

  def render("usr_permission.json", %{user_permissions: user_permissions}) do
    %{data: render_one(user_permissions, UserView, "user_permission.json")}
  end

  def render("get_user_session.json", %{user_session: user_session}) do
    %{data: render_one(user_session.user, UserView, "user.json"), token: user_session.token}
  end

  def render("user.json", %{user: user}) do
    %{
      id: user.id,
      phone: user.phone,
      email: user.email,
      notes: user.notes,
      logged_at: user.logged_at,
      password_hash: user.password_hash,
      inserted_at: user.inserted_at,
      updated_at: user.updated_at,
      deleted: !!user.deleted_at
    }
  end

  def render("user_permission.json", %{user: user}) do
    %{
      id: user.id,
      phone: user.phone,
      email: user.email,
      notes: user.notes,
      logged_at: user.logged_at,
      password_hash: user.password_hash,
      inserted_at: user.inserted_at,
      updated_at: user.updated_at,
      deleted: !!user.deleted_at,
      permissions: render_many(user.permissions, PermissionsView, "permissions.json")
    }
  end

  def render("error_user.json", %{error: {:error, e}}) do
    [{field, {error, _any}}] = e.errors

    %{
      error: %{
        field: field,
        error: error
      }
    }
  end
end
