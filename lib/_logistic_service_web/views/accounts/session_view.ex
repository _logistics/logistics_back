defmodule LogisticServiceWeb.SessionView do
  use LogisticServiceWeb, :view

  def render("index.json", %{message: message}) do
    %{
      message: message
    }
  end

  def render("site_session.json", %{site_session: %{site: site, site_token: site_token}}) do
    %{
      data: %{
        site: render_one(site, LogisticServiceWeb.SiteView, "site.json")
      },
      site_token: site_token
    }
  end

  def render("site_session_error.json", %{message: msg}) do
    %{
      error: msg
    }
  end

  def render("params_error.json", %{message: msg}) do
    %{
      error: %{
        details: msg
      }
    }
  end
end
