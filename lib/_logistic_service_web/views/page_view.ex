defmodule LogisticServiceWeb.PageView do
  use LogisticServiceWeb, :view

  def render("index.json", %{message: message}) do
    %{
      message: message
    }
  end
end
