defmodule LogisticServiceWeb.Middleware.PageSizeCheck do
  @moduledoc """
  Проверка максимальной длинны данных
  """
  @behaviour Absinthe.Middleware

  import Helper.Utils, only: [handle_absinthe_error: 3, get_config: 2]
  import Helper.ErrorCode

  @max_page_size get_config(:general, :page_size)
  @inner_page_size get_config(:general, :inner_page_size)

  def call(%{errors: errors} = resolution, _info) when length(errors) > 0, do: resolution

  def call(%{context: %{current_user: %{customization: c}, arguments: a}} = resolution, _info)
      when not is_nil(c) do
    size = c["max_display"] |> String.to_integer() |> min(@max_page_size)

    case Map.has_key?(a, :filter) do
      true ->
        filter = a.filter |> Map.merge(%{size: size})
        arguments = a |> Map.merge(%{filter: filter})

        %{resolution | arguments: sort_desc_by_default(arguments)}

      false ->
        arguments = a |> Map.merge(%{filter: %{page: 1, size: size, first: size}})

        %{resolution | arguments: arguments}
    end
  end

  def call(resolution, _info) do
    case valid_size(resolution.arguments) do
      {:error, msg} ->
        resolution |> handle_absinthe_error(msg, ecode(:pagination))

      args ->
        %{resolution | arguments: sort_desc_by_default(args)}
    end
  end

  defp sort_desc_by_default(%{filter: filter} = arguments) do
    filter =
      if Map.has_key?(filter, :sort),
        do: filter,
        else: filter |> Map.merge(%{sort: :desc_inserted})

    arguments
    |> Map.merge(%{filter: filter})
  end

  defp valid_size(%{filter: %{first: size}} = args), do: size_check(size, args)
  defp valid_size(%{filter: %{size: size}} = args), do: size_check(size, args)

  defp valid_size(arg),
    do: arg |> Map.merge(%{filter: %{page: 1, size: @max_page_size, first: @inner_page_size}})

  defp size_check(size, arg) do
    case size in 1..@max_page_size do
      true ->
        arg

      false ->
        {:error,
         "Ошибка длинны запроса: длинна запроса должна быть между 0 и #{@max_page_size}, запрошенная: #{
           size
         }"}
    end
  end
end
