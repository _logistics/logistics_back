defmodule LogisticServiceWeb.Middleware.Error do
  @moduledoc """
  This is error middleware
  """
  @behaviour Absinthe.Middleware

  def call(%{errors: [List = errors]} = resolution, _info),
    do: %{resolution | value: [], errors: [%{message: errors}]}

  def call(resolution, _info), do: resolution
end
