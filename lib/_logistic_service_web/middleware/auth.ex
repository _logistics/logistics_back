defmodule LogisticServiceWeb.Middleware.Auth do
  @moduledoc """
  This is auth middleware
  """
  @behaviour Absinthe.Middleware

  import Helper.Utils, only: [handle_absinthe_error: 3]

  def call(%{context: %{cur_user: _}} = resolution, _info), do: resolution

  def call(resolution, _info),
    do: resolution |> handle_absinthe_error("Войдите в учетную запись.", 401)
end
