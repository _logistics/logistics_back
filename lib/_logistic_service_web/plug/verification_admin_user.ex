defmodule LogisticServiceWeb.Plugs.Verification.AdminUser do
  @moduledoc """
  Данный плагин проверяет, есть ли сайт в контексте
  """
  @behaviour Plug
  alias LogisticService.Accounts

  def init(opts), do: opts

  def call(conn, _) do
    with user <- conn.assigns[:context][:current_user],
         true <- !!user,
         true <- Accounts.is_admin?(user.id) do
      conn
    else
      _any -> LogisticServiceWeb.FallbackController.admin_only(conn)
    end
  end
end
