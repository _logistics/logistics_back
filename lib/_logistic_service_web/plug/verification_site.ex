defmodule LogisticServiceWeb.Plugs.Verification.Site do
  @moduledoc """
  Данный плагин проверяет, есть ли сайт в контексте
  """
  @behaviour Plug

  def init(opts), do: opts

  def call(conn, _) do
    if !!conn.assigns[:context][:current_site] do
      conn
    else
      LogisticServiceWeb.FallbackController.no_context(conn)
    end
  end
end
