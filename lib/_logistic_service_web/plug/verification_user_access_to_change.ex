defmodule LogisticServiceWeb.Plugs.Verification.UserAccessToChange do
  @moduledoc """
  Данный плагин проверяет, есть ли сайт в контексте
  """
  @behaviour Plug

  alias LogisticService.Accounts.Models.User

  def init(opts), do: opts

  def call(conn, _) do
    with %User{} <- conn.assigns[:context][:current_user] do
      conn
    else
      _any -> LogisticServiceWeb.FallbackController.admin_only(conn)
    end
  end
end
