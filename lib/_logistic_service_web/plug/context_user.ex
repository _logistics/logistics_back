defmodule LogisticServiceWeb.Plugs.Context.User do
  @moduledoc """
  Парсит хэдеры для получения необходимой информации, в частности:

  Конечный пользователь, делющий этот запрос
  """

  @behaviour Plug

  import Plug.Conn

  alias Helper.GuardianUser
  alias LogisticService.Accounts

  def init(opts), do: opts

  def call(conn, _defaults) do
    context =
      conn
      |> build_user_context()
      |> update_absinthe_context_if_exist(conn)

    conn
    |> Plug.Conn.assign(:context, context)
    |> Absinthe.Plug.put_options(context: context)
  end

  def build_user_context(conn) do
    with ["Bearer " <> token] <- get_req_header(conn, "authorization"),
         {:ok, curr_user} <- authorize_user(token) do
      %{current_user: curr_user}
    else
      _ -> %{}
    end
  end

  defp authorize_user(token) do
    with {:ok, %{phone: phone}, _claims} <- GuardianUser.jwt_decode(token) do
      case Accounts.get_user_by_phone(phone) do
        {:ok, user} ->
          {:ok, user}

        {:error, _} ->
          {:error, "Для обработки данного запроса необходимо авторизовать пользователя"}
      end
    end
  end

  defp update_absinthe_context_if_exist(new_conn, conn) do
    case conn.private[:absinthe][:context] do
      nil ->
        new_conn

      context ->
        context
        |> Map.merge(new_conn)
    end
  end
end
