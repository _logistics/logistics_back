defmodule LogisticServiceWeb.Plugs.Context.Site do
  @moduledoc """
  Парсит хэдеры для получения необходимой информации, в частности:

  Сайт, который делает данный запрос
  """

  @behaviour Plug

  @token "sjdfgeuw3r783r2gqfdebsiurfhew9875y98435rh98hefnduh"

  import Plug.Conn
  def init(opts), do: opts

  def call(conn, _defaults) do
    context =
      conn
      |> build_site_context()
      |> update_absinthe_context_if_exist(conn)

    conn
    |> Plug.Conn.assign(:context, context)
    |> Absinthe.Plug.put_options(context: context)
  end

  def build_site_context(conn) do
    with ["Bearer " <> token] <- get_req_header(conn, "sitetoken"),
         {:ok, cur_site} <- authorize_site(token) do
      %{current_site: cur_site}
    else
      _ -> %{}
    end
  end

  defp authorize_site(""),
    do: {:error, "Для обработки данного запроса необходимо авторизовать сервис"}

  defp authorize_site(token) do
    case token == @token do
      true -> {:ok, %{name: "frontend applcation"}}
      false -> {:error, "Неверный токен"}
    end
  end

  defp update_absinthe_context_if_exist(new_conn, conn) do
    case conn.private[:absinthe][:context] do
      nil ->
        new_conn

      context ->
        context
        |> Map.merge(new_conn)
    end
  end
end
