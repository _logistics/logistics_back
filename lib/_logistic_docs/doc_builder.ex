defmodule DocBuilder do
  @moduledoc """
  Documentation for `DocBuilder`.
  """
  require Elixlsx
  alias Elixlsx.Sheet
  alias Elixlsx.Workbook

  @data [
    %{
      Накладная: 1,
      Дата: 2,
      Отправитель: 3,
      "Адрес отправителя": 1,
      Получатель: 2,
      "Адрес получателя": 3,
      Вес: 1,
      Объем: 2,
      "Нал. платеж": 3,
      Страховка: 3,
      "Причина отказа": 2,
      Стоимость: 100
    }
  ]
  @border_full [
    left: [style: :thin, color: "#000000"],
    right: [style: :thin, color: "#000000"],
    top: [style: :thin, color: "#000000"],
    bottom: [style: :thin, color: "#000000"]
  ]

  def create_table(filename \\ "test.xlsx", data \\ @data) do
    t_head = [
      "Накладная",
      "Дата",
      "Отправитель",
      "Адрес отправителя",
      "Получатель",
      "Адрес получателя",
      "Вес",
      "Объем",
      "Нал. платеж",
      "Страховка",
      "Причина отказа",
      "Стоимость"
    ]

    t_data = vals(data, t_head)

    create(filename, t_head, t_data)
  end

  def create(filename, t_head, data) do
    dlen = length(data)

    sheet =
      Sheet.with_name("Test")
      |> Sheet.set_row_height(1, 20)
      |> make_header("АТК ТЕМП")
      |> set_t_head(t_head)
      |> set_body(data)
      |> set_width()
      |> set_footer(dlen)

    create_pdf(filename, sheet)
  end

  def set_width(sheet) do
    sheet
    |> Sheet.set_col_width("A", 14.0)
    |> Sheet.set_col_width("C", 18.0)
    |> Sheet.set_col_width("D", 22.0)
    |> Sheet.set_col_width("E", 14.0)
    |> Sheet.set_col_width("F", 22.0)
    |> Sheet.set_col_width("G", 8.0)
    |> Sheet.set_col_width("H", 10.0)
    |> Sheet.set_col_width("I", 16.0)
    |> Sheet.set_col_width("J", 12.0)
    |> Sheet.set_col_width("K", 18.0)
    |> Sheet.set_col_width("L", 16.0)
  end

  @spec create_pdf(binary, Elixlsx.Sheet.t()) :: :ok
  def create_pdf(filename, sheet) do
    Elixlsx.write_to(%Workbook{sheets: [sheet]}, "./priv/static/docs/" <> filename)

    System.cmd("sh", [
      "-c",
      "libreoffice --headless --convert-to pdf:calc_pdf_Export --outdir ./priv/static/docs/ ./priv/static/docs/#{
        filename
      }"
    ])

    # File.rm!("./priv/static/docs/#{filename}")
    :ok
  end

  def replace_format(:pdf, name), do: String.replace(name, ".xlsx", ".pdf")

  def set_footer(sheet, len) do
    params = [
      {:bold, true},
      {:border, @border_full},
      {:align_horizontal, :center},
      {:num_format, "0.00 ₽"}
    ]

    sheet
    |> Sheet.set_cell("K#{len + 3}", "Общая сумма: ", params)
    |> Sheet.set_cell("L#{len + 3}", {:formula, "SUM(L3:L#{len + 2})"}, params)
  end

  def set_body(sheet, data) do
    data
    |> Enum.with_index()
    |> Enum.reduce(sheet, fn {table_row, row_idx}, sheet ->
      table_row
      |> Enum.with_index(1)
      |> Enum.reduce(sheet, fn {table_col_val, col_idx}, sheet ->
        element = <<64 + col_idx>> <> "#{3 + row_idx}"
        table_col_val = if is_nil(table_col_val), do: :empty, else: table_col_val

        params =
          if String.match?(element, ~r/L/),
            do: [{:border, @border_full}, {:num_format, "0.00 ₽"}],
            else: [{:border, @border_full}]

        Sheet.set_cell(sheet, element, table_col_val, params)
      end)
    end)
  end

  def set_t_head(sheet, head_keys) do
    head_keys
    |> Enum.with_index()
    |> Enum.reduce(sheet, fn {head_key, index}, sheet ->
      params = [
        {:bold, true},
        {:border, @border_full},
        {:bg_color, "#eeeeee"},
        {:align_horizontal, :center}
      ]

      element = <<65 + index>> <> "2"
      head_key = if is_nil(head_key), do: :empty, else: head_key

      Sheet.set_cell(sheet, element, head_key, params)
    end)
  end

  def make_header(sheet, title) do
    Sheet.set_cell(sheet, "A1", title, size: 16, bold: true)
  end

  def vals(list, thead) do
    Enum.map(list, fn item ->
      Enum.reduce(thead, [], fn key, acc ->
        acc ++ [item[:"#{key}"]]
      end)
    end)
  end

  def keys([data | _tail]), do: keys(data)
  def keys(data) when is_map(data), do: data |> Map.keys() |> Enum.map(&Atom.to_string/1)
end
