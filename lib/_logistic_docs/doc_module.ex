defmodule LogisticServiceDocs do
  @moduledoc """
  Функции для работы с документами
  """
  require Faker
  alias LogisticService.Repo
  alias LogisticService.Transfering.Delegates.Order

  def make_all_doc(dt_start, dt_finish) do
    data =
      Order.list_orders(:all, %{dt_start: dt_start, dt_finish: dt_finish})
      |> Repo.preload([:products, :sender])
      |> make_map

    make_document("all", data)
  end

  def make_all_doc(dt_start, dt_finish, user_id) do
    data =
      Order.list_orders(:all, %{dt_start: dt_start, dt_finish: dt_finish}, user_id: user_id)
      |> Repo.preload([:products, :sender])
      |> make_map

    make_document("all", data)
  end

  def make_user_doc(user_id, dt_start, dt_finish) do
    data =
      [{:sender_id, user_id}, {:criteries, %{dt_start: dt_start, dt_finish: dt_finish}}]
      |> Order.list_orders()
      |> Repo.preload([:products, :sender])
      |> make_map

    make_document(user_id, data)
  end

  def make_document(user_id, data) do
    filename =
      Faker.UUID.v4() |> String.replace("-", "") |> Kernel.<>("-user-#{user_id}-report.xlsx")

    remove_old(user_id)

    DocBuilder.create_table(filename, data)

    %{excel: "docs/#{filename}", pdf: "docs/#{DocBuilder.replace_format(:pdf, filename)}"}
  end

  defp remove_old(user_id) do
    File.ls!("priv/static/docs/")
    |> Enum.reject(&(!String.match?(&1, ~r/user-#{user_id}/)))
    |> Enum.map(&File.rm!("priv/static/docs/" <> &1))
  end

  defp make_map(result) when is_list(result),
    do: result |> Enum.reject(&is_nil(Map.get(&1, :order_status_id))) |> Enum.map(&make_map/1)

  defp make_map(result) do
    insurance = Map.get(result, :insurance)

    dt =
      Map.get(result, :delivery_awaited_datetime)
      |> case do
        nil -> nil
        dt -> Timex.format!(dt, "%Y-%m-%d", :strftime)
      end

    sender = Map.get(result, :sender)
    sender_fio = Map.get(result, :sender_fio)
    sender_addr = "#{Map.get(result, :sender_town)} #{Map.get(result, :sender_address)}"
    receiver = Map.get(result, :receiver_fio)
    receiver_addr = "#{Map.get(result, :receiver_town)} #{Map.get(result, :receiver_address)}"
    weight = Map.get(result, :products) |> length()
    price = Map.get(result, :price)

    sender =
      """
        #{Map.get(sender, :first_name)} #{Map.get(sender, :middle_name)} #{
        Map.get(sender, :last_name)
      } #{sender_fio && "(#{sender_fio})"}
      """
      |> String.trim()

    %{
      Накладная: insurance,
      Дата: dt,
      Отправитель: sender,
      "Адрес отправителя": sender_addr,
      Получатель: receiver,
      "Адрес получателя": receiver_addr,
      Вес: weight,
      Объем: nil,
      "Нал. платеж": nil,
      Страховка: insurance,
      "Причина отказа": nil,
      Стоимость: price
    }
  end
end
