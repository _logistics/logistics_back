# Серверное приложение

[![N|Solid](./stack.png)](./docs/stack.md)

**Сервис в себе содержит логику работы а именно:**

- 1
- 2
- 3

## Версия

0.1, разработка

## Запуск проекта

**Предустановка**

> Для запуска
>
> - **elixir**
> - **yarn** (либо изменить скрипты в package.json)
> - **postgres** (login: postgres, password: potgres, port: 5432, либо изменить конфиг)

**Запуск**

> Режим разработки:
>
> ```bash
>#  создание базы данных, миграции, заполнение данными из /priv/repo/csv/*
> mix ecto.setup
> # запуск сервера
> mix phx.server
> ```
>
> Релиз и production режим:
> (не реализовано)

## Тесты

Не проведены

## Взаимодействие

Состояние: разработка\не запущен
