defmodule LogisticService.AccountsTest do
  use LogisticService.DataCase

  alias LogisticService.Accounts.Delegates.UserStatusesFuncs

  describe "user_statuses" do
    alias LogisticService.Accounts.Models.UserStatuses

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def user_statuses_fixture(attrs \\ %{}) do
      {:ok, user_statuses} =
        attrs
        |> Enum.into(@valid_attrs)
        |> UserStatusesFuncs.create_user_statuses()

      user_statuses
    end

    test "list_user_statuses/0 returns all user_statuses" do
      user_statuses = user_statuses_fixture()
      assert UserStatusesFuncs.list_user_statuses() == [user_statuses]
    end

    test "get_user_statuses!/1 returns the user_statuses with given id" do
      user_statuses = user_statuses_fixture()
      assert UserStatusesFuncs.get_user_statuses!(user_statuses.id) == user_statuses
    end

    test "create_user_statuses/1 with valid data creates a user_statuses" do
      assert {:ok, %UserStatuses{} = user_statuses} =
               UserStatusesFuncs.create_user_statuses(@valid_attrs)

      assert user_statuses.name == "some name"
    end

    test "create_user_statuses/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = UserStatusesFuncs.create_user_statuses(@invalid_attrs)
    end

    test "update_user_statuses/2 with valid data updates the user_statuses" do
      user_statuses = user_statuses_fixture()

      assert {:ok, %UserStatuses{} = user_statuses} =
               UserStatusesFuncs.update_user_statuses(user_statuses, @update_attrs)

      assert user_statuses.name == "some updated name"
    end

    test "update_user_statuses/2 with invalid data returns error changeset" do
      user_statuses = user_statuses_fixture()

      assert {:error, %Ecto.Changeset{}} =
               UserStatusesFuncs.update_user_statuses(user_statuses, @invalid_attrs)

      assert user_statuses == UserStatusesFuncs.get_user_statuses!(user_statuses.id)
    end

    test "delete_user_statuses/1 deletes the user_statuses" do
      user_statuses = user_statuses_fixture()
      assert {:ok, %UserStatuses{}} = UserStatusesFuncs.delete_user_statuses(user_statuses)

      assert_raise Ecto.NoResultsError, fn ->
        UserStatusesFuncs.get_user_statuses!(user_statuses.id)
      end
    end

    test "change_user_statuses/1 returns a user_statuses changeset" do
      user_statuses = user_statuses_fixture()
      assert %Ecto.Changeset{} = UserStatusesFuncs.change_user_statuses(user_statuses)
    end
  end
end
