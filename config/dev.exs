use Mix.Config

# Configure your database
config :logistic_service, LogisticService.Repo,
  username: "postgres",
  password: "postgres",
  database: "logistic_service_dev",
  hostname: "localhost",
  show_sensitive_data_on_connection_error: true,
  pool_size: 10

config :logistic_service, Helper.GuardianSite,
  issuer: "logistic_service",
  secret_key: "BQ2dkQql/cDF3qj014w752VwSYh/sIeMGxS8s6SwFyqczonXBAvETfMmC7HCAjDl"

config :logistic_service, Helper.GuardianUser,
  issuer: "logistic_service",
  secret_key: "IOvcgwCxCkUkUMfjqqT0BOlHlcXadrNHPNuDyyOiT6B8uxL7sm5nSEZhLgztNtKu"

config :logistic_service, LogisticServiceWeb.Endpoint,
  http: [:inet6, port: System.get_env("PORT") || 4000],
  url: [host: "example.com", port: 4000],
  debug_errors: false,
  code_reloader: true,
  check_origin: false,
  cache_static_manifest: "priv/static/cache_manifest.json",
  server: true,
  root: ".",
  version: Application.spec(:phoenix_distillery, :vsn)

config :logistic_service, LogisticServiceWeb.Endpoint,
  live_reload: [
    patterns: [
      ~r"priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$",
      ~r"priv/gettext/.*(po)$",
      ~r"lib/logistic_service_web/(live|views)/.*(ex)$",
      ~r"lib/logistic_service_web/templates/.*(eex)$"
    ]
  ]

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20

# Initialize plugs at runtime for faster development compilation
config :phoenix, :plug_init_mode, :runtime
