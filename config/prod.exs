use Mix.Config

config :logistic_service, LogisticServiceWeb.Endpoint,
  http: [:inet6, port: System.get_env("PORT") || 4000],
  url: [host: "localhost", port: 4000],
  cache_static_manifest: "priv/static/cache_manifest.json",
  server: true,
  root: ".",
  version: Application.spec(:phoenix_distillery, :vsn)

config :logistic_service, Helper.GuardianSite,
  issuer: "logistic_service",
  secret_key: "BQ2dkQql/cDF3qj014w752VwSYh/sIeMGxS8s6SwFyqczonXBAvETfMmC7HCAjDl"

config :logistic_service, Helper.GuardianUser,
  issuer: "logistic_service",
  secret_key: "IOvcgwCxCkUkUMfjqqT0BOlHlcXadrNHPNuDyyOiT6B8uxL7sm5nSEZhLgztNtKu"

# Do not print debug messages in production
config :logger, level: :info

config :logistic_service, LogisticService.Repo,
  username: "postgres",
  password: "postgres",
  database: "logistic_service_dev",
  hostname: "localhost",
  show_sensitive_data_on_connection_error: true,
  pool_size: 10
