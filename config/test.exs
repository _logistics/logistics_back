use Mix.Config

config :logistic_service, LogisticService.Repo,
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  database: System.get_env("POSTGRES_DB") || "phx_gitlab_ci_cd_test",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

config :logistic_service, LogisticServiceWeb.Endpoint,
  http: [port: 4002],
  server: false

config :logger, level: :warn
