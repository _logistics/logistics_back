use Mix.Config

config :logistic_service,
  ecto_repos: [LogisticService.Repo]

config :logistic_service, LogisticServiceWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "r1BqU19CDTrKe7uEYrL/Xciu3i1aFUVUbAye5C/Zoxb5KXtq5eIeB1xhoTDfytEU",
  render_errors: [view: LogisticServiceWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: LogisticService.PubSub,
  live_view: [signing_salt: "qXcTGyGc"]

config :logger,
  backends: [
    :console,
    {LoggerFileBackend, :error},
    {LoggerFileBackend, :info},
    {LoggerFileBackend, :warn},
    {LoggerFileBackend, :debug}
  ],
  format: "$date $time $level $message\n $metadata\n",
  metadata: [:file, :funtion]

config :logistic_service, :general,
  page_size: 100,
  inner_page_size: 15

config :logistic_service, :customization,
  theme: "main",
  max_dispay: 20

config :phoenix, :json_library, Jason

config :logistic_service, :general,
  page_size: 100,
  inner_page_size: 5

config :logistic_service, Helper.Scheduler,
  jobs: [
    {"@daily", {Helper.Scheduler, :clear_all_cache, []}}
  ],
  debug_logging: false

config :logger, :error,
  path: Path.expand("./logs/#{Date.utc_today() |> Date.to_string()}/error_log.log"),
  level: :error

config :logger, :info,
  path: Path.expand("./logs/#{Date.utc_today() |> Date.to_string()}/info_log.log"),
  level: :info

config :logger, :debug,
  path: Path.expand("./logs/#{Date.utc_today() |> Date.to_string()}/debug_log.log"),
  level: :debug

config :logger, :warn,
  path: Path.expand("./logs/#{Date.utc_today() |> Date.to_string()}/warn_log.log"),
  level: :warn

import_config "#{Mix.env()}.exs"
