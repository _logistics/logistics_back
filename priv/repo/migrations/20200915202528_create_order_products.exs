defmodule LogisticService.Repo.Migrations.CreateOrderProducts do
  @moduledoc false
  use Ecto.Migration

  def change do
    create table(:order_products) do
      add :name, :string
      add :places, :string
      add :weight, :float
      add :g_height, :float
      add :g_length, :float
      add :g_width, :float
      add :volume, :float
      add :price, :integer
      add :count, :integer
      add :placement_info, :map
      add :comment, :string
      add :order_id, references(:orders, on_delete: :nothing)

      timestamps()
    end

    create index(:order_products, [:order_id])
  end
end
