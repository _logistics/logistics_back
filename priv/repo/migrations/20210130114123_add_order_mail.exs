defmodule LogisticService.Repo.Migrations.AddOrderMail do
  @moduledoc false
  use Ecto.Migration

  def change do
    alter table(:orders) do
      add :sendage_email, :text
      add :invoice_number, :text
    end
  end
end
