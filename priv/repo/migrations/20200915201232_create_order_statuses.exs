defmodule LogisticService.Repo.Migrations.CreateOrderStatuses do
  @moduledoc false
  use Ecto.Migration

  def change do
    create table(:order_statuses) do
      add :name, :string
      add :rules, :map
    end
  end
end
