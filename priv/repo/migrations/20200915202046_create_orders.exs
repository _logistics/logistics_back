defmodule LogisticService.Repo.Migrations.CreateOrders do
  @moduledoc false
  use Ecto.Migration

  def change do
    create table(:orders) do
      add :one_c_data, :map
      add :track_link, :string
      add :relog_data, :map
      add :is_organization, :boolean, default: false, null: false

      add :price, :bigint

      add :receiver_apartment, :string
      add :receiver_float, :string
      add :receiver_fio, :string
      add :receiver_town, :string
      add :receiver_area, :string
      add :receiver_state, :string
      add :receiver_address, :string
      add :receiver_phone, :string
      add :receiver_comment, :string, size: 1000

      add :sender_apartment, :string
      add :sender_float, :string
      add :sender_fio, :string
      add :sender_town, :string
      add :sender_area, :string
      add :sender_state, :string
      add :sender_address, :string
      add :sender_phone, :string
      add :sender_comment, :string, size: 1000

      add :is_valid, :boolean, default: false, null: false
      add :can_be_sent, :boolean, default: false, null: false

      add :delivery_proof_datetime, :naive_datetime
      add :delivery_awaited_datetime, :naive_datetime
      add :is_delivered, :boolean, default: false, null: false
      add :acceptance_datetime, :naive_datetime
      add :is_accepted, :boolean, default: false, null: false
      add :deleted_at, :naive_datetime
      add :sender_id, references(:users, on_delete: :nothing)
      add :courier_id, references(:users, on_delete: :nothing)
      add :order_status_id, references(:order_statuses, on_delete: :nothing)
      add :order_tarif_id, references(:order_tarifs, on_delete: :nothing)
      add :payment_type_id, references(:payment_types, on_delete: :nothing)
      add :updated_by_id, references(:users, on_delete: :nothing)
      add :deleted_by_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:orders, [:sender_id])
    create index(:orders, [:courier_id])
    create index(:orders, [:order_status_id])
    create index(:orders, [:order_tarif_id])
    create index(:orders, [:payment_type_id])
    create index(:orders, [:updated_by_id])
    create index(:orders, [:deleted_by_id])
  end
end
