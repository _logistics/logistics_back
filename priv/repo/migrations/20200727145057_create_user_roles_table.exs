defmodule LogisticService.Repo.Migrations.CreateUserRolesTable do
  @moduledoc false
  use Ecto.Migration

  def change do
    create table(:user_roles) do
      add :role_id, references(:roles, on_delete: :nothing), null: false
      add :user_id, references(:users, on_delete: :nothing), null: false
    end
  end
end
