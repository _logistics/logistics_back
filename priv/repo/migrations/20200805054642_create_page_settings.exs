defmodule LogisticService.Repo.Migrations.CreatePageSettings do
  @moduledoc false
  use Ecto.Migration

  def change do
    create table(:page_settings) do
      add :name, :string
      add :settings, :map

      add :page_id, references(:pages, on_delete: :nothing), null: false
      add :user_id, references(:users, on_delete: :nothing), null: false
    end
  end
end
