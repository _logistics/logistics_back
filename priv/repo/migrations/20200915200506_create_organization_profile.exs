defmodule LogisticService.Repo.Migrations.CreateOrganizationProfile do
  @moduledoc false
  use Ecto.Migration

  def change do
    create table(:organization_profile) do
      add :is_confimed, :boolean, default: false, null: false
      add :org_name, :string
      add :contact_person_fio, :string
      add :state, :string
      add :town, :string
      add :area, :string
      add :street, :string
      add :house_number, :string
      add :floor, :string
      add :apartment, :string
      add :inn, :string
      add :bik, :string
      add :rs, :string
      add :locked_at, :naive_datetime
      add :phone, :string
      add :vk, :string
      add :whatsapp, :string
      add :telegram, :string
      add :comments, :string, size: 1000
      add :mail, :string
      add :prefered_chanel, :string
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:organization_profile, [:user_id])
  end
end
