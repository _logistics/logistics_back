defmodule LogisticService.Repo.Migrations.CreateCommonSettings do
  @moduledoc false
  use Ecto.Migration

  def change do
    create table(:common_settings) do
      add :name, :string
      add :settings_json, :map
      add :settings_string, :string

      add :user_id, references(:users, on_delete: :nothing), null: false
    end
  end
end
