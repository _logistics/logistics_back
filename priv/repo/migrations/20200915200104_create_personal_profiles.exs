defmodule LogisticService.Repo.Migrations.CreatePersonalProfiles do
  @moduledoc false
  use Ecto.Migration

  def change do
    create table(:personal_profiles) do
      add :first_name, :string
      add :middle_name, :string
      add :last_name, :string
      add :state, :string
      add :town, :string
      add :area, :string
      add :street, :string
      add :house_number, :string
      add :floor, :string
      add :apartment, :string
      add :comments, :string, size: 1000
      add :locked_at, :naive_datetime
      add :phone, :string
      add :vk, :string
      add :whatsapp, :string
      add :telegram, :string
      add :mail, :string
      add :prefered_chanel, :string

      timestamps()

      add :user_id, references(:users, on_delete: :nothing)
    end

    create index(:personal_profiles, [:user_id])
  end
end
