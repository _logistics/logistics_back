defmodule LogisticService.Repo.Migrations.UserCode do
  use Ecto.Migration

  def change do
    create table("user_code") do
      add :phone, :string, size: 40
      add :code, :integer
    end

    create unique_index(:user_code, [:phone])
  end
end
