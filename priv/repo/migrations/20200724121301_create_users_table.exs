defmodule LogisticService.Repo.Migrations.CreateUsersTable do
  @moduledoc false
  use Ecto.Migration

  def change do
    create table(:users) do
      add :phone, :string
      add :one_c_data, :string
      add :is_confirmed, :boolean, null: false, default: false
      add :is_organization, :boolean, null: false, default: false
      add :confirmation_code, :string
      add :comments, :string
      add :deleted_at, :utc_datetime
      add :logged_at, :utc_datetime
      add :password_hash, :string

      timestamps()
    end

    create unique_index(:users, [:phone])
  end
end
