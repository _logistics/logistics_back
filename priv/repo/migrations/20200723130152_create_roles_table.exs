defmodule LogisticService.Repo.Migrations.CreateRolesTable do
  @moduledoc false
  use Ecto.Migration

  def change do
    create table(:roles) do
      add :slug, :string
      add :notes, :string
      add :extensions, :map
    end

    create unique_index(:roles, [:slug])
  end
end
