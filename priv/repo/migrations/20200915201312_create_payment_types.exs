defmodule LogisticService.Repo.Migrations.CreatePaymentTypes do
  @moduledoc false
  use Ecto.Migration

  def change do
    create table(:payment_types) do
      add :name, :string
      add :rules, :map
    end
  end
end
