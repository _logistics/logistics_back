defmodule LogisticService.Repo.Migrations.CreateOrderTemplates do
  @moduledoc false
  use Ecto.Migration

  def change do
    create table(:order_templates) do
      add :name, :string
      add :order_id, references(:orders, on_delete: :nothing)
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:order_templates, [:order_id])
    create index(:order_templates, [:user_id])
  end
end
