defmodule LogisticService.Repo.Migrations.AddCommentToPaymentType do
  use Ecto.Migration

  def change do
    alter table(:payment_types) do
      add :comment, :string, default: nil
    end
  end
end
