defmodule LogisticService.Repo.Migrations.AddOppotunityToCreateOrder do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :contract_link, :string, default: nil
      add :opportunity_to_create, :boolean, default: false
    end
  end
end
