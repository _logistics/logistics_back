defmodule LogisticService.Repo.Migrations.CreateUserSmses do
  use Ecto.Migration

  def change do
    create table(:user_smses) do
      add :counter, :integer
      add :phone, :string

      timestamps()
    end
  end
end
