defmodule LogisticService.Repo.Migrations.CreateUserPhotos do
  @moduledoc false
  use Ecto.Migration

  def change do
    create table(:user_photos) do
      add :photo_link, :string

      add :user_id, references(:users, on_delete: :nothing), null: false
    end
  end
end
