defmodule LogisticService.Repo.Migrations.CreatePages do
  @moduledoc false
  use Ecto.Migration

  def change do
    create table(:pages) do
      add :name, :string
      add :link, :string
      add :component, :string
      add :avaliable, :boolean, default: true, null: false

      add :menu_id, references(:menus, on_delete: :nothing), null: false
    end
  end
end
