defmodule LogisticService.Repo.Migrations.AddPaymentCommentOrder do
  use Ecto.Migration

  def change do
    alter table(:orders) do
      add :payment_comment, :text
    end
  end
end
