defmodule LogisticService.Repo.Migrations.AddInsuranceOrder do
  use Ecto.Migration

  def change do
    alter table(:orders) do
      add :insurance, :text
    end
  end
end
