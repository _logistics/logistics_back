defmodule LogisticService.Repo.Migrations.CreateOrderTarifs do
  @moduledoc false
  use Ecto.Migration

  def change do
    create table(:order_tarifs) do
      add :name, :string
      add :rules, :map
    end
  end
end
