defmodule LogisticService.Repo.Migrations.CreateUserTypes do
  @moduledoc false
  use Ecto.Migration

  def change do
    create table(:user_types) do
      add :name, :string
    end
  end
end
