defmodule LogisticService.Repo.Migrations.CreateUserStatuses do
  @moduledoc false
  use Ecto.Migration

  def change do
    create table(:user_statuses) do
      add :name, :string
    end
  end
end
