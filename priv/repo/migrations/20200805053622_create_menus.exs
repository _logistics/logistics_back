defmodule LogisticService.Repo.Migrations.CreateMenus do
  @moduledoc false
  use Ecto.Migration

  def change do
    create table(:menus) do
      add :name, :string
      add :icon, :string
      add :avaliable, :boolean, default: true, null: false

      add :role_id, references(:roles, on_delete: :nothing), null: false
    end
  end
end
