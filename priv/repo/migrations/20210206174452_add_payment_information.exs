defmodule LogisticService.Repo.Migrations.AddPaymentInformation do
  use Ecto.Migration

  def change do
    alter table(:orders) do
      add :odd_money, :string
      add :payer, :string
    end
  end
end
