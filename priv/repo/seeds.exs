alias Helper.CSVReader
import Faker
alias LogisticService.Accounts

alias LogisticService.Factories.{
  Users,
  Roles,
  OrderStatus,
  OrderTarifs
}

"csv/accounts/roles.csv"
|> Path.expand(__DIR__)
|> CSVReader.get()
|> Roles.make_roles()
|> Roles.multisert()

"csv/accounts/users.csv"
|> Path.expand(__DIR__)
|> CSVReader.get()
|> Users.multisert()

"csv/tarifs/order_status.csv"
|> Path.expand(__DIR__)
|> CSVReader.get()
|> OrderStatus.multisert()

"csv/tarifs/order_tarif.csv"
|> Path.expand(__DIR__)
|> CSVReader.get()
|> OrderTarifs.multisert()

"csv/tarifs/payment_types.csv"
|> Path.expand(__DIR__)
|> CSVReader.get()
|> OrderTarifs.multisert()

# ~w(9692003458 9692003459 9692003450 9692003451 9692003452)
# |> Enum.map(&Accounts.get_user_by_phone!/1)
# |> Enum.with_index(1)
# |> Enum.map(fn {user, index} ->
#  case rem(index, 2) == 1 do
#    true  ->
#      user_profile = %{
#        state: Faker.Address.Ru.state(),
#        town: Faker.Address.city(),
#        street: Faker.Address.street_name(),
#        house_number: "#{Faker.random_between(1, 150)}",
#        floor: "#{Faker.random_between(1, 20)}",
#        apartment: "#{Faker.random_between(1, 10_000)}",
#        comments: Faker.Lorem.paragraph(),
#        mail: Faker.Internet.email(),
#        phone: Faker.Phone.EnUs.phone()
#      }
#      LogisticService.Accounts.make_user_user_status(user)
#      LogisticService.Accounts.create_user_profile(Map.get(user, :id), user_profile)
#    false ->
#      org_profile = %{
#        state: Faker.Address.Ru.state(),
#        town: Faker.Address.city(),
#        street: Faker.Address.street_name(),
#        house_number: "#{Faker.random_between(1, 150)}",
#        floor: "#{Faker.random_between(1, 20)}",
#        apartment: "#{Faker.random_between(1, 10_000)}",
#        comments: Faker.Lorem.paragraph(),
#        mail: Faker.Internet.email(),
#        phone: Faker.Phone.EnUs.phone(),
#        bik: Faker.String.base64(20),
#        inn: Faker.String.base64(10),
#        contact_person_fio: Faker.Person.name(),
#        org_name: Faker.Company.name()
#      }
#      LogisticService.Accounts.make_user_org_status(user)
#      LogisticService.Accounts.create_org_profile(Map.get(user, :id), org_profile)
#  end
# end)

make_product = fn x ->
  1..x
  |> Enum.map(fn _y ->
    %{
      name: Faker.Commerce.product_name(),
      price: Faker.random_between(1, 100_000),
      count: Faker.random_between(1, 10)
    }
  end)
end

1..500
|> Enum.map(fn _x ->
  sender_id = Faker.random_between(6, 9)

  order = %{
    sender_id: sender_id,
    receiver_fio: Faker.Person.name(),
    receiver_town: Faker.Address.city(),
    receiver_state: Faker.Address.Ru.state(),
    receiver_address: Faker.Address.street_address(),
    receiver_phone: Faker.Phone.EnUs.phone(),
    receiver_comment: Faker.Lorem.paragraph(),
    sender_fio: Faker.Person.name(),
    sender_town: Faker.Address.city(),
    sender_state: Faker.Address.Ru.state(),
    sender_address: Faker.Address.street_address(),
    sender_phone: Faker.Phone.EnUs.phone(),
    order_status_id: Faker.random_between(1, 7),
    sender_comment: Faker.Lorem.paragraph(),
    price: Faker.random_between(400, 50_000),
    products: make_product.(Faker.random_between(1, 10)),
    inserted_at: Faker.DateTime.between(~N[2020-01-01 00:00:00], ~N[2021-01-01 00:00:00]),
    updated_at: Faker.DateTime.between(~N[2020-01-01 00:00:00], ~N[2021-01-01 00:00:00])
  }

  LogisticService.Transfering.create_order(sender_id, order)
end)
